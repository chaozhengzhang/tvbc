<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TVBC-翡翠东方</title>
    <style type="text/css">
        body{margin:0; padding:0;background:url(/public/images/job/bodybg.png) top left no-repeat; font-family:"Microsoft YaHei";}
        .contaner{}
        .header{width:1000px; margin:0 auto;}
        .logo{float:left; width:135px; height:160px;}
        .toplink{float:right; width:230px; color:#999; height:32px; line-height:32px; margin-top:70px;}
        .toplink a{color:#111; text-decoration:none; display:inline-block; margin:0px 6px; font-size:14px;}
        .toplink a span{background:url(/public/images/job/tvbc-icon.png) left top no-repeat; display:inline-block; width:20px;}
        .toplink a .tvb-logo{ background-position:-4px 2px;}
        .toplink a .sina-weibo{ background-position:-33px 2px;}
        .toplink a .weixin{ background-position:-68px 2px;}
        .toplink a .erweima{color: #FF6600; display: none; font-size: 12px; line-height: 22px;position: absolute; z-index: 9999;}
        .toplink a .itvbc{ background-position:-102px 2px;}
        .toplink a:hover .erweima{display:block;}
        .clearboth{clear:both;}
        .jobMeg{float:left; width:100%;}
        .jobMegshow p{padding-left:50px; font-size:14px; line-height:26px;}
        .footer{clear:both; line-height:32px; text-align:center; font-size:12px; color:#999;}
    </style>
</head>

<body>
<div class="contaner">
    <div class="header">
        <div class="logo"> <a href="http://www.tvbc.com.cn"><img src="/public/images/job/otherlogo.png" alt="TVBC官方" width="129" height="157" /></a>
        </div>
        <div class="toplink">
            <a href="http://tvbc.com.cn/users/signin">登录</a>|<a href="http://tvbc.com.cn/users/signin">注册</a><a href="http://www.tvb.com/" target="_blank"  title="香港无线电台"><span class="tvb-logo" title="香港无线电台">&nbsp;</span></a><a href="http://widget.weibo.com/dialog/follow.php?fuid=2951255900&refer=test.tvbc.com.cn&language=zh_cn&type=widget_page&vsrc=app_followbutton&backurl=http%3A%2F%2Ftest.tvbc.com.cn%2F&rnd=1368895465406" title="新浪微博" target="_blank" ><span class="sina-weibo">&nbsp;</span></a><a href="#"  title="微信"><span class="weixin">&nbsp;</span><div style="color:#006DBB;font-weight:bold;" class="erweima">关注翡翠东方<br><img alt="关注翡翠东方" src="/public/images/indexerma.png" data-pinit="registered">
                </div></a><a href="http://tvbc.com.cn/downloads" title="下载"><span class="itvbc">&nbsp;</span></a>
        </div>
        <div class="clearboth"></div>
    </div>
    <div class="jobMeg">
        <div class="jobMegshow">
            <h2><img src="/public/images/job/job1.png" width="355" height="147" alt="" /></h2>
            <p>依托TVBC丰富资源, 携手中国互联网巨头——<br />
                道腾创意传媒横空出世<br />
                她致力于为亿万网民提供新鲜内容，<br />
                她服务于一切成长在互联网的客户，<br />
                她有专业、牛逼、不拘一格的团队，<br />
                她充分依赖年轻人的创造力和活力，<br />
                任何聪明的大脑、张扬的个性都将在这里成为主角；<br />
                道腾欢迎你来这里欢乐地捣腾！<br />
                等你来！赶紧来！<br />
                在这里，你将获得一个月的项（内）目（容）实（保）习（密）机会；<br />
                周薪1000元的丰（毛）厚（爷）报（爷）酬；<br />
                你将有机会与亚洲顶级明星一起工作；<br />
                你将出入徐家汇商圈，身边有各种IT精英、高大上品牌和1、4、9号地铁线。
            </p>
        </div>
        <div class="jobMegshow">
            <h2><img src="/public/images/job/job2.png" width="391" height="123" alt="" /></h2>
            <p>1.	新闻，广告，艺术专业优先考虑；对传媒行业充满好奇和热情的你一样适合；<br />
                2.	不求英文六级，但求做事认真；杂事，正事一样能干；<br />
                3.	抗得了压力，也寻得了乐子；能与团队协同工作；
            </p>
        </div><div class="jobMegshow">
            <h2><img src="/public/images/job/job3.png" width="408" height="129" alt="" /></h2>
            <p>如果您觉得自己是个中高手，请发简历发至：<strong><a href="mailto:recruitment-tt@tvbc.com.cn">recruitment-tt@tvbc.com.cn</a></strong><br />
                邮件主题: 应聘实习生+姓名+联系方式
            </p>
            <p><a href="http://www.tvbc.com.cn/"><img src="/public/images/job/job-tvbc.png" alt="TVBC官网" width="123" height="56" /></a><a href="http://v.qq.com/" target="_blank"><img src="/public/images/job/job-qq.png" alt="腾讯视频" /></a></p>
        </div>
    </div>
</div>
<div class="footer">Copyright © 2005-2014 www.www.com All rights reserved. 沪ICP备05049372号 </div>
</body>
</html>
