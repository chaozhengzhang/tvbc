<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-21
 * Time: 下午2:27
 * To change this template use File | Settings | File Templates.
 */

class Positions extends REST_Controller{

    public function read()
    {
        $params = $this->get();
        $response = Position::get_by_params($params);
        $this->response($response['results'],$response['total']);
    }

    public function update($id) {
        $model = Position::find($id);
        $data = $this->put('data');

        if($model) {
            unset($data['id']);
            if(isset($data['ranking'])) {
                $ranking = $data['ranking'];
                $old_position = Position::find_by_ranking($ranking);
                $old_position->ranking = $model->ranking;
                $old_position->save();
            }
            $model->update_attributes($data);
        }

        $this->response($model->to_array(),1,'编辑成功！');
    }

    public function create(){
        $data = $this->post('data');
        if(!Position::find_by_name($data['name'])){
            $model = Position::create($data);
            $this->response($model->to_array(),1,'增加成功！');
        } else {
            $this->response(NULL,0,'该职位已经存在！',FALSE);
        }
    }

    public function destroy($id = 0){
        $model = Position::find($id);
        if($model) {
            $model->delete();
        }
        $this->response(NULL,1,'删除成功！');
    }
}