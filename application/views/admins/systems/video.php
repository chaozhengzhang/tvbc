<script type="text/javascript">
    Ext.define('Ext.ux.VideoView', {
        extend: 'Ext.form.Panel',
        alias: 'widget.videoview',
        title: '公司视频简介上传',
        buttonAlign: 'center',
        bodyPadding: 30,
        fieldDefaults: {
            labelAlign: 'right'
        },
        layout: 'anchor',
        defaults: {
            anchor: '40%'
        },
        defaultType: 'textfield',
        initComponent: function (config) {
            var me = this;

            Ext.apply(me, config || {});

            me.items = [
                {
                    xtype: 'filefield',
                    emptyText: '选择视频......',
                    fieldLabel: '视频',
                    name: 'userfile',
                    buttonText: '',
                    buttonConfig: {
                        iconCls: 'upload-icon'
                    }
                }
            ];

            me.buttons = [
                {
                    text: '上传',
                    formBind: true,
                    disabled: true,
                    handler: function () {
                        var form = this.up('form').getForm();
                        if (form.isValid()) {
                            form.submit({
                                url: 'data/videos/upload',
                                success: function (form, action) {
                                    Ext.Msg.alert('成功', action.result.message);
                                },
                                failure: function (form, action) {
                                    Ext.Msg.alert('失败', action.result.message);
                                }
                            });
                        }
                    }
                }
            ];
            me.callParent(arguments);
        }
    });

    Ext.widget('videoview',{ renderTo: 'video-view'});
</script>
<div id="video-view"></div>