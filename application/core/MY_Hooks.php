<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Hooks Class for extends
 *
 * Expanded the system core hooks class.
 *
 * @package     CodeIgniter
 * @category    Hooks
 * @author      Wanglong
 * @link        http://wanglong.name
 */
class MY_Hooks extends CI_Hooks {

    /**
     * Automatic loading directory
     *
     * @var array
     */
    static $_autoload_paths = array('third_party/restserver');

    /**
     * Class constructor
     *
     * @return void
     */
    function __construct() {
        parent::__construct();
        spl_autoload_register(__CLASS__ . '::autoload');
    }

    // --------------------------------------------------------------------

    /**
     * Auto load class
     *
     * When not found class, execute this method.
     *
     * @param  string
     * @return void
     */
    static function autoload($class_name) {
        foreach (self::$_autoload_paths as $dir) {
            $file = APPPATH . $dir . DIRECTORY_SEPARATOR . $class_name . '.php';
            if (file_exists($file)) {
                require $file;
                break;
            }
        }
    }
}

/* End of file MY_Hooks.php */