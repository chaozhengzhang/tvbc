<style type="text/css">
    .drama-btn-span {
        height: 25px;
        width: 25px;
        display: inline-block;
        text-align: center;
        cursor: pointer;
    }
    .drama-btn {
        height: 16px;
        width: 16px;
        padding-top: 5px;
    }
    .drama-btn-on {
        border: 1px solid #b4b5bc;
        background-color: #d2d2d2;
        border-radius: 5px;
    }
</style>
<div class="magicshow">
    <div class="l-pic"><img src="<?php echo base_url('uploads/teleplays/poster/big/'.$teleplay->poster);?>"  /></div>
    <div class="r-meg magicMeg">
        <h2><?php echo $teleplay->name;?></h2><div class="magicfx"><DIV class=bshare-custom>分享：<A class=bshare-qzone title=分享到QQ空间></A><A class=bshare-sinaminiblog title=分享到新浪微博></A><A class=bshare-renren title=分享到人人网></A><A class=bshare-qqmb title=分享到腾讯微博></A><A class=bshare-neteasemb title=分享到网易微博></A><A class="bshare-more bshare-more-icon more-style-addthis" title=更多平台></A><SPAN class="BSHARE_COUNT bshare-share-count">0</SPAN></DIV>
<SCRIPT type=text/javascript charset=utf-8 src="http://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=2&amp;lang=zh"></SCRIPT>

<SCRIPT type=text/javascript charset=utf-8 src="http://static.bshare.cn/b/bshareC0.js"></SCRIPT></div>
<div class="clearboth"></div>
        <dl>
            <dt>监制：</dt>
            <dd><?php echo $teleplay->producer;?></dd>
        </dl>
        <dl>
            <dt>编审：</dt>
            <dd><?php echo $teleplay->copy_editor;?></dd>
        </dl>
        <dl>
            <dt>主演：</dt>
            <dd><?php echo $teleplay->starring;?></dd>
        </dl>
        <dl>
            <dt>剧情：</dt>
            <dd>
                <?php echo $teleplay->plot ? $teleplay->plot: '暂未上映，敬请期待。';?>
            </dd>
        </dl>


    </div>
    <div class="clearboth"></div>
    <?php
        if($dramas) {

    ?>
    <h3>剧集</h3>
    <?php
        if($switch_on == 2) {
            ?>
            <div style="padding: 5px 10px;">
                <span>播放源:</span>
                <span style="margin: 0 5px;" data-drama="tudou-drama" data-sdrama="youku-drama" class="drama-btn-span drama-btn-on">
                    <img src="/public/img/tudou.png" class="drama-btn" />
                </span>
                <span class="drama-btn-span" data-drama="youku-drama" data-sdrama="tudou-drama">
                    <img src="/public/img/youku.png" class="drama-btn" />
                </span></div>
        <?php
        }
            ?>
    <div class="jujilistDiv" id="tudou-drama">
        <?php
            foreach ($dramas as $drama) {
                if($drama['part']) {
                    echo '<h3>'.$drama['part'].'</h3>';
                }

                if(count($drama['data']) > 0) {
                    foreach($drama['data'] as $data) {
        ?>
        <a target="_blank" href="<?php echo $data->url;?>"><?php echo $data->leave;?>集</a>
        <?php
                    }
                } else {
                    echo "暂未上映，敬请期待。";
                }
            }
        ?>
    </div>
    <div class="jujilistDiv" style="display: none;" id="youku-drama">
            <?php
            if(count($dramays[0]['data']) > 0) {
                foreach ($dramays as $dramay) {
                    if ($dramay['part']) {
                        echo '<h3>' . $dramay['part'] . '</h3>';
                    }

                    if (count($dramay['data']) > 0) {
                        foreach ($dramay['data'] as $data) {
                            ?>
                            <a target="_blank" href="<?php echo $data->url; ?>"><?php echo $data->leave; ?>集</a>
                        <?php
                        }
                    } else {
                        echo "暂未上映，敬请期待。";
                    }
                }
            }
            ?>
    </div>
    <?php
        }
    ?>
    <div class="clearboth"></div>
    <?php
        $stills = $teleplay->stills;
        if($stills) {
    ?>
    <ul class="jjpicshow">
        <?php
            foreach ($stills as $still) {
        ?>
        <li><a href="javascript:void(0);"><img src="<?php echo base_url('uploads/teleplays/stills/'.$still->file_name);?>" alt="剧照" /></a></li>
        <?php
            }
        ?>
    </ul>
    <?php
        }
    ?>
</div>
<script type="text/javascript">
    $(function(){

        $('.drama-btn-span').on('click', function () {
            var drama = $(this).data('drama');
            var sdrama = $(this).data('sdrama');
            $('#' + drama).show();
            $('#' + sdrama).hide();

            if($(this).next('span')) {
                $(this).addClass('drama-btn-on').next('span').removeClass('drama-btn-on');
            }

            if($(this).prev('.drama-btn-span')){
                $(this).addClass('drama-btn-on').prev('.drama-btn-span').removeClass('drama-btn-on');
            }

        })
    });
</script>