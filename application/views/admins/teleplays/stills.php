<style>
    #stills-view .x-panel-body {
        background: white;
    }

    #stills-view .thumb {
        background: #dddddd;
        padding: 3px;
        padding-bottom: 0;
    }

    .x-quirks #stills-view .thumb {
        padding-bottom: 3px;
    }

    #stills-view .thumb img {
        height: 115px;
        width: 170px;
    }

    #stills-view .thumb-wrap {
        float: left;
        margin: 4px;
        margin-right: 0;
        padding: 5px;
    }

    #stills-view .thumb-wrap span {

        display: block;
        overflow: hidden;
        text-align: center;
        width: 175px; /* for ie to ensure that the text is centered */
    }

    #stills-view .x-item-over {
        border: 1px solid #dddddd;
        background: #efefef url(/public/img/over.gif) repeat-x left top;
        padding: 4px;
    }

    #stills-view .x-item-selected {
        background: #eff5fb url(/public/img/selected.gif) no-repeat right bottom;
        border: 1px solid #99bbe8;
        padding: 4px;
    }

    #stills-view .x-item-selected .thumb {
        background: transparent;
    }

    #stills-view .loading-indicator {
        font-size: 11px;
        background-image: url("/public/img/delete.png");;
        background-repeat: no-repeat;
        background-position: left;
        padding-left: 20px;
        margin: 10px;
    }

    .x-view-selector {
        position: absolute;
        left: 0;
        top: 0;
        width: 0;
        border: 1px dotted;
        opacity: .5;
        -moz-opacity: .5;
        filter: alpha(opacity=50);
        zoom: 1;
        background-color: #c3daf9;
        border-color: #3399bb;
    }

    .ext-strict .ext-ie .x-tree .x-panel-bwrap {
        position: relative;
        overflow: hidden;
    }
</style>
<script type="text/javascript">
    Ext.define('Ext.ux.StillView', {
        extend: 'Ext.Panel',
        alias: 'widget.stillview',
        frame: true,
        initComponent: function (config) {
            var me = this;
            Ext.apply(me, config || {});

            me.store = Ext.create('Ext.ux.data.NotificationStore', {
                model: 'TeleplayStill',
                autoLoad: false,
                autoSync: true,
                proxy: {
                    url: baseUrl('data/teleplay_stills')
                }
            });

            me.tbar = ['-', {
                xtype: 'combo',
                fieldLabel: '影视剧',
                labelAlign: 'right',
                displayField: 'name',
                valueField: 'id',
                store: Ext.create('Ext.data.Store', {
                    fields: ['id', 'name'],
                    pageSize: 300,
                    proxy: {
                        type: 'rest',
                        startParam: 'offset',
                        url: baseUrl('data/teleplays'),
                        reader: {
                            type: 'json',
                            root: 'results'
                        }
                    },
                    autoLoad: true
                }),
                queryMode: 'local',
                typeAhead: true,
                listeners: {
                    select: function (combo) {
                        me.store.proxy.extraParams.teleplay_id = combo.value;
                        me.store.load();
                    }
                }
            }, '-', {
                text: '照片选择',
                handler: function () {
                    me.uploadStill(this.previousNode('combo').getValue());
                }
            }, '-', {
                text: '删除选中',
                handler: function (btn, e) {
                    var selection = me.dataView.getSelectionModel().getSelection();
                    if (selection[0]) me.store.remove(selection[0]);
                }
            }];

            me.initDataView();
            me.items = [me.dataView];
            me.callParent(arguments);
        },
        uploadStill: function (teleplay_id) {
            var me = this, win;
            if (!teleplay_id) {
                tools.showTips('提示','请先选择影视剧!');
                return false;
            }
            if (!win) {
                /*SWFUpload.Console.debugEl = "debug";*/
                win = Ext.widget('window', {
                    height: 320,
                    width: 820,
                    modal: true,
                    items: [
                        {
                            xtype: 'swfuploadpanel',
                            setting: {
                                post_params: {
                                    teleplay_id: teleplay_id
                                },
                                file_types: '*.jpg;*.gif;*.jpeg;*.png',
                                upload_url: '/data/teleplay_stills/upload_image',
                                flash_url: 'public/swfupload/swfupload.swf',
                                flash9_url: 'public/swfupload/swfupload_fp9.swf'
                            },
                            listeners: {
                                'uploadSuccess': function (sefupload, file, data) {
                                    if (data) {
                                        me.store.proxy.extraParams.teleplay_id = teleplay_id;
                                        me.store.load();
                                    }
                                },
                                uploadComplete: function(){
                                    win.close();
                                }
                            }
                        }
                    ]
                }).show();
            }
        },
        initDataView: function () {
            var me = this;
            me.dataView = Ext.create('Ext.view.View', {
                store: me.store,
                border: 0,
                tpl: [
                    '<tpl for=".">',
                    '<div class="thumb-wrap">',
                    '<div class="thumb"><img src="uploads/teleplays/stills/{file_name}" title="{file_name}"></div>',
                    '</div>',
                    '</tpl>',
                    '<div class="x-clear"></div>'
                ],
                multiSelect: false,
                height: 410,
                trackOver: true,
                overItemCls: 'x-item-over',
                itemSelector: 'div.thumb-wrap',
                emptyText: 'No images to display'
            });
        }
    });

    Ext.widget('stillview', {
        title: '剧照',
        id: 'stills-view',
        renderTo: 'stills-view-panel'
    });


</script>
<div id="stills-view-panel"></div>
<!--<div id="debug"></div>-->