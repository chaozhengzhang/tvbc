<?php
/**
 * Created by PhpStorm.
 * User: zhang.chaozheng
 * Date: 14-1-17
 * Time: 上午11:00
 */

class Comments extends REST_Controller {

	public function create()
	{
        $file = fopen(base_url('public/badword.txt'), "r");
        $keywords = array();

        while (!feof($file)) {
            $keywords[] = fgets($file);
        }

        fclose($file);

		$data = $this->post();

		$detail = $data['detail'];
		if(check_keyword($detail,$keywords)) {
			echo '{"info":"评论中含有非法字符！","status":"n"}';
		} else {
			$data['create_time'] = time();
			unset($data['detail']);
			$data['user_id'] = $this->user['id'];
			$comment = Comment::create($data);
			if($comment) {
				CommentProfile::create(array(
					'comment_id' => $comment->id,
					'detail' => $detail
				));
			}
			echo '{"info":"发表成功！","status":"y"}';
		}
	}
} 