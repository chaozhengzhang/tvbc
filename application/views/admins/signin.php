<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>TVBC数据管理平台v1.0</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/extjs/resources/css/ext-all-debug.css'); ?>"/>
    <script src="<?php echo base_url('public/extjs/ext-all-debug.js'); ?>"></script>
    <script>
        Ext.onReady(function () {
            Ext.QuickTips.init();

            Ext.create('widget.window', {
                y: 150,
                title: 'TVBC数据管理平台v1.0',
                closable: false,
                plain: true,
                layout: 'fit',
                items: [
                    {
                        xtype: 'form',
                        layout: 'form',
                        frame: false,
                        border: false,
                        bodyPadding: 10,
                        width: 350,
                        fieldDefaults: {
                            labelAlign: 'right'
                        },

                        defaultType: 'textfield',
                        items: [
                            {
                                fieldLabel: '用户名',
                                allowBlank: false,
                                name: 'name'
                            }, {
                                fieldLabel: '密码',
                                allowBlank: false,
                                inputType: 'password',
                                name: 'password'
                            }],

                        buttons: [{
                            text: '确定',
                            handler: function() {
                                var form = this.up('form').getForm();
                                if(form.isValid()){
                                    form.submit({
                                        url: '/data/admins/login',
                                        success: function(fm,action){
                                            window.location.href = '/admin';
                                        },
                                        failure: function(fm,action){
                                            switch (action.failureType) {
                                                case Ext.form.action.Action.CLIENT_INVALID:
                                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                                    break;
                                                case Ext.form.action.Action.CONNECT_FAILURE:
                                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                                    break;
                                                case Ext.form.action.Action.SERVER_INVALID:
                                                    Ext.Msg.alert('Failure', action.result.message);
                                            }
                                        }
                                    });
                                }
                            }
                        }]
                    }
                ]
            }).show();
        });
    </script>
</head>
<body>
</body>
</html>
