<div class="regDiv regDivWidget" id="usre-signin-view" style="width:500px;display: none;">
    <h2>我的帐户</h2>

    <div class="regMegDivWidget">
        <div class="regDiv-l">
            <h3>登录 LOGIN（已注册用户由此登录）</h3>

            <p class="regWord">如果您已经拥有一个帐户，请输入您的用户名和密码。</p>

            <form class="login-form" method="post" action="<?php echo base_url('data/users/login'); ?>">
                <p>用户名<br/>
                    <input name="name_email" datatype="*" class="inputxt" type="text" nullmsg="用户名不能为空"/>
                    <span class="Validform_checktip"></span>
                </p>

                <p>密码<br/>
                    <input type="password" name="password" datatype="*" class="inputxt" nullmsg="密码不能为空" />
                    <span class="Validform_checktip"></span>
                </p>
                <p style="text-align: right;margin-right: 185px;">
					<a href="<?php echo base_url('users/forgot');?>">忘记密码?</a>
                    <input type="submit" class="loginbtn" value="登录"/>
                    <span id="msgdemo" class="Validform_checktip"></span>
                </p>
            </form>
        </div>
    </div>
</div>