<script type="text/javascript">

Ext.define('Ext.ux.CampaignView', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.campaignview',
    title: '活动编辑 (* 双击表格列即可编辑活动)',
    height: 540,
    initComponent: function (config) {
        var me = this;

        Ext.apply(me, config || {});

        me.store = Ext.create('Ext.ux.data.NotificationStore', {
            model: 'Campaign',
            autoLoad: true,
            autoSync: true,
            proxy: {
                url: baseUrl('data/campaigns')
            }
        });

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            errorSummary: false,
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    if (context.record.phantom) {
                        me.store.remove(context.record);
                    }
                }
            }
        });

        me.plugins = [rowEditing];

        me.columns = [
            {
                text: '编号',
                width: 60,
                sortable: true,
                dataIndex: 'id'
            },
            {
                text: '活动名称',
                flex: 1,
                sortable: true,
                dataIndex: 'name',
                field: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            },
            {
                text: '开始日期',
                width: 100,
                sortable: true,
                dataIndex: 'start_time',
                field: {
                    xtype: 'datefield',
                    format: 'Y-m-d'
                }
            },
            {
                text: '结束日期',
                width: 100,
                sortable: true,
                dataIndex: 'end_time',
                field: {
                    xtype: 'datefield',
                    format: 'Y-m-d'
                }
            },
            {
                text: '参与奖说明',
                flex: 1,
                sortable: false,
                dataIndex: 'reward',
                field: {
                    xtype: 'textarea'
                }
            },
            {
                text: '大奖说明',
                flex: 1,
                sortable: false,
                dataIndex: 'top_prize',
                field: {
                    xtype: 'textarea'
                }
            },
            {
                text: '发布?',
                xtype: 'checkcolumn',
                align: 'center',
                width: 80,
                sortable: true,
                dataIndex: 'display',
                field: {
                    xtype: 'checkbox',
                    cls: 'x-grid-checkheader-editor'
                }
            },
            {
                xtype: 'actioncolumn',
                width: 100,
                text: '参与活动方式',
                align: 'center',
                items: [
                    {
                        icon: 'public/img/edit_task.png',  // Use a URL in the icon config
                        tooltip: '编辑参与活动方式',
                        handler: function (grid, rowIndex, colIndex) {
                            var win, rec = grid.getStore().getAt(rowIndex);
                            if (!win) {
                                win = Ext.widget('window', {
                                    modal: true,
                                    resizable: true,
                                    maximizable: true,
                                    bodyPadding: 10,
                                    layout: 'fit',
                                    items: [
                                        {
                                            xtype: 'form',
                                            minWidth: 640,
                                            minHeight: 400,
                                            border: false,

                                            fieldDefaults: {
                                                anchor: '100%',
                                                labelAlign: 'right'
                                            },
                                            items: [
                                                {
                                                    xtype: 'htmleditor',
                                                    fieldLabel: '参与活动方式',
                                                    height: 340,
                                                    name: 'rule',
                                                    value: rec.get('rule')
                                                }
                                            ],
                                            buttonAlign: 'center',
                                            buttons: [
                                                {
                                                    itemId: 'save',
                                                    text: '保存',
                                                    handler: function () {
                                                        var fm = this.up('form').getForm();
                                                        var values = fm.getValues();
                                                        if (Ext.String.trim(values.rule) == "<br>") {
                                                            tools.showTips('错误', '规则不能为空!');
                                                            return false;
                                                        }

                                                        rec.set(values);
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }).show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'actioncolumn',
                width: 120,
                text: '影视剧关联',
                align: 'center',
                items: [
                    {
                        icon: 'public/img/videos.png',  // Use a URL in the icon config
                        tooltip: '影视剧关联',
                        handler: function (grid, rowIndex, colIndex) {
                            var win, rec = grid.getStore().getAt(rowIndex);
                            if (!win) {

                                win = Ext.widget('window', {
                                    modal: true,
                                    resizable: true,
                                    maximizable: true,
                                    title: '活动剧集',
                                    layout: 'fit',
                                    items: [
                                        me.initTeleplayGrid(rec.get('id'))
                                    ]
                                }).show();
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'actioncolumn',
                width: 80,
                text: '删除',
                align: 'center',
                items: [
                    {
                        icon: 'public/img/delete.png',  // Use a URL in the icon config
                        tooltip: '删除活动',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            Ext.Msg.show({
                                title: '警告?',
                                msg: '数据一旦删除将无法恢复,确认删除？',
                                buttons: Ext.Msg.YESNO,
                                fn: function (btn, text) {
                                    if (btn == 'yes') {
                                        me.store.remove(rec);
                                    }
                                },
                                icon: Ext.Msg.WARNING
                            });
                        }
                    }
                ]
            }
        ];

        me.tbar = ['-', {
            text: '增加',
            iconCls: 'icon-add',
            handler: function () {
                me.store.insert(0, new Campaign());
                rowEditing.startEdit(0, 0);
            }
        }, '-'];

        me.bbar = Ext.create('Ext.PagingToolbar', {
            store: me.store,
            displayInfo: true
        });

        me.callParent(arguments);
    },
    initTeleplayGrid: function (campaign_id) {
        Ext.define('CampaignTeleplay', {
            extend: 'Ext.data.Model',
            fields: [
                {
                    name: 'id',
                    type: 'int',
                    defaultValue: 0
                },
                {
                    name: 'campaign_id',
                    type: 'int',
                    defaultValue: campaign_id
                },
                {
                    name: 'teleplay_id',
                    type: 'int',
                    defaultValue: 0
                },
                {
                    name: 'sequence',
                    type: 'int',
                    defaultValue: 1
                },
                'teleplay_name',
                'link'
            ]
        });

        var store = Ext.create('Ext.ux.data.NotificationStore', {
            model: 'CampaignTeleplay',
            autoLoad: true,
            autoSync: true,
            proxy: {
                url: baseUrl('data/campaign_teleplays'),
                extraParams: {
                    campaign_id: campaign_id
                }
            },
            listeners: {
                beforesync: function( options, eOpts ) {
                    var data;

                    if(options.create){
                        data = options.create[0].data;
                    }

                    if(options.update){
                        data = options.update[0].data;
                    }


                    if(data.link == '' && data.teleplay_id == 0) {
                        tools.showTips('错误', '剧集或其它地址有一个不能为空!');
                        return false;
                    }
                }
            }
        });

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    if (context.record.phantom) {
                        store.remove(context.record);
                    }
                }
            }
        });


        var grid = Ext.create('Ext.grid.Panel', {
            plugins: [rowEditing],
            width: 640,
            height: 300,
            frame: false,
            border: 0,
            store: store,
            columns: [
                {
                    text: '编号',
                    width: 40,
                    sortable: true,
                    dataIndex: 'id'
                },
                {
                    text: '剧集',
                    width: 120,
                    sortable: true,
                    dataIndex: 'teleplay_id',
                    renderer: function(v,meta,rec){
                        return v ? rec.get('teleplay_name') : '';
                    },
                    field: {
                        xtype: 'combo',
                        displayField: 'name',
                        valueField: 'id',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['id', 'name'],
                            pageSize: 300,
                            proxy: {
                                type: 'rest',
                                startParam: 'offset',
                                url: baseUrl('data/teleplays'),
                                reader: {
                                    type: 'json',
                                    root: 'results'
                                }
                            },
                            autoLoad: true
                        }),
                        queryMode: 'local',
                        typeAhead: true
                    }
                },
                {
                    header: '其它地址',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'link',
                    field: {
                        xtype: 'textfield',
                        vtype: 'url'
                    }
                },
                {
                    header: '排序值',
                    width: 80,
                    sortable: true,
                    dataIndex: 'sequence',
                    field: {
                        xtype: 'numberfield'
                    }
                },
                {
                    xtype: 'actioncolumn',
                    width: 80,
                    text: '删除剧集',
                    align: 'center',
                    items: [
                        {
                            icon: 'public/img/delete.png',  // Use a URL in the icon config
                            tooltip: '删除剧集',
                            handler: function (grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                store.remove(rec);
                            }
                        }
                    ]
                }
            ],
            tbar: ['-', {
                text: '增加',
                iconCls: 'icon-add',
                handler: function () {
                    store.insert(0, new CampaignTeleplay());
                    rowEditing.startEdit(0, 0);
                }
            }, '-'],
            bbar: Ext.create('Ext.PagingToolbar', {
                store: store,
                displayInfo: true
            })
        });

        return grid;
    }
});

Ext.widget('campaignview', {
    renderTo: 'campaign-view'
});

</script>
<div id="campaign-view"></div>