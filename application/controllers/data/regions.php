<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-21
 * Time: 下午2:40
 * To change this template use File | Settings | File Templates.
 */

class Regions extends REST_Controller {

    public function read(){
        $params = $this->get();
        $response = Region::get_by_params($params);
        $this->response($response['results'],$response['total']);
    }
}