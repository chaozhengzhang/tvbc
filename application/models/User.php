<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-26
 * Time: 下午9:32
 * To change this template use File | Settings | File Templates.
 */

class User extends ActiveRecord\Model{

    static function get_by_params($params = array())
    {
        $columns = self::connection()->columns('users');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        if (isset($params['start_time']) && isset($params['start_time'])) {
            if($params['start_time'] != '' && $params['end_time'] != '') {
                $start_time = strtotime($params['start_time']);
                $end_time = strtotime($params['end_time']);
                $conditions .= " AND (create_time>={$start_time} AND create_time<={$end_time}) ";
            }

            if($params['start_time'] != '' && $params['end_time'] == '') {
                $start_time = strtotime($params['start_time']);
                $conditions .= " AND create_time={$start_time} ";
            }


            if($params['end_time'] != '' && $params['start_time'] == '') {
                $end_time = strtotime($params['end_time']);
                $conditions .= " AND create_time={$end_time} ";
            }


            unset($params['start_time']);
            unset($params['end_time']);
        }
        foreach ($columns as $columnKey => $columnValue) {
            if (isset($params[$columnKey]) && $params[$columnKey] != '') {
                if ($columnValue == 'varchar') {
                    $conditions .= " AND $columnKey LIKE '%" . $params[$columnKey] . "%'";
                } else {
                    $conditions .= " AND $columnKey=" . $params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions' => $conditions));
        $data['results'] = array();

        $options['order'] = 'id DESC';
        if ($data['total'] > 0) {
            $activities = self::all($options);

            if ($activities) {
                $data['results'] = array_map(function ($row) {
                    $_data = $row->to_array();

                    $_data['birthday'] = date('Y-m-d',$row->birthday);
                    $_data['create_time'] = date('Y-m-d H:i:s',$row->create_time);
                    return $_data;
                }, $activities);
            }
        }

        return $data;
    }
}
