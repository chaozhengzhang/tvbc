<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-9-16
 * Time: 下午10:35
 * To change this template use File | Settings | File Templates.
 */

class Cities extends REST_Controller{

    public function read(){

        $params = $this->get();
        $province_id = $params['province_id'];

        $cities = City::all(
            array(
                'select' => 'id,name',
                'conditions' => "province_id={$province_id}"
            )
        );

        $datas = array_map(function($row){
            return $row->to_array();
        },$cities);
        $this->response($datas,count($datas));
    }
}