<div class="adshow2Div">
    <div class="adlistshow2"><img src="<?php echo base_url('public/images/32_2.png');?>"/>

        <p>&nbsp;</p>
    </div>
    <h2>合作模式</h2>

    <div class="showadMeg">
        <img src="<?php echo base_url('public/images/33_2.png');?>"/>

        <h3>视频前贴片</h3>

        <p>●视频播放页节目播放前15秒，30秒前贴片广告位<br/>
            ●广告展示方式：节目播放前不可跳过广告形式，视频格式的广告形式</p>

        <p><img src="<?php echo base_url('public/images/34_2.png');?>"/></p>
    </div>
    <div class="showadMeg">
        <img src="<?php echo base_url('public/images/35_2.png');?>"/>

        <h3>角标</h3>

        <p>抢占用户观看过程中的唯一展示空间</p>

        <p style="color:#f00; margin-top:110px;">注：节目播放后5秒自动出现，展示时间每3分钟一次循环至节目播放结束，<br/>全屏播放时依然可见</p>
    </div>
    <div class="showadMeg">
        <img src="<?php echo base_url('public/images/35_2.png');?>"/>

        <h3>暂停广告&amp;Mini Banner</h3>

        <p>品牌与产品结合，为品牌提供丰富的呈现空间</p>

        <p style="color:#f00; margin-top:40px;">注一：每个广告素材最多展示10分钟，每10分钟自动更换广告主</p>

        <p style="color:#f00; margin-top:30px;">注二：暂停广告：节目播放中用户主动暂停节目时出现的图片广告，用户取消暂停后广告消失</p>
    </div>
    <div class="showadMeg">
        <img src="<?php echo base_url('public/images/37_2.png');?>"/>

        <h3>TVB平道首页Banner广告</h3>

        <p>点击链接，品牌丰富内容直达</p>

        <p style="color:#f00; margin-top:40px;">注一：首页Banner广告，客户网站直达，可每周更换素材</p>
    </div>
    <div class="clearboth"></div>
    <h2>广告刊例</h2>

    <div style="padding:10px; margin-bottom:10px;"><img src="<?php echo base_url('public/images/38_2.png');?>"/></div>
    <h2>联系我们</h2>

    <div class="adcontant">联系人：朱洁静 Jessie Zhu电话：+86-021-5405 1151 邮箱：jessiezhu@tvbc.com.cn</div>
</div>