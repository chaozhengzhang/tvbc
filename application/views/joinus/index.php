<script language="javascript" type="text/javascript">

    $(function () {
        $("ul.menu li:first-child").addClass("current");
        $("div.content").find("div.layout:not(:first-child)").hide();
        $("div.content div.layout").attr("id", function () {
            return idNumber("No") + $("div.content div.layout").index(this)
        })
        $("ul.menu li").click(function () {
            var c = $("ul.menu li");
            var index = c.index(this);
            var p = idNumber("No");
            show(c, index, p);
        })
        function show(controlMenu, num, prefix) {
            var content = prefix + num;
            $('#' + content).siblings().hide();
            $('#' + content).show();
            controlMenu.eq(num).addClass("current").siblings().removeClass("current");
        }
        function idNumber(prefix) {
            var idNum = prefix;
            return idNum;
        }

        $('.region li').click(function(){
            var data_id = $(this).attr('data-id');
            $('.region li').removeClass("current");
            $(this).addClass('current');

            $('.job-box').hide();
            $('#box-' + data_id).show();
        });
    });
</script>
<style type="text/css">
    .box {
        font-size: 12px;
        float: left;
        width: 100%;
    }
    .tagMenu, .cityshowDiv {
        float: left;
        width: 100%;
        margin-bottom: 10px;
    }
    .tagMenu ul, .cityshowDiv ul {
        float: left;
        width: 100%;
    }
    ul.menu li, ul.menu3 li {
        float: left;
        padding: 0 50px;
        cursor: pointer;
        background: #999;
        color: #333;
        height: 32px;
        line-height: 32px;
        margin: 0px 8px;
        border-radius: 4px;
    }
    ul.menu li.current, ul.menu3 li.current {
        background: #fff;
        background: #fff;
        color: #333;
    }
    ul.menu2 li {
        padding: 0px 10px;
        margin: 2px 4px;
		white-space:nowrap;
    }
    .content {
        padding: 0px 0px
    }
</style>
<div class="jionusDiv">
    <div style="padding:0 10px 10px 10px;">
        <p style="text-indent:24px; font-size:14px;"><strong>上海翡翠东方传播有限公司（TVBC）</strong>将致力于多元化发展，将根植中国文化土壤，继承TVB优良血统，以“传承娱乐精髓、弘融华人生活”为核心理念。独家运作并展开TVB
            在中国内地的各项业务，助力TVB实现从以香港业务为发展核心到以大中国业务为发展前沿的战略转移。 </p>
        <p style="text-indent:24px; font-size:14px;">
            融合和创新是我们寻找人才的原动力。我们需要有开放性思维和求知欲的人才，和我们一起锐意开拓，引领创新。在这里，有才干、有抱负的一流团队将为达成共同的目标而奋斗。我们要找的就是您！带着您的想法让我们来缔造对全中国乃至全球华人最具影响力的华语媒体帝国！ </p>
    </div>
    <h2>职业发展</h2> <br/>
    <div class="cityshowDiv">
        <ul class="menu3 region">
            <?php
                foreach ($regions as $key=>$region) {
            ?>
                    <li class="<?php echo $key == 0 ? 'current': '';?>" data-id="<?php echo $region->id;?>">
                        <?php echo $region->name;?>
                    </li>
            <?php
                }
            ?>
        </ul>
    </div>
    <div class="clearboth"></div>
    <br/>
    <h2>招聘职位</h2>
    <?php
        if(isset($joins)) {
            foreach ($joins as $key => $join) {
    ?>
    <div class="box job-box" id="box-<?php echo $key;?>" style="display:<?php echo $key == 1 ? '' : 'none';?> ;">
        <div class="tagMenu">        
            <ul class="menu menu2">
                <?php
                foreach ($join as $data) {
                    ?>
                    <li><?php echo $data->name;?></li>
                <?php
                }
                ?>
            </ul>
        </div>

        <div class="content">
            <?php
            foreach ($join as $data) {
                ?>       
                <div class="layout">
                    <h2>岗位职责</h2>
                    <?php echo $data->obligation;?>
                    <?php if(!empty($data->demand)){?>
                    <h2>任职要求</h2> 
                    <?php
                        echo $data->demand;
                    }
                    ?>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php
            }
        }
    ?>
    <div style="padding-top:20px;">
        请将个人简历投递至招聘信箱：<a href="maito:recruitment@tvbc.com.cn">recruitment@tvbc.com.cn</a>
    </div>
</div>