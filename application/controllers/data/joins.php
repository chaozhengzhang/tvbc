<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-21
 * Time: 下午7:44
 * To change this template use File | Settings | File Templates.
 */

class Joins extends REST_Controller {


    public function read()
    {
        $params = $this->get();
        $response = Join::get_by_params($params);
        $this->response($response['results'],$response['total']);
    }

    public function update($id) {
        $model = Join::find($id);
        $data = $this->put('data');
        unset($data['position_name']);
        unset($data['region_name']);
        if($model) {
            unset($data['id']);
            //if(!empty($data['position_id'])) {
                $model->update_attributes($data);
            //}
        }

        $response = $model->to_array();
        $response['position_name'] = $model->position->name;
        $response['region_name'] = $model->region->name;
        $this->response($response,1,'编辑成功！');
    }

    public function create(){
        $data = $this->post('data');
        unset($data['position_name']);
        unset($data['region_name']);
        if(!empty($data['position_id'])) {
            $model = Join::create($data);
            $response = $model->to_array();
            $response['position_name'] = $model->position->name;
            $response['region_name'] = $model->region->name;
            $this->response($response,1,'增加成功！');
        } else {
            $this->response(NULL,1,'职位不能为空！',FALSE);
        }
    }

    public function destroy($id = 0){
        $model = Join::find($id);
        if($model) {
            $model->delete();
        }
        $this->response(NULL,1,'删除成功！');
    }
}