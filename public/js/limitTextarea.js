/**
 * Created by zhang.chaozheng on 14-1-16.
 */
(function($){
    $.fn.limitTextarea = function(opts){
        var defaults = {
            maxNumber:150,//允许输入的最大字数
            onOk:function(){},//输入后，字数未超出时调用的函数
            onOver:function(){}//输入后，字数超出时调用的函数
        };
        var option = $.extend(defaults,opts);
        this.each(function(){
            var _this = $(this);
            var info = $(option.info_limit);
            info.html(option.maxNumber- _this.val().length);

            var fn = function(){
                var extraNumber = option.maxNumber - _this.val().length;
                if(extraNumber>=0){
                    info.html(extraNumber);
                    option.onOk();
                }
                else{
                    info.html((-extraNumber));
                    option.onOver();
                }
            };

            //绑定输入事件监听器
            if(window.addEventListener) { //先执行W3C
                _this.get(0).addEventListener("input", fn, false);
            } else {
                _this.get(0).attachEvent("onpropertychange", fn);
            }
            if($.browser.msie && /msie 9\.0/i.test(userAgent)) { //IE9
                _this.get(0).attachEvent("onkeydown", function() {
                    var key = window.event.keyCode;
                    (key == 8 || key == 46) && fn();//处理回退与删除
                });
                _this.get(0).attachEvent("oncut", fn);//处理粘贴
            }
        });
    }
})(jQuery);