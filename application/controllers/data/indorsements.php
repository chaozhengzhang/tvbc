<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-1-18
 * Time: 下午5:06
 */

class Indorsements extends REST_Controller {

    public function create(){
        $data = $this->post();
        $data['user_id'] = $this->user['id'] ? $this->user['id'] : 0;
        $data['ip_address'] = ip2long(get_ip_address());
        $data['create_time'] = time();
        $indorsement = Indorsement::create($data);
        $count = Indorsement::count(array(
            'conditions' => 'comment_id='.$indorsement->comment_id
        ));
        echo '{"info":'.$count.',"status":"y"}';
    }
} 