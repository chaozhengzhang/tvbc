var tools = {
    showTips: function(title,message){
        Ext.widget('uxNotification', {
            position: 't',
            cls: 'ux-notification-light',
            iconCls: 'ux-notification-icon-information',
            closable: true,
            title: title,
            manager: 'fullscreen',
            slideInDuration: 500,
            autoCloseDelay: 3000,
            html: message
        }).show();
    }
};