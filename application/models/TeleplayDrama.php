<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-17
 * Time: 下午11:58
 * To change this template use File | Settings | File Templates.
 */

class TeleplayDrama extends ActiveRecord\Model {

    static $belongs_to = array(
        array('part','class_name' => 'TeleplayPart')
    );

    static function get_by_params($params = array()){
        $columns = self::connection()->columns('teleplay_dramas');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }



        $conditions = ' 1=1 ';

        foreach($columns as $columnKey=>$columnValue) {
            if(isset($params[$columnKey])) {
                if($columnValue->raw_type == 'varchar') {
                    $conditions .=  " AND $columnKey LIKE '%".$params[$columnKey]."%'";
                } else {
                    $conditions .=  " AND $columnKey=".$params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions'=>$conditions));
        $data['results'] = array();
        $options['order'] = "teleplay_dramas.leave DESC";
        if($data['total'] > 0) {
            $activities = self::all($options);

            if($activities) {
                $data['results'] = array_map(function($row){
                    return $row->to_array();
                },$activities);
                foreach ($activities as $key => $activitie) {
                    $data['results'][$key] = $activitie->to_array();
                }

            }
        }

        return $data;
    }
}