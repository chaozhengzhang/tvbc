<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-22
 * Time: 下午8:40
 * To change this template use File | Settings | File Templates.
 */

class Systems extends CI_Controller {

    public function sliders()
    {
        $this->load->view('admins/systems/sliders');
    }

    public function video()
    {
        $this->load->view('admins/systems/video');
    }

    public function setting(){
        $this->load->view('admins/systems/setting');
    }
}