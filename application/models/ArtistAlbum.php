<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-16
 * Time: 下午8:40
 * To change this template use File | Settings | File Templates.
 */

class ArtistAlbum extends ActiveRecord\Model {

    static $before_destroy = array('delete_image');

    public function delete_image()
    {
        @unlink('./uploads/artists/albums/big/'.$this->file_name);
        @unlink('./uploads/artists/albums/small/'.$this->file_name);
    }

    static function get_by_params($params = array()){
        $columns = self::connection()->columns('artist_albums');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        foreach($columns as $columnKey=>$columnValue) {
            if(isset($params[$columnKey])) {
                if($columnValue->raw_type == 'varchar') {
                    $conditions .=  " AND $columnKey LIKE '%".$params[$columnKey]."%'";
                } else {
                    $conditions .=  " AND $columnKey=".$params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions'=>$conditions));
        $data['results'] = array();

        if($data['total'] > 0) {
            $activities = self::all($options);

            if($activities) {
                $data['results'] = array_map(function($row){
                    return $row->to_array();
                },$activities);
            }
        }

        return $data;
    }
}