<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-15
 * Time: 下午11:51
 * To change this template use File | Settings | File Templates.
 */

class Welcome extends CI_Controller{

    public function index(){
        $admin = $this->session->userdata('admin');
        if(isset($admin)) {
            $this->load->view('admins/index');
        } else {
            redirect(base_url('admin/signin'));
        }
    }
}
