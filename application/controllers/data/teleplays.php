<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-17
 * Time: 下午9:23
 * To change this template use File | Settings | File Templates.
 */

class Teleplays extends REST_Controller{

    public function read(){
        $params = $this->get();

        $response = Teleplay::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function create(){
        $data = $this->post('data');
        if(Teleplay::find_by_name($data['name'])){
            $this->response(NULL,0,'该剧集已经存在！',FALSE);
        } else {
            $response = Teleplay::create($data);
            $this->response($response->to_array(),1,'增加成功！');
        }
    }

    public function upload_poster()
    {
        $this->load->library('upload');
        $directorys = array(
            './uploads/teleplays/poster/big/',
            './uploads/teleplays/poster/small/',
        );

        $sizes = array(
            array(
                'width' => '401',
                'height' => '305',
            ),
            array(
                'width' => '150',
                'height' => '110',
            ),
        );
        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
            $this->load->library('image_lib');
            $thumb_config['image_library'] = 'gd2';
            $thumb_config['source_image'] = $directorys['0'] . $data['file_name'];
            $thumb_config['create_thumb'] = false;
            $thumb_config['maintain_ratio'] = false;

            foreach ($directorys as $key => $directory) {
                $thumb_config['width'] = $sizes[$key]['width'];
                $thumb_config['height'] = $sizes[$key]['height'];
                $thumb_config['new_image'] = $directory . $data['file_name'];
                $this->image_lib->initialize($thumb_config);
                $this->image_lib->resize();
            }

            $data['src'] = "/uploads/teleplays/poster/small/".$data['file_name'];
        }


        $this->response($data, 1, '上传成功！');
    }

    public function update($id = 0){
        $teleplay = Teleplay::find($id);
        /*@unlink('./uploads/teleplays/poster/big/'.$teleplay->poster);
        @unlink('./uploads/teleplays/poster/small/'.$teleplay->poster);*/
        $data = $this->put('data');
        $teleplay->update_attributes($data);
        $this->response($teleplay->to_array(),1,'修改成功！');
    }

    public function destroy($id = 0){
        $teleplay = Teleplay::find($id);
        $teleplay->delete();
        $this->response(NULL,1,'修改成功！');
    }
}