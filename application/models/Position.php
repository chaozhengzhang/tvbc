<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-21
 * Time: 下午2:39
 * To change this template use File | Settings | File Templates.
 */

class Position extends  ActiveRecord\Model{

    static  $has_many = array(
        array('joins')
    );

    static function get_by_params($params = array()){
        $columns = self::connection()->columns('positions');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        foreach($columns as $columnKey=>$columnValue) {
            if(isset($params[$columnKey])) {
                if($columnValue->raw_type == 'varchar') {
                    $conditions .=  " AND $columnKey LIKE '%".$params[$columnKey]."%'";
                } else {
                    $conditions .=  " AND $columnKey=".$params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions'=>$conditions));
        $data['results'] = array();

        if($data['total'] > 0) {
            $options['order'] = 'ranking ASC';
            $activities = self::all($options);

            if($activities) {
                $data['results'] = array_map(function($row){
                    return $row->to_array();
                },$activities);
            }
        }

        return $data;
    }
}