<link type="text/css" rel="stylesheet" href="<?php echo base_url('public/plugin/validform/css/validform.css'); ?>">
<script type="text/javascript"
        src="<?php echo base_url('public/plugin/validform/js/Validform_v5.3.2_min.js'); ?>"></script>
<div class="regDiv">
    <h2>忘记密码</h2>

    <div class="forgetDiv">
        <form class="forgot-password" method="post" action="<?php echo base_url('data/users/forgot'); ?>">
            <h3>请输入您注册的用户名和注册时填写的电子邮件地址</h3>

            <p>用户名<br/>
                <input name="username" type="text" datatype="*" class="inputxt" type="text" nullmsg="用户名不能为空"/>
                <span class="Validform_checktip"></span>
            </p>

            <p>电子邮件地址<br/>
                <input name="email" type="text" datatype="e" class="inputxt" nullmsg="电子邮箱不能为空" type="text"/>
                <span class="Validform_checktip"></span>
            </p>
            <input type="submit" value="提交" class="tijiaobtn"/>
        </form>
        <div class="clearboth"></div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $(".forgot-password").Validform({
            tiptype: function (msg, o, cssctl) {
                if (!o.obj.is("form")) {
                    var objtip = o.obj.siblings(".Validform_checktip");
                    cssctl(objtip, o.type);
                    objtip.text(msg);
                }
            },
            showAllError: true,
            ajaxPost: true,
            callback: function (data) {
                if (data.status == 'y') {
                    window.location.href = "/users/forgot_success";
                }
            }
        });
    });
</script>