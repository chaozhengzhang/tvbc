<style>
    #albums-view .x-panel-body{
        background: white;
    }
    #albums-view .thumb{
        background: #dddddd;
        padding: 3px;
        padding-bottom: 0;
    }

    .x-quirks #albums-view .thumb {
        padding-bottom: 3px;
    }

    #albums-view .thumb img{
        height: 160px;
        width: 107px;
    }
    #albums-view .thumb-wrap{
        float: left;
        margin: 4px;
        margin-right: 0;
        padding: 5px;
    }
    #albums-view .thumb-wrap span {

        display: block;
        overflow: hidden;
        text-align: center;
        width: 110px; /* for ie to ensure that the text is centered */
    }

    #albums-view .x-item-over{
        border:1px solid #dddddd;
        background: #efefef url(/public/img/over.gif) repeat-x left top;
        padding: 4px;
    }

    #albums-view .x-item-selected{
        background: #eff5fb url(/public/img/selected.gif) no-repeat right bottom;
        border:1px solid #99bbe8;
        padding: 4px;
    }
    #albums-view .x-item-selected .thumb{
        background:transparent;
    }

    #albums-view .loading-indicator {
        font-size:11px;
        background-image:url("/public/img/delete.png");;
        background-repeat: no-repeat;
        background-position: left;
        padding-left:20px;
        margin:10px;
    }

    .x-view-selector {
        position:absolute;
        left:0;
        top:0;
        width:0;
        border:1px dotted;
        opacity: .5;
        -moz-opacity: .5;
        filter:alpha(opacity=50);
        zoom:1;
        background-color:#c3daf9;
        border-color:#3399bb;
    }.ext-strict .ext-ie .x-tree .x-panel-bwrap{
         position:relative;
         overflow:hidden;
     }
</style>
<script type="text/javascript">
    Ext.define('Ext.ux.AlbumView',{
        extend: 'Ext.Panel',
        alias: 'widget.albumview',
        frame: true,
        initComponent:function(config){
            var me = this;
            Ext.apply(me,config || {});

            me.store = Ext.create( 'Ext.ux.data.NotificationStore', {
                model : 'ArtistAlbum',
                autoLoad : false,
                autoSync : true,
                proxy : {
                    url : baseUrl('data/artist_albums')
                }
            });

            me.tbar = ['-',{
                xtype: 'combo',
                fieldLabel: '艺人',
                labelAlign: 'right',
                displayField: 'name',
                valueField: 'id',
                store: Ext.create( 'Ext.data.Store', {
                    fields: ['id','name'],
                    pageSize: 300,
                    proxy : {
                        type : 'rest',
                        startParam: 'offset',
                        url: baseUrl('data/artists'),
                        reader : {
                            type : 'json',
                            root : 'results'
                        }
                    },
                    autoLoad : true
                } ),
                queryMode: 'local',
                typeAhead: true,
                listeners: {
                    select: function(combo){
                        me.store.proxy.extraParams.artist_id = combo.value;
                        me.store.load();
                    }
                }
            },'-',{
                text: '照片选择',
                handler: function(){
                    me.uploadAlbum(this.previousNode('combo').getValue());
                }
            },'-',{
                text: '删除选中',
                handler: function(btn,e){
                    var selection = me.dataView.getSelectionModel().getSelection();
                    if(selection[0]) me.store.remove(selection[0]);
                }
            }];

            me.initDataView();
            me.items = [me.dataView];
            me.callParent(arguments);
        },
        uploadAlbum: function(artist_id){
            var me = this,win;
            if(!artist_id) {
                return false;
            }
            if(!win) {
                win = Ext.widget('window',{
                    height: 130,
                    width: 400,
                    modal: true,
                    items:[{
                        xtype: 'form',
                        bodyPadding: 5,
                        border: 0,
                        defaults: {
                            anchor: '100%',
                            allowBlank: false,
                            labelAlign: 'right'
                        },
                        items: [{
                            xtype: 'hidden',
                            name: 'artist_id',
                            value: artist_id
                        },{
                            xtype: 'filefield',
                            fieldLabel: '照片',
                            allowBlank: false,
                            name: 'userfile'
                        }],
                        buttonAlign: 'center',
                        buttons: [{
                            text: '上传',
                            handler: function(){
                                var form = this.up('form').getForm();
                                if(form.isValid()) {
                                    form.submit({
                                        url: baseUrl('data/artist_albums/upload_image'),
                                        success: function(form, action) {
                                            tools.showTips('提示',action.result.message);
                                            me.store.proxy.extraParams.artist_id = artist_id;
                                            me.store.load();
                                            win.close();
                                        },
                                        failure: function(form, action) {
                                            switch (action.failureType) {
                                                case Ext.form.action.Action.CLIENT_INVALID:
                                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                                    break;
                                                case Ext.form.action.Action.CONNECT_FAILURE:
                                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                                    break;
                                                case Ext.form.action.Action.SERVER_INVALID:
                                                    Ext.Msg.alert('Failure', action.result.message);
                                            }
                                        }
                                    });
                                }
                            }
                        }]
                    }]
                }).show();
            }
        },
        initDataView: function(){
            var me = this;
            me.dataView = Ext.create('Ext.view.View', {
                store: me.store,
                border: 0,
                tpl: [
                    '<tpl for=".">',
                    '<div class="thumb-wrap">',
                    '<div class="thumb"><img src="uploads/artists/albums/small/{file_name}" title="{file_name}"></div>',
                    '</div>',
                    '</tpl>',
                    '<div class="x-clear"></div>'
                ],
                multiSelect: false,
                height: 410,
                trackOver: true,
                overItemCls: 'x-item-over',
                itemSelector: 'div.thumb-wrap',
                emptyText: 'No images to display'
            });
        }
    });

    Ext.widget('albumview',{
        title: '艺人相册',
        id: 'albums-view',
        renderTo: 'album-view-panel'
    });


</script>
<div id="album-view-panel"></div>