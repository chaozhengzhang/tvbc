<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-16
 * Time: 下午8:40
 * To change this template use File | Settings | File Templates.
 */

class Artist_albums extends REST_Controller {

    public function read(){
        $params = $this->get();

        $response = ArtistAlbum::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function upload_image(){
        $data = $this->input->post();
        $artist_id = $data['artist_id'];
        $this->load->library('upload');
        $directorys = array(
            './uploads/artists/albums/big/',
            './uploads/artists/albums/small/',
        );
        $response = array();
        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys[0];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
//$info_banners
            $this->load->library('image_lib');
            $thumb_config['image_library'] = 'gd2';
            $thumb_config['source_image'] = $directorys[0] . $data['file_name'];
            $thumb_config['create_thumb'] = false;
            $thumb_config['maintain_ratio'] = true;
            $thumb_config['master_dim'] = 'width';

            $thumb_config['width'] = 107;
            $thumb_config['new_image'] = $directorys[1] . $data['file_name'];
            $this->image_lib->initialize($thumb_config);
            $this->image_lib->resize();
            $albums = ArtistAlbum::create(array(
                'artist_id' => $artist_id,
                'file_name' => $data['file_name'],
            ));
            $response = $albums->to_array();
        }

        $this->response($response,1,'上传成功！');
    }

    public function destroy($id)
    {
        $albums = ArtistAlbum::find($id);

        if($albums) {
            $albums->delete();
        }

        $this->response(NULL,1,'删除成功！');
    }
}