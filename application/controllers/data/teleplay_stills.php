<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-17
 * Time: 下午10:28
 * To change this template use File | Settings | File Templates.
 */

class Teleplay_stills extends REST_Controller {


    public function read(){
        $params = $this->get();

        $response = TeleplayStill::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function upload_image(){
        $data = $this->input->post();
        $teleplay_id = $data['teleplay_id'];
        $this->load->library('upload');
        $directorys = array(
            './uploads/teleplays/stills/',
        );
        $response = array();
        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();

            $this->load->library('image_lib');
            $thumb_config['image_library'] = 'gd2';
            $thumb_config['source_image'] = $directorys[0] . $data['file_name'];
            $thumb_config['create_thumb'] = FALSE;
            $thumb_config['maintain_ratio'] = FALSE;


            $thumb_config['width'] = 170;
            $thumb_config['height'] = 115;
            $thumb_config['new_image'] = $directorys[0] .$data['file_name'];
            $this->image_lib->initialize($thumb_config);
            $this->image_lib->resize();

            $stills = TeleplayStill::create(array(
                'teleplay_id' => $teleplay_id,
                'file_name' => $data['file_name'],
            ));
            $response = $stills->to_array();
        }

        $this->response($response,1,'上传成功！');
    }

    public function destroy($id)
    {
        $stills = TeleplayStill::find($id);

        if($stills) {
            $stills->delete();
        }

        $this->response(NULL,1,'删除成功！');
    }
}