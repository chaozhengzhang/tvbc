<script type="text/javascript">
    Ext.define('Ext.ux.DramaView', {
        extend: 'Ext.grid.Panel',
        alias: 'widget.dramaview',
        title: '剧集播放地址',
        columnLines: true,
        frame: true,
        minHeight: 320,
        maxHeight: 540,
        initComponent: function (config) {
            var me = this;
            Ext.apply(me, config || {});

            me.store = Ext.create('Ext.ux.data.NotificationStore', {
                model: 'TeleplayDrama',
                autoLoad: false,
                autoSync: true,
                proxy: {
                    url: baseUrl('data/teleplay_dramas')
                }
            });

            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                errorSummary: false,
                clicksToEdit: 2,
                listeners: {
                    cancelEdit: function(rowEditing, context) {
                        if (context.record.phantom) {
                            me.store.remove(context.record);
                        }
                    }
                }
            });

            me.plugins= [rowEditing];

            me.tbar = ['-', {
                xtype: 'combo',
                fieldLabel: '影视剧',
                labelAlign: 'right',
                labelWidth: 60,
                displayField: 'name',
                valueField: 'id',
                store: Ext.create('Ext.data.Store', {
                    fields: ['id', 'name'],
                    pageSize: 300,
                    proxy: {
                        type: 'rest',
                        startParam: 'offset',
                        url: baseUrl('data/teleplays'),
                        reader: {
                            type: 'json',
                            root: 'results'
                        }
                    },
                    autoLoad: true
                }),
                queryMode: 'local',
                typeAhead: true,
                listeners: {
                    select: function (combo) {
                        me.store.proxy.extraParams.teleplay_id = combo.value;
                        me.store.load();
                    }
                }
            }, '-', {
                text: 'Add',
                iconCls: 'icon-add',
                handler: function(){
                    var teleplay_id = this.previousNode('combo').getValue();
                    if(teleplay_id) {
                        me.store.insert(0, {
                            teleplay_id: teleplay_id
                        });
                        rowEditing.startEdit(0, 0);
                    } else {
                        tools.showTips('提示','请先选择影视剧!');
                    }
                }
            }, '-'];


            me.columns =  [{
                text: '编号',
                width: 80,
                sortable: true,
                dataIndex: 'id'
            }, {
                text: '集数',
                width: 80,
                sortable: true,
                dataIndex: 'leave',
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    value: 1,
                    minValue: 1
                }
            }, {
                text: '播放地址',
                flex: 1,
                sortable: true,
                dataIndex: 'url',
                editor: {
                    allowBlank: false
                }
            },{
                text: '来源',
                width:120,
                dataIndex: 'type',
                renderer: function(v) {
                    return v == 1 ? '土豆' :'优酷';
                },
                editor: {
                    xtype: 'combo',
                    displayField: 'name',
                    valueField: 'id',
                    store: Ext.create('Ext.data.Store', {
                        autoDestroy: true,
                        fields: ['id','name'],
                        data: [
                            {
                                id: 1,
                                name: '土豆'
                            },
                            {
                                id: 2,
                                name: '优酷'
                            }
                        ]
                    }),
                    queryMode: 'local',
                    typeAhead: true
                }
            },{
                xtype: 'actioncolumn',
                width: 60,
                header: '操作',
                align: 'center',
                items: [
                    {
                        icon: baseUrl('public/img/delete.png'),  // Use a URL in the icon config
                        tooltip: '删除餐厅',
                        iconCls: 'icon-action-margin',
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            grid.getStore().remove(rec);
                        }
                    }
                ]
            }];

            me.bbar = Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true
            });
            me.callParent(arguments);
        }
    });

    Ext.widget('dramaview', {
        renderTo: 'dramas-view-panel'
    });


</script>
<div id="dramas-view-panel"></div>