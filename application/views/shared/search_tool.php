<div class="r-Div searchNews">
    <h3>新闻搜索</h3>
    <div class="searchMeg">
        <form action="<?php echo base_url('news');?>" method="get">
            <input type="text" class="typetext" name="keyword" />
            <input type="submit" value="搜 索" class="searchbtn" />
        </form>
    </div>
    <h3>热点新闻</h3>
    <div class="hotnewsDiv">
        <ul>
            <?php
            if(isset($infos)){
                foreach($infos as $info){
                    ?>
                    <li>
                        <a href="<?php echo base_url('news/show/'.$info->id);?>" title="<?php echo $info->title;?>">
                            <?php echo character_cn_limiter(strip_tags($info->title), 18);?>
                        </a>
                    </li>
                <?php
                }
            }
            ?>
        </ul>
    </div>
</div>