var letter = [
    {name: 'A'},
    {name: 'B'},
    {name: 'C'},
    {name: 'D'},
    {name: 'E'},
    {name: 'F'},
    {name: 'G'},
    {name: 'H'},
    {name: 'I'},
    {name: 'J'},
    {name: 'K'},
    {name: 'L'},
    {name: 'M'},
    {name: 'N'},
    {name: 'O'},
    {name: 'P'},
    {name: 'Q'},
    {name: 'R'},
    {name: 'S'},
    {name: 'T'},
    {name: 'U'},
    {name: 'V'},
    {name: 'W'},
    {name: 'X'},
    {name: 'Y'},
    {name: 'Z'}
];
var constellation = [
    {name: '白羊座'},
    {name: '金牛座'},
    {name: '双子座'},
    {name: '巨蟹座'},
    {name: '狮子座'},
    {name: '处女座'},
    {name: '天秤座'},
    {name: '天蝎座'},
    {name: '射手座'},
    {name: '魔羯座'},
    {name: '水瓶座'},
    {name: '双鱼座'}
];


