<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-22
 * Time: 下午8:44
 * To change this template use File | Settings | File Templates.
 */

class Sliders extends REST_Controller{

    public function read()
    {
        $params = $this->get();

        $response = Slider::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function create(){
        $data = $this->post('data');
        $response = Slider::create($data);
        $this->response($response->to_array(),1,'增加成功！');
    }

    public function update($id = 0)
    {
        $model = Slider::find($id);
        $params = $this->put('data');
        unset($params['info_name']);
        $model->update_attributes($params);
        $this->response($model->to_array(),1,'编辑成功！');
    }

    public function destroy($id){
        $model = Slider::find($id);
        $model->delete();
        $this->response(NULL,1,'删除成功！');
    }

    public function upload_image(){
        $this->load->library('upload');
        $directorys = array(
            './uploads/sliders/'
        );
        $id = $this->post('id');
        if($id > 0) {
            $slider = Slider::find($id);
            $image = $slider->image;
        }

        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
            if($id > 0)
                @unlink($directorys[0].$image);
        }

        $this->response($data,1,'上传成功！');
    }
}