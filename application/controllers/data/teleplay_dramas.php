<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-17
 * Time: 下午11:58
 * To change this template use File | Settings | File Templates.
 */

class Teleplay_dramas extends REST_Controller {

    public function read()
    {
        $params = $this->get();

        $response = TeleplayDrama::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function create()
    {
        $data = $this->post('data');
        $success = TRUE;
        $message = '增加成功！';
        $response = array();
        try{
            if(TeleplayDrama::find_by_url(trim($data['url']))){
                $success = FALSE;
                $message = '改链接已经存在！';
            } else {
                $drama = TeleplayDrama::create($data);
                $response = $drama->to_array();
            }
        } catch (ActiveRecord\ActiveRecordException $ex) {
            $response = $ex;
            $success = FALSE;
            $message = '剧集集数不能重复！';
        }


        $this->response($response,1,$message,$success);
    }

    public function update($id)
    {
        $params = $this->put('data');
        $model = TeleplayDrama::find($id);
        if($model) {
            $model->update_attributes($params);
        }

        $this->response($model->to_array(),1,'编辑成功！');
    }

    public function destroy($id)
    {
        $model = TeleplayDrama::find($id);

        if($model) {
            $model->delete();
        }

        $this->response(NULL,1,'删除成功！');
    }
}