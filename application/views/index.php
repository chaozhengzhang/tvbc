<script src="<?php echo base_url('public/js/jquery.slides.js'); ?>" type="text/javascript"></script>
<div style=" overflow:hidden; display:;" class="indexflashDiv">
<div id="slider">
     <div id="slider_name" class="slider_box">
         <ul class="silder_con">
             <?php
                if(isset($sliders)) {
                    foreach ($sliders as $slider) {
             ?>
             <li class="silder_panel clearfix">
                 <a onclick="javascript: pageTracker._trackPageview ('Home page/Carousel/<?php echo $slider['id']; ?>');" class="f_l" href="<?php echo $slider['link'];?>" title="<?php echo $slider['title'];?>"><img src="<?php echo base_url('uploads/sliders/'.$slider['image']);?>"></a>
             </li>
             <?php
                    }
                }
             ?>
         </ul>
         <a class="prev" style="opacity: 0.2;">Prev</a><a class="next" style="opacity: 0.2;">Next</a></div>
     <div class="silderBox"></div>
     <div class="clearboth"></div>
     <div class="indeximg">
         <ul>
             <?php
                if(isset($info_banners)) {
                    $k = 1;
                    foreach($info_banners as $info_banner) {
                        $link = $info_banner['other_link'];
                        if(isset($info_banner['info_id']) && $info_banner['info_id'] > 0) {
                            $link = base_url('news/show/'.$info_banner['info_id']);
                        }
             ?>
                <li>
                    <a href="<?php echo $link;?>" onclick="javascript: pageTracker._trackPageview ('Home Site/BottomBanner/Banner<?php echo $k;?>');">
                        <img src="<?php echo base_url('uploads/infobanners/'.$info_banner['image']);?>" width="321" height="151" />
                    </a>
                </li>
             <?php
                        $k++;
                    }
                }
             ?>
             <li class="erweima"><a href="http://tvbc.com.cn/downloads" onclick="javascript: pageTracker._trackPageview ('Home Site/BottomBanner/Banner3');" target="_blank"><img src="/public/images/indeximg6.png" /></a></li>
         </ul>
     </div>
     <div class="clearboth"></div>
 </div>
</div>
