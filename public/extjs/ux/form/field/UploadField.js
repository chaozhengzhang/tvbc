/**
 * Created with JetBrains PhpStorm.
 * User: zcz
 * Date: 12-12-1
 * Time: 下午8:54
 * To change this template use File | Settings | File Templates.
 */
Ext.define( 'Ext.ux.form.field.UploadField', {
    extend : 'Ext.container.Container',
    alias : ['widget.uploadfield'],
    width : 210,
    queued : false, //允许多个上传时开启
    allowBlank : true,
    fieldLabel : '',
    labelWidth: 100,
    buttonText : 'Browse...',
    debug : false,
    showTexField: false,
    showImage: false,
    fileType : '*',
    isSubmitValue: false,
    filePostName : 'userfile',
    resultName : 'file_name',
    initComponent : function ( config ) {
        config = config || {};
        var me = this;
        Ext.apply( me, config );
        me.swfSettings = {
            upload_url : '',
            flash_url : '',
            file_post_name : me.filePostName,
            use_query_string : false,
            requeue_on_error : false,
            http_success : [201, 202],
            file_types : "*.*",
            file_types_description : "All Files",
            file_size_limit : 0,
            file_upload_limit : 0,
            file_queue_limit : me.queued ? 0 : 1,
            debug : me.debug,
            button_image_url : "",
            button_width : 61,
            button_height : 22,
            button_text_style : '',
            button_text_left_padding : 0,
            button_text_top_padding : 0,
            button_disable : me.queued ? false : true,
            button_cursor : SWFUpload.CURSOR.HAND,
            button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT,
            button_action : me.queued ? SWFUpload.BUTTON_ACTION.SELECT_FILES : SWFUpload.BUTTON_ACTION.NONE,
            custom_settings : {
                scope : me
            }
        };

        Ext.apply( me.swfSettings, me.setting || {} );

        me.valueField = Ext.create( 'Ext.form.field.Hidden', {
            name : me.name,
            submitValue: me.isSubmitValue,
            allowBlank : true,
            listeners : {
                scope : me,
                'change' : function ( field, newValue, oldValue, eOpts ) {
                    if ( me.fileType == 'image' && !me.queued ) {
                        var data = {};
                        var fields = this.viewStore.model.getFields();
                        for ( var i = 0; i < fields.length; i++ ) {
                            data[fields[i].name] = newValue;
                        }
                        me.setIamgeSrc( data );
                        if ( newValue == '' || !newValue ) {
                            me.IamgeView.hide();
                        }
                    }
                }
            }
        } );

        me.items = [
            {
                xtype : 'container',
                layout : {
                    type : 'hbox'
                },
                items : [
                    {
                        xtype : 'textfield',
                        itemId : 'displayFiel',
                        allowBlank : true,
                        flex: 1,
                        hidden: me.showTexField,
                        submitValue: false,
                        fieldLabel : me.fieldLabel,
                        labelWidth : me.labelWidth
                    },
                    {
                        xtype : 'progressbar',
                        itemId : 'progressbar',
                        hidden : true,
                        text : 'Ready'
                    },
                    {
                        xtype : 'button',
                        text : me.buttonText,
                        margin : '2 10 0 10'
                    },
                    {
                        xtype: 'imagecomponent',
                        width: 100,
                        hidden: true,
                        height: 100,
                        itemId: 'imagecomponent',
                        src: baseUrl('public/img/no_picture.gif')
                    }
                ]
            },
            me.valueField
        ];


        me.addEvents(
            'fieldMouseOut',
            /**
             * @event filefileDialogReady
             * 当queued关闭时,flash鼠标移动到按钮上时触发此事件
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             */
            'fieldMouseOver',
            /**
             * @event filefileDialogShow
             * 当queued关闭时,flash按钮被点击时触发此事件
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             */
            'fieldClick',
            /**
             * @event fileButtonLoad
             * 当button渲染完成时可触发此事件
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             */
            'fileButtonLoad',
            /**
             * @event fileButtonLoad
             * 文件选择对话框显示之前可触发此事件
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             */
            'fileDialogStart',
            /**
             * @event fileQueued
             * 文件选择对话框关闭消失时触发此事件
             * @param {SWFUpload} swfupload
             * @param {file object} file
             * @param {Ext.ux.form.field.WinFileField} this
             */
            'fileQueued',
            /**
             * @event fileQueueError
             * 当选择文件对话框关闭消失时，如果选择的文件加入到上传队列中失败，那么针对每个出错的文件都会触发一次该事件.
             * error code参数表示了当前错误的类型，更具体的错误类型可以参见SWFUpload.UPLOAD_ERROR中的定义。
             * Message参数表示的是错误的描述。File参数表示的是上传失败的文件对象。
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             * @param {file object} file
             * @param {int} errorCode
             * @param {sting} message
             */
            'fileQueueError',
            /**
             * @event fileDialogComplete
             * 当选择文件对话框关闭，并且所有选择文件已经处理完成（加入上传队列成功或者失败）时，
             * 此事件被触发，
             * number of files selected是选择的文件数目，number of files queued是此次选择的文件中成功加入队列的文件数目。
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             * @param {int} numFilesSelected
             * @param {int} numFilesQueued
             */
            'fileDialogComplete',
            /**
             * @event uploadStart
             * 在文件往服务端上传之前触发此事件，可以在这里完成上传前的最后验证以及其他你需要的操作，例如添加、修改、删除post数据等。
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             * @param {file object} file
             */
            'uploadStart',
            /**
             * @event uploadProgress
             * 该事件由flash定时触发，提供三个参数分别访问上传文件对象、已上传的字节数，总共的字节数。
             * 因此可以在这个事件中来定时更新页面中的UI元素，以达到及时显示上传进度的效果。
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             * @param {file object} file
             * @param {int} bytesLoaded
             * @param {int} bytesTotal
             */
            'uploadProgress',
            /**
             * @event uploadError
             * 无论什么时候，只要上传被终止或者没有成功完成，那么该事件都将被触发。
             * error code参数表示了当前错误的类型，更具体的错误类型可以参见SWFUpload.UPLOAD_ERROR中的定义。
             * Message参数表示的是错误的描述。File参数表示的是上传失败的文件对象。
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             * @param {file object} file
             * @param {int} errorCode
             * @param {sting} message
             */
            'uploadError',
            /**
             * @event uploadSuccess
             * 当文件上传的处理已经完成（这里的完成只是指向目标处理程序发送了Files信息，只管发，不管是否成功接收），
             * 并且服务端返回了200的HTTP状态时，触发此事件。
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             * @param {file object} file
             * @param {json object} data
             */
            'uploadSuccess',
            /**
             * @event uploadComplete
             * 当上传队列中的一个文件完成了一个上传周期，无论是成功(uoloadSuccess触发)还是失败(uploadError触发)，
             * 此事件都会被触发，这也标志着一个文件的上传完成，可以进行下一个文件的上传了。
             * 如果要进行多文件自动上传，那么在这个时候调用this.startUpload()来启动下一个文件的上传是不错的选择。
             * @param {SWFUpload} swfupload
             * @param {Ext.ux.form.field.WinFileField} this
             * @param {file object} file
             */
            'uploadComplete'
        );
        me.callParent( arguments );
        if ( me.fileType == 'image' && me.showImage == true ) {
            this.down('#imagecomponent').show();
        }

    },
    getValue: function() {
        return this.valueField.getValue();
    },
    afterRender : function ( cmp, eOpts ) {
        var me = this;
        me.callParent( arguments );
        var btn = me.child( 'container' ).child( 'button' );

        btn.btnEl.setStyle({
            position : 'relative',
            display : 'block'
        });

        var id = Ext.id(null,'swf-placeholder');

        btn.btnEl.createChild({
            tag : 'div',
            id : id
        });
        Ext.apply( me.swfSettings, {
            button_placeholder_id : id,

            mouse_click_handler: me.mouseClickAction,
            mouse_over_handler: me.mouseOverAction,
            mouse_out_handler: me.mouseOutAction,

            swfupload_loaded_handler : me.swfLoadedAction,
            swfupload_preload_handler : me.swfPreloadAction,
            file_dialog_start_handler : me.fileDialogStartAction,
            file_queued_handler : me.fileQueuedAction,
            file_queue_error_handler : me.fileQueueErrorAction,
            file_dialog_complete_handler : me.fileDialogCompleteAction,

            upload_start_handler : me.uploadStartAction,
            upload_progress_handler : me.uploadProgressAction,
            upload_error_handler : me.uploadErrorAction,
            upload_success_handler : me.uploadSuccessAction,
            upload_complete_handler : me.uploadCompleteAction,
            debug_handler : me.debugAction
        }, {
            button_width : btn.getWidth() - 5,
            button_height : btn.getHeight() - 2
        } );

        me.swfupload = new SWFUpload( me.swfSettings );

        Ext.get(me.swfupload.movieName).setStyle({
            position : 'absolute',
            left: '0px',
            top: '0px'
        });
        if ( !me.swfSettings.flash_url || !me.swfSettings.flash9_url ) {
            Ext.Msg.alert( "错误", "初始化错误！请指明swf路径!" );
            return false;
        }
    },
    onBoxReady : function (  width, height ) {
        this.callParent();
        this.down('#progressbar' ).setWidth(this.down( '#displayFiel' ).getWidth());
    },
    /**
     * @private
     * mouseOver事件将在鼠标在flash影片的任何部分移动时被触发
     */
    mouseOverAction: function() {
        var me = this.customSettings.scope;
        if(!me.fireEvent( 'fieldMouseOver', this, me )) {
            return false;
        } else {
            this.setButtonDisabled(false);
            this.setButtonAction(SWFUpload.BUTTON_ACTION.SELECT_FILE);
        }
    },
    /**
     * @private
     * mouseOut 事件再鼠标离开flash影片时被触发
     */
    mouseOutAction: function() {
        var me = this.customSettings.scope;

        if(!me.fireEvent('fieldMouseOut',this,me)) {
            this.setButtonDisabled(true);
            this.setButtonAction(SWFUpload.BUTTON_ACTION.NONE);
            return false;
        }
    },
    /**
     * @private
     * mouseClick事件在按钮被单击，
     * （同时button_action setting的值为SWFUpload.BUTTON_ACTION.NONE，或者是flash按钮被设置为disable时）才被触发。
     * 如果button_action settings的值为其他，或者flash按钮可用，这个事件都不被触发
     * @return {Boolean}
     */
    mouseClickAction: function(){
        var me = this.customSettings.scope;
        if(!me.fireEvent( 'fieldClick', this, me )) {
            this.setButtonDisabled(true);
            this.setButtonAction(SWFUpload.BUTTON_ACTION.NONE);
            return false;
        }
    },
    /**
     * @private
     * 该事件函数是内部事件，因此不能被重写。当SWFupload实例化，加载的FLASH完成所有初始化操作时触发此事件。
     */
    swfPreloadAction : function () {
        if ( !this.support.loading ) {
            Ext.Msg.alert( "提示", "需要安装Flash Player的9.028或以上版本才能上传文件" );
            return false;
        }
    },
    /**
     * @private
     * 该事件函数是内部事件，因此不能被重写。加载的FLASH控件完成所有初始化操作时，
     * 将触发此事件来通知SWFUpload它可以接受各种命令了。
     * @return {Boolean}
     */
    swfLoadedAction : function () {
        var me = this.customSettings.scope;
        if(!me.fireEvent( 'fileButtonLoad', this, me )) {
            return false;
        }
    },
    /**
     * @private
     * 此事件在selectFile或者selectFiles调用后，文件选择对话框显示之前触发。只能同时存在一个文件对话框。
     */
    fileDialogStartAction : function () {
        var me = this.customSettings.scope;
        me.fireEvent( 'fileDialogStart', this, me );
    },
    /**
     * @param {object} file
     * @private
     * 当文件选择对话框关闭消失时，如果选择的文件成功加入上传队列，
     * 那么针对每个成功加入的文件都会触发一次该事件（N个文件成功加入队列，就触发N次此事件）。
     */
    fileQueuedAction : function ( file ) {
        var me = this.customSettings.scope;

        if ( this.getStats().files_queued > 0 ) {
            var text = me.child( 'container' ).child( 'textfield' );
            text.setValue( file.name );
            me.fireEvent( 'fileQueued', this, file, me );
            this.startUpload();
        } else {
            return false;
        }
    },
    /**
     * @private
     * @param {object} file
     * @param {int} errorCode
     * @param {sting} message
     * 当选择文件对话框关闭消失时，如果选择的文件加入到上传队列中失败，那么针对每个出错的文件都会触发一次该事件
     * (此事件和fileQueued事件是二选一触发，文件添加到队列只有两种可能，成功和失败)。
     * 文件添加队列出错的原因可能有：超过了上传大小限制，文件为零字节，超过文件队列数量限制，设置之外的无效文件类型。
     * 具体的出错原因可由error code参数来获取，error code的类型可以查看SWFUpload.QUEUE_ERROR中的定义。
     */
    fileQueueErrorAction : function ( file, errorCode, message ) {
        var me = this.customSettings.scope;
        me.fireEvent( 'fileQueueError', this, me, file, errorCode, message );
    },
    /**
     * @private
     * @param {int} numFilesSelected
     * @param {int} numFilesQueued
     * @return {Boolean}
     * 当选择文件对话框关闭，并且所有选择文件已经处理完成（加入上传队列成功或者失败）时，
     * 此事件被触发，number of files selected是选择的文件数目，number of files queued是此次选择的文件中成功加入队列的文件数目。
     */
    fileDialogCompleteAction : function ( numFilesSelected, numFilesQueued ) {
        var me = this.customSettings.scope;
        me.fireEvent( 'fileDialogComplete', this, me, numFilesSelected, numFilesQueued );
    },
    /**
     * @private
     * @param {object} file
     * @return {Boolean}
     * 在文件往服务端上传之前触发此事件，可以在这里完成上传前的最后验证以及其他你需要的操作，例如添加、修改、删除post数据等。
     * 在完成最后的操作以后，如果函数返回false，那么这个上传不会被启动，
     * 并且触发uploadError事件（code为ERROR_CODE_FILE_VALIDATION_FAILED），
     * 如果返回true或者无返回，那么将正式启动上传。
     */
    uploadStartAction : function ( file ) {
        var me = this.customSettings.scope;
        me.fireEvent( 'uploadStart', this, file, me );

    },
    /**
     * @private
     * @param {object} file
     * @param {int} bytesLoaded
     * @param {int} bytesTotal
     * 该事件由flash定时触发，提供三个参数分别访问上传文件对象、已上传的字节数，总共的字节数。
     * 因此可以在这个事件中来定时更新页面中的UI元素，以达到及时显示上传进度的效果。
     * 注意: 在Linux下，Flash Player只在所有文件上传完毕以后才触发一次该事件，
     * 官方指出这是Linux Flash Player的一个bug，目前SWFpload库无法解决
     */
    uploadProgressAction : function ( file, bytesLoaded, bytesTotal ) {
        var me = this.customSettings.scope;

        me.fireEvent( 'uploadProgress', this, me, file, bytesLoaded, bytesTotal );
        var progressbar = me.down( '#progressbar' );
        var displayFiel = me.down( '#displayFiel' );

        progressbar.show();
        displayFiel.hide();
        if ( bytesLoaded < bytesTotal ) {
            bytesLoaded = me.convert( bytesLoaded );
            bytesTotal = me.convert( bytesTotal );
            progressbar.updateProgress( bytesLoaded, 'Loading ' + bytesLoaded + ' of ' + bytesTotal + '...' );
        } else {
            progressbar.updateText( 'Done.' );
            progressbar.hide();
            if(! me.showTexField) displayFiel.show();
        }
    },
    /**
     * @private
     * @param {object} file
     * @param {int} errorCode
     * @param {string} message
     * 无论什么时候，只要上传被终止或者没有成功完成，那么该事件都将被触发。
     * error code参数表示了当前错误的类型，更具体的错误类型可以参见SWFUpload.UPLOAD_ERROR中的定义。
     * Message参数表示的是错误的描述。File参数表示的是上传失败的文件对象。
     * 例如，我们请求一个服务端的一个不存在的文件处理页面，那么error code会是-200，message会是404。
     * 停止、退出、uploadStart返回false、HTTP错误、IO错误、文件上传数目超过限制等，都将触发该事件，
     * Upload error will not fire for files that are cancelled but still waiting in the queue。
     */
    uploadErrorAction : function ( file, errorCode, message ) {
        var me = this.customSettings.scope;
        me.fireEvent( 'uploadError', this, me, file, errorCode, message );
    },
    /**
     * @private
     * @param {object} file
     * @param {string} serverData
     * 当文件上传的处理已经完成（这里的完成只是指向目标处理程序发送了Files信息，只管发，不管是否成功接收），
     * 并且服务端返回了200的HTTP状态时，触发此事件。
     */
    uploadSuccessAction : function ( file, serverData ) {
        var me = this.customSettings.scope;
        var data = Ext.decode( serverData );
        if ( data.success ) {
            var results = data.results;
            me.valueField.setValue( results[me.resultName] );
            me.fireEvent( 'uploadSuccess', this, me, file, data );
            if ( me.fileType == 'image' && me.showImage == true )  {
                me.setIamgeSrc( results );
            }
        }

    },
    /**
     * @param {object} file
     * @private
     * 当上传队列中的一个文件完成了一个上传周期，无论是成功(uoloadSuccess触发)还是失败(uploadError触发)，
     * 此事件都会被触发，这也标志着一个文件的上传完成，可以进行下一个文件的上传了。
     * 如果要进行多文件自动上传，那么在这个时候调用this.startUpload()来启动下一个文件的上传是不错的选择。
     */
    uploadCompleteAction : function ( file ) {
        var me = this.customSettings.scope;
        if ( this.getStats().files_queued === 0 ) {
            me.fireEvent( 'uploadComplete', this, me, file );
        } else {
            if ( this.getStats().files_queued > 0 ) {
                this.startUpload();
            } else {
                return false;
            }
        }
    },
    /**
     * @param {string} message
     * @private
     */
    debugAction : function ( message ) {
        var me = this.customSettings.scope;
        if ( me.debug ) {
            console.log( message );
        }
    },
    /**
     * @param {string} value
     * 文件大小换算
     * @private
     */
    convert : function ( value ) {
        if ( value >= 1000000 ) {
            return parseInt( value / 1000000, 10 ) + 'MB';
        } else if ( value >= 1000 && value < 1000000 ) {
            return parseInt( value / 1000, 10 ) + 'KB';
        } else if ( value < 1000 ) {
            return parseInt( value / 1000, 10 ) + 'B';
        }
    },
    setIamgeSrc : function ( data ) {
        this.down('#imagecomponent').setSrc(data.src ? data.src + "?" + Math.random() : '');
    }
} );
