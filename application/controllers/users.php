<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-25
 * Time: 下午10:59
 * To change this template use File | Settings | File Templates.
 */

class Users extends MY_Controller {

    public function index()
    {
        if(!$this->user) {
            redirect(base_url('users/signin'));
        } else {
            $this->render('users/index');
        }
    }

    public function signin()
    {
        $artists = Artist::all(array(
            'select' => 'id,name'
        ));

        $provinces = Province::all();

        $this->load->vars('provinces',$provinces);
        $this->load->vars('artists',$artists);
        $this->render('users/signin');
    }

    public function logout()
    {
        $this->session->unset_userdata('user');
        $this->session->sess_destroy();

        redirect(base_url());
    }

    public function forgot(){
        $this->render('users/forgot');
    }

    public function forgot_success()
    {
        $email = $this->session->userdata('email');
        $this->session->set_userdata('email');
        if(!$email) {
            show_404();
        }
        $this->load->vars('email',$email);
        $this->render('users/forgot_success');
    }
}