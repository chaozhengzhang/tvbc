<?php

class Artist extends ActiveRecord\Model
{

    static $has_many = array(
        array('albums', 'class_name' => 'ArtistAlbum')
    );

    static $before_destroy = array('delete_image');

    public function delete_image()
    {
        $directorys = array(
            './uploads/artists/cover/big/',
            './uploads/artists/cover/small/',
        );

        foreach ($directorys as $directory) {
            @unlink($directory . $this->cover);
        }

        $albums = $this->albums;
        if ($albums) {
            foreach ($albums as $album) {
                @unlink('./uploads/artists/albums/' . $album->file_name);
            }

        }
    }

    static function get_by_params($params = array())
    {
        $columns = self::connection()->columns('artists');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        foreach ($columns as $columnKey => $columnValue) {
            if (isset($params[$columnKey])) {
                if ($columnValue == 'varchar') {
                    $conditions .= " AND $columnKey LIKE '%" . $params[$columnKey] . "%'";
                } else {
                    $conditions .= " AND $columnKey=" . $params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions' => $conditions));
        $data['results'] = array();

        if ($data['total'] > 0) {
            $activities = self::all($options);

            if ($activities) {
                $data['results'] = array_map(function ($row) {
                    return $row->to_array();
                }, $activities);
            }
        }

        return $data;
    }

    static function get_with_initial()
    {
        $initials = array(
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z'
        );

        $response = array();

        foreach ($initials as $initial) {
            $artists = self::all(array(
                'select' => "id,name,initials,cover",
                'conditions' => "initials='{$initial}'"
            ));

            if($artists) {
                $response[$initial] = array_map(function($row){
                    return $row->to_array();
                },$artists);
            }

        }


        return $response;

    }
}