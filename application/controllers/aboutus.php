<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-23
 * Time: 下午5:11
 * To change this template use File | Settings | File Templates.
 */

class Aboutus extends MY_Controller {

    public function index(){

        //左侧新闻搜索
        $infos = Info::all(
            array(
                'select' => 'id,title,note,publish_time,view_count,cover_image',
                'conditions' => " publish_time > 0",
                'order' => 'publish_time DESC'
            )
        );
        $this->load->vars('infos',$infos);

        //导航栏高亮样式
        $this->load->vars('selectNavli_about','selectNavli');
        $this->render('aboutus/index');
    }

    public function map()
    {
        //导航栏高亮样式
        $this->load->vars('selectNavli_about','selectNavli');
        $this->render('aboutus/map');
    }
}