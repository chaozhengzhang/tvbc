set :application, "tvbc"
server '101.226.179.55', :app, :web, :db, :primary => true

set :deploy_to, "/var/www/html/#{application}/staging"
set :branch, :master
namespace :app do
  task :destroy do
    run "rm -rf #{release_path}/config #{release_path}/.git #{release_path}/.gitignore #{release_path}/Capfile"
    run "ln -s  #{shared_path}/public/config/database.php  #{release_path}/application/config/database.php"
    run "ln -s  #{shared_path}/public/uploads #{release_path}/uploads"
    run "ln -s  #{shared_path}/public/logs #{release_path}/application/logs"
  end
end

after("deploy:update_code","app:destroy","deploy:cleanup")