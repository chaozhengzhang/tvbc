<script type="text/javascript">
    Ext.define('Ext.ux.SliderView',{
        extend: 'Ext.grid.Panel',
        alias: 'widget.sliderview',
        title: '首页新闻设置 (* 双击表格即可编辑)',
        height: 460,
        initComponent: function(config){
            var me = this;

            Ext.apply(me, config || {});

            me.store = Ext.create('Ext.ux.data.NotificationStore', {
                model: 'Slider',
                proxy: {
                    url: baseUrl('data/sliders')
                }
            });

            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                errorSummary: false,
                listeners: {
                    cancelEdit: function(rowEditing, context) {
                        if (context.record.phantom) {
                            me.store.remove(context.record);
                        }
                    },
                    edit: function( editor, context, eOpts){
                        if(!context.record.get('image')) {
                            tools.showTips('提示','必须上传图片');
                        }
                    }
                }
            });

            me.plugins = [rowEditing];

            me.columns = [
                {
                    text: '编号',
                    width: 60,
                    sortable: true,
                    dataIndex: 'id'
                },
                {
                    text: '排序',
                    width: 60,
                    sortable: true,
                    dataIndex: 'sequence',
                    field: {
                        xtype: 'numberfield',
                        allowBlank: false,
                        minValue: 1,
                        value: 1
                    }
                },
                {
                    text: '图片',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'image',
                    renderer: function(v) {
                        var image = "public/img/no_picture.gif";
                        if(v) {
                            image = "uploads/sliders/" + v;
                        }
                        return '<img src="'+image+'" width="321" height="151">';
                    },
                    editor: {
                        xtype: 'uploadfield',
                        showTexField: true,
                        debug: false,
                        queued: true,
                        setting: {
                            file_types: '*.jpg;*.gif;*.jpeg;*.png',
                            upload_url: '/data/sliders/upload_image',
                            flash_url: 'public/swfupload/swfupload.swf',
                            flash9_url: 'public/swfupload/swfupload_fp9.swf'
                        },
                        listeners: {
                            uploadStart : function(sup){
                                var selection  = me.getSelectionModel().getSelection();
                                var id = selection[0].data.id;
                                if(id > 0)
                                    sup.setPostParams({id: id});
                            },
                            uploadSuccess: function(sup, panel, file, data){
                                var selection  = me.getSelectionModel().getSelection();
                                selection[0].set('image',data.results.file_name);
                            }
                        }
                    }
                },
                {
                    text: '标题',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'title',
                    editor: {
                        xtype: 'textfield'
                    }
                },
                {
                    text: '链接地址',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'link',
                    field: {
                        xtype: 'textfield',
                        vtype: 'url'
                    }
                },
                {
                    xtype: 'actioncolumn',
                    text: '删除',
                    width: 120,
                    align: 'center',
                    items: [
                        {
                            icon: '/public/img/delete.png',
                            tooltip: '点击删除',
                            iconCls: 'icon-action-margin',
                            handler: function (grid, rowIndex, colIndex) {
                                var record = grid.getStore().getAt(rowIndex);
                                me.store.remove(record);
                            }
                        }
                    ]
                }
            ];

            me.tbar = [
                '-',
                {
                    text: '增加',
                    iconCls: 'icon-add',
                    handler: function(){
                        me.store.insert(0, new Slider());
                        rowEditing.startEdit(0, 0);
                    }
                },
                '-'
            ];

            me.bbar = Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true
            });

            me.callParent(arguments);
        }
    });

    Ext.widget('sliderview',{ renderTo: 'slider-view'});
</script>
<div id="slider-view"></div>
