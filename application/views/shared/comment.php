<?php
$reply_count = Comment::count(array(
    'conditions' => 'info_id='.$info->id
));

$indorsement_count = Indorsement::count(array(
    'conditions' => 'info_id='.$info->id
));

?>
<div class="comment">
	<form class="comment-form" method="post" action="<?php echo base_url('data/comments'); ?>">
		<div class="comment-detail">
			<a href="<?php echo base_url('news/comment/'.$info->id)?>">已有
                <span class="f_red"><?php echo $reply_count;?></span>
                条评论，共<span
					class="f_red"><?php echo ($reply_count + $indorsement_count); ?></span>人参与</a>
			<span class="detail-info" id="detail-info"><span class="limit-tip" id="limit-tip">还可以输入</span>
                <b id="info-limit">150</b>字</span>
			<input type="hidden" name="info_id" value="<?php echo $info->id;?>">
		</div>
		<div class="comment-detail-area">
			<textarea class="comment-detail-text" id="detail" name="detail" rows="6" datatype="*1-200" class="inputxt" nullmsg="评论内容不能为空"></textarea>
			<span class="Validform_checktip"></span>
		</div>
		<div class="comment-button" style="clear: both;">
			<?php if(!isset($user)) {?>
				<span id="signin-tip">要发布评论,请先<a class="fancybox" href="#usre-signin-view" id="sign-in-button">登录</a></span>
			<?php } else {?>
				<button id="publish-button" class="button red">发布</button>
			<?php } ?>
		</div>
	</form>
</div>
<script type="text/javascript">
        $(function () {
            $('#detail').limitTextarea({
                maxNumber: 200,     //最大字数
                info_limit: '#info-limit',
                onOk: function () {
                    $('.comment-detail-text').css('border-color', '#CCC');
                    $('#info-limit').css('color', '#000');
                },
                onOver: function () {
                    $('.comment-detail-text').css('border-color', '#F00');
                    $('#info-limit').css('color', '#F00');
                    $('#limit-tip').html('已超出');
                }
            });
        });
</script>