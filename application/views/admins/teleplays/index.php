<script type="text/javascript">
Ext.define('Ext.ux.TeleplayView', {
    extend: 'Ext.container.Container',
    alias: 'widget.teleplayview',
    autoScroll: true,
    activeRecord: null,
    initComponent: function (config) {
        var me = this;
        Ext.apply(me, config || {});
        me.store = Ext.create('Ext.ux.data.NotificationStore', {
            model: 'Teleplay',
            autoLoad: true,
            autoSync: true,
            proxy: {
                url: baseUrl('data/teleplays')
            },
            listeners: {
                afterSave: function () {
                    me.onReset();
                }
            }
        });
        me.initArtisForm();
        me.initGrid();
        me.items = [me.form, me.grid];
        me.callParent(arguments);
    },
    initArtisForm: function () {
        var me = this;

        me.form = Ext.widget('form', {
            defaultType: 'textfield',
            bodyPadding: 10,
            fieldDefaults: {
                anchor: '40%',
                labelAlign: 'right'
            },
            items: [
                {
                    xtype: 'hidden',
                    name: 'id'
                },
                {
                    fieldLabel: '剧集名',
                    name: 'name',
                    allowBlank: false
                },
                {
                    fieldLabel: '监制',
                    name: 'producer'
                },
                {
                    fieldLabel: '编审',
                    name: 'copy_editor'
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: '热播？',
                    name: 'is_hot',
                    checked: false
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: '期待？',
                    name: 'is_await',
                    checked: false
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: '列表中显示？',
                    name: 'display',
                    checked: true
                },
                {
                    fieldLabel: '主演',
                    name: 'starring',
                    anchor: '80%',
                    allowBlank: false
                },
                {
                    xtype: 'uploadfield',
                    itemId: 'poster_file',
                    fieldLabel: '海报',
                    fileType: 'image',
                    labelWidth: 60,
                    name: 'poster',
                    isSubmitValue: true,
                    margin: '0 0 0 40',
                    width: 520,
                    buttonText: '选择...',
                    debug: false,
                    queued: true,
                    showImage: true,
                    setting: {
                        file_types: '*.jpg;*.gif;*.jpeg;*.png',
                        upload_url: '/data/teleplays/upload_poster',
                        flash_url: 'public/swfupload/swfupload.swf',
                        flash9_url: 'public/swfupload/swfupload_fp9.swf'
                    }
                },
                {
                    xtype: 'htmleditor',
                    fontFamilies: [],
                    name: 'plot',
                    fieldLabel: '剧情',
                    height: 200,
                    anchor: '80%'

                }
            ],
            buttonAlign: 'center',
            buttons: [
                {
                    text: '保存',
                    handler: function (btn, e) {
                        me.onSave();
                    }
                },
                {
                    text: '重置',
                    handler: function (btn, e) {
                        me.onReset();
                    }
                }
            ]
        });
    },

    initGrid: function () {
        var me = this;

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            errorSummary: false,
            autoCancel: false
        });

        me.grid = Ext.widget('grid', {
            store: me.store,
            columnLines: true,
            padding: 10,
            columns: [
                {
                    header: '编号',
                    width: 50,
                    dataIndex: 'id'
                },
                {
                    header: '电视剧名',
                    dataIndex: 'name',
                    flex: 1,
                    editor: {
                        allowBlank: false
                    }
                },
                {
                    header: '监制',
                    flex: 1,
                    dataIndex: 'producer',
                    editor: {
                        xtype: 'textfield'
                    }
                },
                {
                    header: '编审',
                    flex: 1,
                    dataIndex: 'copy_editor',
                    editor: {
                        xtype: 'textfield'
                    }
                },
                {
                    header: '主演',
                    flex: 1,
                    dataIndex: 'starring',
                    editor: {
                        xtype: 'textfield'
                    }
                },
                {
                    header: '剧情',
                    flex: 1,
                    dataIndex: 'plot'
                },
                {
                    header: '热播?',
                    xtype: 'checkcolumn',
                    flex: 1,
                    dataIndex: 'is_hot',
                    editor: {
                        xtype: 'checkbox'
                    }
                },
                {
                    header: '尽请期待?',
                    xtype: 'checkcolumn',
                    flex: 1,
                    dataIndex: 'is_await',
                    editor: {
                        xtype: 'checkbox'
                    }
                },
                {
                    header: '显示在列表中',
                    xtype: 'checkcolumn',
                    flex: 1,
                    dataIndex: 'display',
                    editor: {
                        xtype: 'checkbox'
                    }
                },
                {
                    xtype: 'actioncolumn',
                    width: 60,
                    header: '操作',
                    align: 'center',
                    items: [
                        {
                            icon: baseUrl('public/img/document_text.png'),  // Use a URL in the icon config
                            tooltip: '编辑',
                            handler: function (grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                me.setActiveRecord(rec);
                            }
                        },
                        {
                            icon: baseUrl('public/img/delete.png'),  // Use a URL in the icon config
                            tooltip: '删除',
                            iconCls: 'icon-action-margin',
                            handler: function (grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                grid.getStore().remove(rec);
                            }
                        }
                    ]
                }
            ],
            minHeight: 300,
            maxHeight: 480,
            title: '剧集列表',
            frame: true,
            plugins: [rowEditing],
            tbar: ['-', {
                xtype: 'textfield',
                labelAlign: 'right',
                labelWidth: 50,
                fieldLabel: '剧集名'
            }, '-',{
                text: '搜索',
                handler: function(btn,e){
                    var name = btn.previousSibling('textfield');
                    me.store.proxy.extraParams.name = name.getValue();
                    me.store.load();
                }
            }],
            bbar: Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true
            })
        });
    },
    onSave: function () {
        var active = this.activeRecord,
            form = this.form.getForm();


        if (form.isValid()) {
            if (active) {
                form.updateRecord(active)
            } else {
                this.store.insert(0, form.getValues());
            }
        }
    },
    onReset: function () {
        this.down('#poster_file').setIamgeSrc({
            src: baseUrl('public/img/no_picture.gif')
        });
        this.form.getForm().reset();
    },
    setActiveRecord: function (record) {
        this.activeRecord = record;
        if (record) {
            var data = {
                src: baseUrl('uploads/teleplays/poster/small/') + record.get('poster') + "?" + Math.random()
            };
            this.down('#poster_file').setIamgeSrc(data);
            this.form.getForm().loadRecord(record);
        }
    }
});

Ext.widget('teleplayview', {
    renderTo: 'teleplays-view'
})
</script>
<div id="teleplays-view"></div>