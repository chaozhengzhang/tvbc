<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-16
 * Time: 上午2:21
 * To change this template use File | Settings | File Templates.
 */

class Artists extends REST_Controller
{

    public function read()
    {
        $params = $this->get();

        $response = Artist::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function create(){
        $data = $this->post('data');
        if(Artist::find_by_name($data['name'])){
            $this->response(NULL,0,'该艺人已经存在！',FALSE);
        } else {
            $response = Artist::create($data);
            $this->response($response->to_array(),1,'增加成功！');
        }
    }

    public function update($id = 0)
    {
        $model = Artist::find($id);
        $params = $this->put('data');
        $model->update_attributes($params);
        $this->response($model->to_array(),1,'编辑成功！');
    }

    public function destroy($id){
        $model = Artist::find($id);
        $model->delete();
        $this->response(NULL,1,'删除成功！');
    }

    public function upload_cover()
    {
        $this->load->library('upload');
        $directorys = array(
            './uploads/artists/cover/big/',
            './uploads/artists/cover/small/',
        );
        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
            $this->load->library('image_lib');
            $thumb_config['image_library'] = 'gd2';
            $thumb_config['source_image'] = $directorys['0'] . $data['file_name'];
            $thumb_config['create_thumb'] = false;
            $thumb_config['maintain_ratio'] = false;


            $thumb_config['width'] = 101;
            $thumb_config['height'] = 153;
            $thumb_config['new_image'] = $directorys[1] . $data['file_name'];
            $this->image_lib->initialize($thumb_config);
            $this->image_lib->resize();
            $data['src'] = "/uploads/artists/cover/small/".$data['file_name'];
        }


        $this->response($data, 1, '上传成功！');
    }
}