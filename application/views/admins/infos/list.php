<script type="text/javascript">
    Ext.define('Ext.ux.InfoListView', {
        extend: 'Ext.grid.Panel',
        alias: 'widget.infolistview',
        title: '新闻列表(* 双击表格中的数据即可进入编辑状态)',
        minHeight: 320,
        maxHeight: 520,
        initComponent: function (config) {
            var me = this;
            Ext.apply(me, config || {});

            me.store = Ext.create('Ext.ux.data.NotificationStore', {
                model: 'Info',
                proxy: {
                    url: baseUrl('data/infos')
                }
            });

            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                errorSummary: false,
                listeners: {
                    cancelEdit: function (rowEditing, context) {
                        if (context.record.phantom) {
                            me.store.remove(context.record);
                        }
                    }
                }
            });

            me.plugins = [rowEditing];

            me.columns = [
                {
                    text: '编号',
                    width: 60,
                    sortable: true,
                    dataIndex: 'id'
                },
                {
                    text: '标题',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'title',
                    field: {
                        xtype: 'textfield',
                        allowBlank: false
                    }
                },
                {
                    text: '标题颜色',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'title_color',
                    field: {
                        xtype: 'textfield'
                    }
                },
                {
                    text: '来源',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'source',
                    field: {
                        xtype: 'textfield'
                    }
                },
                {
                    text: '作者',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'author',
                    field: {
                        xtype: 'textfield'
                    }
                },
                {
                    text: '简介',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'note',
                    field: {
                        xtype: 'textfield'
                    }
                },
                {
                    text: '发布时间',
                    width: 120,
                    sortable: true,
                    dataIndex: 'publish_time'
                },
                {
                    text: '发布?',
                    xtype: 'checkcolumn',
                    align: 'center',
                    width: 80,
                    sortable: true,
                    dataIndex: 'display',
                    field: {
                        xtype: 'checkbox',
                        cls: 'x-grid-checkheader-editor'
                    }
                },
                {
                    text: '热点?',
                    xtype: 'checkcolumn',
                    align: 'center',
                    width: 80,
                    sortable: true,
                    dataIndex: 'hot_spot',
                    field: {
                        xtype: 'checkbox',
                        cls: 'x-grid-checkheader-editor'
                    }
                },
                {
                    xtype:'actioncolumn',
                    width:80,
                    text: '内容编辑',
                    align: 'center',
                    items: [{
                        icon: 'public/img/edit_task.png',  // Use a URL in the icon config
                        tooltip: '编辑内容',
                        handler: function(grid, rowIndex, colIndex) {
                            var win,htmlEditor, rec = grid.getStore().getAt(rowIndex);
                            if(!win) {
                                win = Ext.widget('window',{
                                    modal: true,
                                    resizable: true,
                                    maximizable: true,
                                    layout: 'fit',
                                    items: [
                                        {
                                            xtype: 'form',
                                            minWidth: 840,
                                            minHeight: 500,
                                            border: false,
                                            bodyPadding: 10,
                                            fieldDefaults:{
                                                anchor:'100%',
                                                labelAlign:'right'
                                            },
                                            items: [
                                                {
                                                    xtype: 'hidden',
                                                    itemId: 'cover_image',
                                                    name: 'cover_image',
                                                    value: rec.get('cover_image')
                                                },
                                                {
                                                    xtype: 'hidden',
                                                    itemId: 'detail',
                                                    name: 'detail',
                                                    value: rec.get('detail')

                                                },
                                                {
                                                    xtype:'textarea',
                                                    fieldLabel:'详情',
                                                    submitValue: false,
                                                    name:'detail_editor'
                                                }
                                            ],
                                            listeners: {
                                                afterrender:function(){
                                                    htmlEditor = KindEditor.create('textarea[name="detail_editor"]', {
                                                        uploadJson:'data/infos/upload_image',
                                                        fileManagerJson:'data/infos/file_manager',
                                                        allowFileManager:true,
                                                        height:'300px',
                                                        filePostName:'userfile',
                                                        items:['source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                                                            'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                                                            'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                                                            'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                                                            'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                                                            'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image',/*'insertfile',*/
                                                            'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
                                                            'anchor', 'link', 'unlink']
                                                    });

                                                    htmlEditor.html(rec.get('detail'));
                                                }
                                            },
                                            buttonAlign:'center',
                                            buttons:[
                                                {
                                                    itemId:'save',
                                                    text:'保存',
                                                    handler:function(){
                                                        var fm = this.findParentByType('form').getForm();
                                                        if(htmlEditor.isEmpty()){
                                                            tools.showTips('错误','内容不能为空!');
                                                            return false;
                                                        }

                                                        var text = htmlEditor.text();
                                                        var reg = /<img(.+?)src=""*([^\s]+?)""*(\s|>)/ig;
                                                        var images = text.match(reg);
                                                        if(images) {
                                                            var image = images[0].replace(reg,"$2");
                                                            var t_img = image.split('/');
                                                            win.down('#cover_image').setValue(t_img[t_img.length - 1]);
                                                        }

                                                        win.down('#detail').setValue(htmlEditor.html());
                                                        var values = fm.getValues();
                                                        rec.set(values);
                                                    }
                                                },
                                                {
                                                    text:'重置',
                                                    scope:me,
                                                    handler:function(){
                                                        htmlEditor.html(rec.get('detail'));
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }).show();
                            }
                        }
                    }]
                },
                {
                    xtype:'actioncolumn',
                    width:80,
                    text: '删除',
                    align: 'center',
                    items: [{
                        icon: 'public/img/delete.png',  // Use a URL in the icon config
                        tooltip: '删除新闻',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            Ext.Msg.show({
                                title:'警告?',
                                msg: '数据一旦删除将无法恢复,确认删除？',
                                buttons: Ext.Msg.YESNO,
                                fn: function(btn, text){
                                    if(btn == 'yes') {
                                        me.store.remove(rec);
                                    }
                                },
                                icon: Ext.Msg.WARNING
                            });
                        }
                    }]
                }
            ];

            me.bbar = Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true
            });

            me.callParent(arguments);
        }
    });

    Ext.widget('infolistview', {
        renderTo: 'info-list-view'
    });
</script>
<div id="info-list-view"></div>