<div class="adlistDiv">
    <h2>王牌节目</h2>
    <dl>
        <dt><img src="<?php echo base_url('public/images/29_1.png')?>"/></dt>
        <dd><h3><a href="#">《东张西望》</a></h3>
            <p>日播资讯类节目。节目每日紧贴追踪社会大事、趣闻轶事，并报道娱乐圈最新动态、艺人消息和娱乐花絮。播出时间每晚19:30-20:00。</p></dd>
    </dl>
    <dl>
        <dt><img src="<?php echo base_url('public/images/30_1.png')?>"/></dt>
        <dd><h3><a href="#">《十大劲歌金曲颁奖典礼》</a></h3>
            <p>《十大劲歌金曲颁奖典礼》由TVB主办，创始于1984年1月28日，每年举办一次。颁奖礼会由翡翠台作现场直播（2008
                年起，高清翡翠台加入联播）。十大劲歌金曲颁奖典礼是香港四台联播的一个重要奖项。</p></dd>
    </dl>
    <dl>
        <dt><img src="<?php echo base_url('public/images/31_1.png')?>"/></dt>
        <dd><h3><a href="#">《香港小姐》</a></h3>
            <p>《香港小姐》是香港大型的选美活动，从1973年开始，每年度由TVB举办。港姐是香港女性美的代表，高贵端庄，堪称
                女性楷模，是香港一张永恒的魅力名片。代表性的港姐有世纪美人赵雅芝、风情万种钟楚红、精致五官李嘉欣、清纯
                自然袁咏仪、硕士学历郭蔼明、国际影星张曼玉等都出身于香港小姐。</p></dd>
    </dl>
    <h2>合作形式</h2>
    <div class="adClass">
        <ul>
            <li>广告合作<br/>节目内资讯广告<br/>大型活动策划及制作</li>
            <li>常规广告投放<br/>量身制作栏目<br/>一站式多媒体宣传推广</li>
            <li>剧集及栏目冠名<br/>10分钟广告杂志<br/>电视剧筹拍，赞助与发行</li>
            <li>剧集内产品植入宣传<br/>公关活动策划及艺员出席代言<br/>土豆网-TVB专区内广告资源</li>
        </ul>
    </div>
    <h2>联系我们</h2>
    <div class="adcontant">联系人：朱洁静 Jessie Zhu电话：+86-021-5405 1151 邮箱：jessiezhu@tvbc.com.cn</div>
</div>