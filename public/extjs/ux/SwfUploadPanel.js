/**
 * @author 朝政<chaozhengzhang@hotmail.com>
 */

Ext.define("Ext.ux.SwfUploadPanel", {
    extend : "Ext.panel.Panel",
    alias : "widget.swfuploadpanel",
    layout:"fit",
    minHeight: 240,
    queued: true,
    autoClear: false,
    toolbar: null,
    serverIds: [],
    serverDataRoot: 'results',//服务器返回json数据源,如：{"total":0,"results":{"file":".\/uploads\/providerlog\/1331879211.xlsx"},"message":"","success":true}
    initComponent : function(config) {
        var me = this;
        Ext.apply(me, config);
        if(!me.width) {
            me.width = me.minWidth;
        }
        
        if(!me.height) {
            me.height = me.minHeight;
        }
        
        me.defaultSetting = {
	        upload_url : '',
	        flash_url : '',
            file_post_name : 'userfile',
	        post_params : {files_number:0},
	        use_query_string : false,
	        requeue_on_error : false,
	        http_success : [201, 202],
	        file_types : "*.*",
	        file_types_description: "All Files",
	        file_size_limit : 0,
	        file_upload_limit : 0,
	        file_queue_limit : 0,
	        debug : false,
	        prevent_swf_caching : true,
	        button_placeholder_id : 'swfupload-btn' + Ext.id(null,'-'),
	        button_image_url : "",
	        button_width : 1,
	        button_height : 1,
	        button_text : '',
	        button_text_style : '',
	        button_text_left_padding : 2,
	        button_text_top_padding : 2,
	        button_action : me.queued ? SWFUpload.BUTTON_ACTION.SELECT_FILES : SWFUpload.BUTTON_ACTION.SELECT_FILE,
	        button_disable : false,
	        
	        button_cursor : SWFUpload.CURSOR.HAND,
	        button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT,
	        
	        swfupload_loaded_handler : me.onFlashReady,
	        
	        file_dialog_start_handler : me.onFileDialogStart,
            swfupload_load_failed_handler : me.onLoadFailed,
	        upload_complete_handler : me.onUploadComplete,
	        file_queued_handler : me.onFileQueued,
	        file_queue_error_handler : me.onFileQueueError,
	        file_dialog_complete_handler : me.onFileDialogComplete,
	        upload_start_handler : me.onUploadStart,
	        upload_progress_handler : me.onUploadProgress,
	        upload_error_handler : me.onUploadError,
	        upload_success_handler : me.onUploadSuccess,
	        
	        custom_settings : {
	            scope : me
	        }
	    };
        
        Ext.apply(me.defaultSetting,me.setting);
        
        me.store = Ext.create("Ext.data.Store",{
            fields : [ 'id','filename', 'filetype', 'filesize', 'status', 'percent' ]
        });
        
        me.columns = [{
	        header : '文件名',
            flex : 1,
            sortable : true,
            dataIndex : 'filename'
        },{
            header : '文件类型',
            flex : 1,
            sortable : true,
            dataIndex : 'filetype'
        },{
            header : '文件大小',
            sortable : true,
            dataIndex : 'filesize',
            renderer: function(value,metaData,record,rowIndex,colIndex,store,view){
                if(value >= 1000000) {
                    return parseInt(value/1000000,10) + 'MB';
                } else  if(value >= 1000 && value < 1000000){
                    return parseInt(value/1000,10) + 'KB';
                } else if(value < 1000) {
                    return parseInt(value/1000,10) + 'B';
                }
            }
        },{
            xtype: 'progresscolumn',
            header : '进度',
            sortable : true,
            flex : 1,
            dataIndex : 'percent'
        },{
            header : '状态',
            sortable : true,
            dataIndex : 'status',
            renderer: function(value,metaData,record,rowIndex,colIndex,store,view) {
                switch(value){
		            case SWFUpload.FILE_STATUS.QUEUED  : return '等待上传';
		            break;
		            case SWFUpload.FILE_STATUS.IN_PROGRESS  : return '上传中......';
		            break;
		            case SWFUpload.FILE_STATUS.ERROR  : return '上传失败';
		            break;
		            case SWFUpload.FILE_STATUS.COMPLETE  : return '上传成功';
		            break;
		            case SWFUpload.FILE_STATUS.CANCELLED  : return '取消上传';
		            break;
		            default: return value;
		        }
            }
        }];
        
        me.selectButton = Ext.create('Ext.Button', {
		    text: '选择文件'
		});
        
        me.uploadButton = Ext.create('Ext.Button', {
            text: '全部上传',
            handler : me.onStartUpload,
            scope : me
        });
        
        me.uploadSelectButton = Ext.create('Ext.Button', {
            text: '上传选中',
            handler : me.onSelectUpload,
            scope : me
        });
        
        me.stopButton = Ext.create('Ext.Button', {
            text: '暂停',
            handler: me.onStopUpload,
            scope : me
        });
        
         me.deleteButton = Ext.create('Ext.Button', {
            text: '删除选中',
            handler: me.onDeleteQueue,
            scope : me
        });
        
        me.clearButton = Ext.create('Ext.Button', {
            text: '清空',
            handler: me.onClearQueue,
            scope : me
        });
        
        var items = [];
        if(me.toolbar) {
            var l = me.toolbar.length;
            for(var i = 0; i < l; i++) {
                items.push('-',me.toolbar[i]);
            }
            items.push ('-',me.selectButton,'-',me.uploadButton,'-',me.uploadSelectButton,'-',me.deleteButton,'-',me.stopButton,'-',me.clearButton,'-');
        } else {
            items = ['-',me.selectButton,'-',me.uploadButton,'-',me.uploadSelectButton,'-',me.deleteButton,'-',me.stopButton,'-',me.clearButton,'-'];
        }
        
        me.tbar = Ext.create('Ext.toolbar.Toolbar', {
            items: items
        });
        
        me.bbar = Ext.create('Ext.ux.StatusBar', {
            id: 'upload-statusbar',
            defaultText: 'Ready'
        });
        
        me.grid = Ext.create('Ext.grid.Panel', {
		    store: me.store,
            border: false,
		    columns: me.columns,
            selModel: Ext.create('Ext.selection.CheckboxModel'),
		    autoScroll : true,
            viewConfig: {
		        emptyText: '<div style="margin-top:10px;"><p align="center">' + me.emptyText + '</p></div>',
                forceFit : true
		    },
            frame: false
		});
        me.items = [me.grid];
        me.addEvents('fileDialog','uploadSuccess','uploadComplete','fileQueued','startUpload','uploadStart','stopUpload','clearQueue');
        me.callParent(arguments);
        me.addListener('destroy',function(){
            if(me.contentmenu) {
                me.contentmenu.remove();
            }
        },me);
        me.records = [];
    },
    afterRender: function(ct, position) {
        var me = this;
        me.callParent(arguments);
        var placeholder_em = me.selectButton.getEl().dom.children[0];
        var em = Ext.get(placeholder_em);
        
        var em = Ext.get(placeholder_em);
        em.setStyle({
            position : 'relative',
            display : 'block'
        });
        em.createChild({
            tag : 'div',
            id : 'swf_placeholder'
        });
        me.onClearQueue();
        me.swfupload = new SWFUpload(Ext.apply(me.defaultSetting,{
            button_width : em.getWidth(),
            button_height : em.getHeight(),
            button_placeholder_id : 'swf_placeholder'
        }));
        Ext.get(me.swfupload.movieName).setStyle({
            position : 'absolute',
            left: '0px',
            top: '0px'
        });
        /*me.grid.addListener('itemcontextmenu',me.onItemcontextmenu,me);
        me.grid.addListener('containercontextmenu',function(grid,e,eOpts){
            e.stopEvent();
        });*/
        
        me.statusbar = Ext.getCmp('upload-statusbar');
    },
    /**
     * @private
     * 此事件在selectFile或者selectFiles调用后，文件选择对话框显示之前触发。只能同时存在一个文件对话框。
     */
    onFileDialogStart : function(){
        try{
            var me = this.customSettings.scope;
            me.logFlag = false;
        } catch(ex) {
            this.debug(ex);
        }
    },
    /**
     * @private
     * 该事件函数是内部事件，因此不能被重写。当SWFupload实例化，加载的FLASH完成所有初始化操作时触发此事件。
     */
    onFlashReady : function(){
        if (!this.support.loading) {
	        Ext.Msg.alert("提示","需要安装Flash Player的9.028或以上版本才能上传文件");
	        return false;
	    }
    },
     /**
     * @private
     * 该事件函数是内部事件，因此不能被重写。当SWFupload实例化，加载的FLASH出错时触发此事件。
     */
    onLoadFailed: function(){
        Ext.Msg.alert("提示","加载Flash Player错误");
        return false;
    },
    /**
     * @param {object} file
     * @private
     * 当上传队列中的一个文件完成了一个上传周期，无论是成功(uoloadSuccess触发)还是失败(uploadError触发)，此事件都会被触发，这也标志着一个文件的上传完成，可以进行下一个文件的上传了。
     * 如果要进行多文件自动上传，那么在这个时候调用this.startUpload()来启动下一个文件的上传是不错的选择。
     */
    onUploadComplete : function(file) {
        try{
            var me = this.customSettings.scope;
            var store = me.store;
            if (this.getStats().files_queued === 0) {
                me.fireEvent('uploadComplete', me, me.serverIds.join(','), store);
	            return false;
	        } else {
                if(me.records.length > 0) {
                    if(me.autoClear) store.remove(me.records[0]);
	                me.records.shift(me.records[0]);
	                this.startUpload(me.records[0].data.id);
	            } else {
                    var len = store.getCount();
		            if(file.filestatus == -4){
		                for(var i = 0; i < len; i++){
		                    var record = store.getAt(i);
		                    if(record.get('id') == file.id){
		                        record.set('percent', 1);
		                        if(record.get('state') != -3){
		                            record.set('state', file.filestatus);
		                        }
		                        record.commit();
		                    }
		                }
		            }
                    
		            if (this.getStats().files_queued > 0) {
		                this.startUpload();
		            } else {
		                return false;
		            }
                }
	        }
        } catch(ex) {
            this.debug(ex);
        }
    },
    /**
     * @param {object} file
     * @private
     * 当文件选择对话框关闭消失时，如果选择的文件成功加入上传队列，那么针对每个成功加入的文件都会触发一次该事件（N个文件成功加入队列，就触发N次此事件）。
     */
    onFileQueued : function(file){
        try {
           var me = this.customSettings.scope;
           
            var _file = {
                id: file.id,
                filename : file.name,
                filesize: file.size,
                filetype: file.type,
                status: file.filestatus,
                percent: 0
            };
            if(!file.type) {
                var f = file.name.split('.');
                _file.filetype = f[f.length - 1].toUpperCase();
            }
            me.fireEvent('fileQueued', me, _file);
            me.store.add(_file);
            me.setStatuBarText();
	    } catch (ex) {
	        this.debug(ex);
	    }
    },
    /**
     * @private
     * @param {object} file
     * @param {int} errorCode
     * @param {sting} message
     * 当选择文件对话框关闭消失时，如果选择的文件加入到上传队列中失败，那么针对每个出错的文件都会触发一次该事件
     * (此事件和fileQueued事件是二选一触发，文件添加到队列只有两种可能，成功和失败)。
     * 文件添加队列出错的原因可能有：超过了上传大小限制，文件为零字节，超过文件队列数量限制，设置之外的无效文件类型。
     * 具体的出错原因可由error code参数来获取，error code的类型可以查看SWFUpload.QUEUE_ERROR中的定义。
     */
    onFileQueueError: function(file, errorCode, message){
        try {
	        if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
	            Ext.Msg.alert("Error","You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
	            return;
	        }
	
	        switch (errorCode) {
		        case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
		            Ext.Msg.alert("Error","File is too big.");
		            this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
		        case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
		            Ext.Msg.alert("Error","Cannot upload Zero Byte files.");
		            this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
		        case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
		            Ext.Msg.alert("Error","Invalid File Type.");
		            this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
		        default:
		            if (file !== null) {
		                Ext.Msg.alert("Error","Unhandled Error");
		            }
		            this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
	        }
	    } catch (ex) {
	        this.debug(ex);
	    }
    },
    /**
     * @private
     * @param {int} numFilesSelected
     * @param {int} numFilesQueued
     * @return {Boolean}
     * 当选择文件对话框关闭，并且所有选择文件已经处理完成（加入上传队列成功或者失败）时，
     * 此事件被触发，number of files selected是选择的文件数目，number of files queued是此次选择的文件中成功加入队列的文件数目。
     */
    onFileDialogComplete: function(numFilesSelected, numFilesQueued){
        try {
	        if (numFilesSelected > 0) {
                
	        } else {
                return false;
            }
	    } catch (ex)  {
	        this.debug(ex);
	    }
    },
    /**
     * @private
     * @param {object} file
     * @return {Boolean}
     * 在文件往服务端上传之前触发此事件，可以在这里完成上传前的最后验证以及其他你需要的操作，例如添加、修改、删除post数据等。
     * 在完成最后的操作以后，如果函数返回false，那么这个上传不会被启动，并且触发uploadError事件（code为ERROR_CODE_FILE_VALIDATION_FAILED），
     * 如果返回true或者无返回，那么将正式启动上传。
     */
    onUploadStart: function(file){
        try {
            var me = this.customSettings.scope;
            if (me.getStats().files_queued > 0) {
                me.beginUpload();
            }
	    }
	    catch (ex) {
            this.debug(ex);
        }
	    
	    return true;
    },
    /**
     * @private
     * @param {object} file
     * @param {int} bytesLoaded
     * @param {int} bytesTotal
     * 该事件由flash定时触发，提供三个参数分别访问上传文件对象、已上传的字节数，总共的字节数。因此可以在这个事件中来定时更新页面中的UI元素，以达到及时显示上传进度的效果。
     * 注意: 在Linux下，Flash Player只在所有文件上传完毕以后才触发一次该事件，官方指出这是Linux Flash Player的一个bug，目前SWFpload库无法解决
     */
    onUploadProgress: function(file, bytesLoaded, bytesTotal){
        try {
	        var percent = bytesLoaded / bytesTotal;
            var me = this.customSettings.scope;
            var len = me.store.getCount();
            if(len > 0) {
                for(var i = 0; i < len; i++) {
                    var record = me.store.getAt(i);
                    if(record.get("id") == file.id){
                        record.set('percent',percent);
                        record.set('status', file.filestatus);
                        record.commit();
                    }
                }
            }
	    } catch (ex) {
	        this.debug(ex);
	    }
    },
    /**
     * @private
     * @param {object} file
     * @param {int} errorCode
     * @param {string} message
     * 无论什么时候，只要上传被终止或者没有成功完成，那么该事件都将被触发。
     * error code参数表示了当前错误的类型，更具体的错误类型可以参见SWFUpload.UPLOAD_ERROR中的定义。
     * Message参数表示的是错误的描述。File参数表示的是上传失败的文件对象。
     * 例如，我们请求一个服务端的一个不存在的文件处理页面，那么error code会是-200，message会是404。
     * 停止、退出、uploadStart返回false、HTTP错误、IO错误、文件上传数目超过限制等，都将触发该事件，
     * Upload error will not fire for files that are cancelled but still waiting in the queue。
     */
    onUploadError: function(file, errorCode, message){
        try {
	        switch (errorCode) {
		        case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
		            Ext.Msg.alert("Error","Upload Error: " + message);
		            this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
		            break;
		        case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
		            Ext.Msg.alert("Error","Upload Failed.");
		            this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
		        case SWFUpload.UPLOAD_ERROR.IO_ERROR:
		            Ext.Msg.alert("Error","Server (IO) Error");
		            this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
		            break;
		        case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
		            Ext.Msg.alert("Error","Security Error");
		            this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
		            break;
		        case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
		            Ext.Msg.alert("Error","Upload limit exceeded.");
		            this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
		        case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
		            Ext.Msg.alert("Error","Failed Validation.  Upload skipped.");
		            this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
		        case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
		            Ext.Msg.alert("Error","Cancelled");
		            break;
		        case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
		            Ext.Msg.alert("Error","Stopped");
		            break;
		        default:
		            Ext.Msg.alert("Error","Unhandled Error: " + errorCode);
		            this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
		            break;
	        }
	    } catch (ex) {
	        this.debug(ex);
	    }
    },
    /**
     * @private
     * @param {object} file
     * @param {string} serverData
     * 当文件上传的处理已经完成（这里的完成只是指向目标处理程序发送了Files信息，只管发，不管是否成功接收），
     * 并且服务端返回了200的HTTP状态时，触发此事件。
     */
    onUploadSuccess: function(file, serverData){
        try {
            var me = this.customSettings.scope;
            var len = me.store.getCount();
            if(len > 0) {
                for(var i = 0;i < len;i++) {
                    var record = me.store.getAt(i);
                    if(record.get("id") == file.id){
                        record.set('status', file.filestatus);
                        record.commit();
                    }
                    if(me.autoClear) {
	                    me.store.remove(me.store.getAt(i));
	                }
                }
                me.setStatuBarText();
                var rs = Ext.decode(serverData);
                me.fireEvent('uploadSuccess', me, file, rs.results);
            }
            
	    } catch (ex) {
	        this.debug(ex);
	    }
    },
    /**
     * @private
     * 按钮事件，当点击全部上传时触发该事件。
     */
    onStartUpload: function(btn,e){
        this.files_number = this.store.data.length;
        if (this.swfupload.getStats().files_queued > 0) {
            this.swfupload.startUpload();
        }
    },
    /**
     * @private
     * 按钮事件，当点击暂停是触发该事件。
     */
    onStopUpload: function(btn,e){
        if (this.swfupload.getStats().files_queued > 0) {
	        this.swfupload.stopUpload();
	        this.fireEvent('stopUpload', this,this.store);
        }
    },
    /**
     * @private
     * 按钮事件，当点击清除时触发该事件。
     */
    onClearQueue: function(btn,e){
        this.store.removeAll();
        if(Ext.select('.x-column-header-checkbox',this.grid.el)) {
            Ext.select('.x-column-header-checkbox',this.grid.el).removeCls('x-grid-hd-checker-on');
        }
        this.setStatuBarText('Ready');
        this.fireEvent('clearQueue', this,this.store);
    },
    /**
     * @private
     * 按钮事件，当点击删除选中时触发该时间
     */
    onDeleteQueue: function(btn,e){
        this.records = this.grid.getSelectionModel().getSelection();
        var len = this.records.length;
        if(len > 0) {
            for(var i = 0;i < len; i++){
                this.store.remove(this.records[i]);
                this.swfupload.cancelUpload(this.records[i].data.id,false);
            }
        }
        if(this.store.getCount() == 0) {
	        if(Ext.select('.x-column-header-checkbox',this.grid.el)) {
	            Ext.select('.x-column-header-checkbox',this.grid.el).removeCls('x-grid-hd-checker-on');
	        }
            this.setStatuBarText('Ready');
        } 
        this.records = [];
    },
    /**
     * @private
     * 按钮事件，当点击menu或上传选中按钮时触发。
     */
    onSelectUpload: function(button,e){
        this.records = this.grid.getSelectionModel().getSelection();
        this.files_number = this.records.length;
        if(this.records.length > 0) {
            this.beginUpload(this.records[0].data.id);
        } else {
            return false;
        }
    },
    /**
     * @private
     */
    beginUpload: function(id) {
        if(this.fireEvent('startUpload', this)) {
            if(id) {
                this.swfupload.startUpload(id);
            } else {
                this.swfupload.startUpload();
            }
        }
    },
    /**
     * @private
     * grid的item右键鼠标事件，将生成一个menu
     */
    onItemcontextmenu : function(grid,record,item,index,e,eOpts){
        var xy = e.getXY();
        e.stopEvent();

        if(!this.contentmenu) {
            this.contentmenu = Ext.create('Ext.menu.Menu', {
                width: 100,
                height: 100,
                items: [{
                    text: '上传选中',
                    listeners : {
                        click : {
                            fn: this.onSelectUpload,
                            scope : this
                        }
                    }
                },{
                    text: '删除选中',
                     listeners : {
                        'click' : this.onDeleteQueue,
                        scope : this
                    }
                }]
            });
        }
        this.contentmenu.showAt(xy);
    },
    setStatuBarText: function (text){
        if(this.swfupload) {
	        var v = '上传文件列表中共:<span style="font-weight:bold;color:#008000;">' + this.swfupload.getStats().files_queued 
	        + '</span>个文件。已经上传<span style="font-weight:bold;color:#f00;">' + this.swfupload.getStats().successful_uploads + '</span>个，';
	        if(!text) {
	            this.statusbar.setText(v);
	        } else {
	            this.statusbar.setText(text);
	        }
        } else {
            return false;
        }
    }
});