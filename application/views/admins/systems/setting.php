<script type="text/javascript">

    Ext.define('Ext.ux.SettingView', {
        extend: 'Ext.form.Panel',
        bodyPadding: 20,
        defaultType: 'textfield',
        layout: 'anchor',
        border: 0,
        alias: 'widget.settingview',
        defaults: {
            anchor: '70%'
        },
        initComponent: function (config) {
            var me = this;

            Ext.apply(me, config || {});

            me.items = [
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: '首页背景管理',
                    combineErrors: true,
                    border: 0,
                    msgTarget: 'side',
                    layout: 'hbox',
                    defaults: {

                        hideLabel: true
                    },
                    items: [
                        {
                            xtype: 'filefield',
                            margin: '0 5 0 0',
                            flex: 1,
                            name: 'userfile',
                            allowBlank: false
                        },
                        {
                            xtype: 'button',
                            text: '上传',
                            margin: '0 5 0 0',
                            allowBlank: false,
                            handler: function () {
                                var form = me.getForm();
                                if(form.isValid()) {
                                    form.submit({
                                        url: baseUrl('data/systems/set_index_bg'),
                                        success: function(form, action) {
                                            tools.showTips('提示',action.result.message);
                                        },
                                        failure: function(form, action) {
                                            switch (action.failureType) {
                                                case Ext.form.action.Action.CLIENT_INVALID:
                                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                                    break;
                                                case Ext.form.action.Action.CONNECT_FAILURE:
                                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                                    break;
                                                case Ext.form.action.Action.SERVER_INVALID:
                                                    Ext.Msg.alert('Failure', action.result.message);
                                            }
                                        }
                                    });
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            margin: '0 5 0 0',
                            text: '使用默认背景',
                            handler: function() {
                                Ext.MessageBox.show({
                                    title: '背景替换',
                                    msg: '确定使用默认背景？',
                                    width:300,
                                    buttons: Ext.MessageBox.OKCANCEL,
                                    fn: function(btn, text){
                                        if(btn == 'ok') {
                                            Ext.Ajax.request({
                                                url: baseUrl('data/systems/set_index_bg_defualt'),
                                                success: function(response){
                                                    var text = Ext.decode(response.responseText);
                                                    // process server response here
                                                    tools.showTips('提示',text.message);
                                                }
                                            });
                                        }
                                    },
                                    animateTarget: 'mb3'
                                });
                               /* */
                            }
                        }
                    ]
                }
            ];
            me.callParent(arguments);
        }
    });

    Ext.widget('settingview', { renderTo: 'setting-view'});
</script>
<div id="setting-view"></div>