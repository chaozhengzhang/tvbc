<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-16
 * Time: 上午2:03
 * To change this template use File | Settings | File Templates.
 */

class Artists  extends CI_Controller{

    public function index()
    {
        $this->load->view('admins/artists/index');
    }

    public function albums()
    {
        $this->load->view('admins/artists/albums');
    }
}