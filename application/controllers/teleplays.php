<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-23
 * Time: 下午9:26
 * To change this template use File | Settings | File Templates.
 */

class Teleplays extends MY_Controller {

    public function index()
    {
        $teleplays = Teleplay::all(
            array(
                'conditions' => 'display=1'
            )
        );
        $this->load->vars('teleplays',$teleplays);

        //导航栏高亮样式
        $this->load->vars('selectNavli_teleplays','selectNavli');

        $this->render('teleplays/index');
    }

    public function show($id = 0)
    {
        if(!is_numeric($id) && $id == 0) {
            show_404();
        }

        $teleplay = Teleplay::find($id);


        if(!$teleplay) {
            show_404();
        }

        $dramas = array();
        $dramays = array();
        if($teleplay->parts) {
            $parts = $teleplay->parts;
            foreach ($parts as $key => $part) {
                $datas = TeleplayDrama::all(
                    array(
                        'conditions' => "teleplay_part_id={$part->id}"
                    )
                );
                $dramas[$key]['part'] = $part->name;
                $dramas[$key]['data'] = $datas;
            }

        } else {

            $dramas[0]['part'] = '';
            $dramas[0]['data'] = $teleplay->dramas;
        }

        if($teleplay->parts) {
            $parts = $teleplay->parts;
            foreach ($parts as $key => $part) {
                $datas = TeleplayDrama::all(
                    array(
                        'conditions' => "teleplay_part_id={$part->id}"
                    )
                );
                $dramays[$key]['part'] = $part->name;
                $dramays[$key]['data'] = $datas;
            }

        } else {

            $dramays[0]['part'] = '';
            $dramays[0]['data'] = $teleplay->dramays;
        }

        //剧集详情
        $this->load->vars('teleplay',$teleplay);

        //剧集土豆播放地址
        $this->load->vars('dramas',$dramas);


        //剧集优酷播放地址
        $this->load->vars('dramays',$dramays);

        if( count($dramays[0]['data']) > 0) {
            $this->load->vars('switch_on',2);
        } else {
            $this->load->vars('switch_on',1);
        }

        //导航栏高亮样式
        $this->load->vars('selectNavli_teleplays','selectNavli');

        $this->render('teleplays/show');
    }
}