<script type="text/javascript">
    Ext.define('Ext.ux.JoinView', {
        extend: 'Ext.panel.Panel',
        alias: 'widget.joinview',
        activeRecord: null,
        autoScroll: true,
        initComponent: function (config) {
            var me = this;

            Ext.apply(me, config || {});
            me.store = Ext.create('Ext.ux.data.NotificationStore', {
                model: 'Join',
                autoLoad: true,
                autoSync: true,
                proxy: {
                    url: baseUrl('data/joins')
                },
                listeners: {
                    afterSave: function(){
                        me.onReset();
                    }
                }
            });

            me.positionStore = Ext.create('Ext.data.Store', {
                autoDestroy: true,
                model: 'Position',
                proxy: {
                    type: 'ajax',
                    startParam: 'offset',
                    url: baseUrl('data/positions'),
                    reader: {
                        type: 'json',
                        root: 'results'
                    }
                },
                autoLoad: true
            });

            me.regionStore = Ext.create('Ext.data.Store', {
                autoDestroy: true,
                model: 'Region',
                proxy: {
                    type: 'ajax',
                    startParam: 'offset',
                    url: baseUrl('data/regions'),
                    reader: {
                        type: 'json',
                        root: 'results'
                    }
                },
                autoLoad: true

            });

            me.initForm();
            me.initGrid();
            me.items = [me.form,me.grid];

            me.callParent(arguments);
        },
        initForm: function () {
            var me = this;
            me.form = Ext.widget('form', {
                frame: true,
                title: '编辑招聘信息',
                bodyPadding: 10,
                collapsible: true,
                fieldDefaults: {
                    labelAlign: 'right',
                    allowBlank: false
                },
                layout: 'anchor',
                defaults: {
                    anchor: '40%'
                },
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: '区域',
                        queryMode: 'local',
                        valueField: 'id',
                        displayField: 'name',
                        name: 'region_id',
                        store: me.regionStore
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: '职位',
                        queryMode: 'local',
                        valueField: 'id',
                        displayField: 'name',
                        name: 'position_id',
                        store: me.positionStore
                    },
                    {
                        xtype: 'htmleditor',
                        anchor: '70%',
                        fieldLabel: '岗位职责',
                        name: 'obligation'
                    },
                    {
                        xtype: 'htmleditor',
                        anchor: '70%',
                        fieldLabel: '任职要求',
                        name: 'demand'
                    }
                ],
                buttonAlign: 'center',
                buttons: [
                    {
                        text: '保存',
                        handler: function (btn, e) {
                            me.onSave();
                        }
                    },
                    {
                        text: '重置',
                        handler: function (btn, e) {
                            me.onReset();
                        }
                    }
                ]
            });
        },

        initGrid: function(){
            var me = this;

            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 2,
                errorSummary: false,
                autoCancel: false
            });

            me.grid = Ext.widget('grid', {
                store: me.store,
                columnLines: true,
                padding: 10,
                height: 640,
                resizable: true,
                columns: [
                    {
                        header: '编号',
                        width: 50,
                        dataIndex: 'id'
                    },
                    {
                        header: '区域',
                        width: 100,
                        dataIndex: 'region_id',
                        renderer: function(value,metaData,record,rowIndex,colIndex,store,view ){
                            return record.get('region_name');
                        },
                        editor: {
                            allowBlank: false,
                            xtype: 'combo',
                            queryMode: 'local',
                            valueField: 'id',
                            displayField: 'name',
                            store: me.regionStore
                        }
                    },
                    {
                        header: '职位',
                        dataIndex: 'position_id',
                        width: 160,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store,view ){
                            return record.get('position_name');
                        },
                        editor: {
                            allowBlank: false,
                            xtype: 'combo',
                            queryMode: 'local',
                            valueField: 'id',
                            displayField: 'name',
                            store: me.positionStore
                        }
                    },
                    {
                        header: '岗位职责',
                        flex: 1,
                        dataIndex: 'obligation',
                        editor: {
                            xtype: 'htmleditor',
                            allowBlank: false
                        }
                    },
                    {
                        header: '任职要求',
                        flex: 1,
                        dataIndex: 'demand',
                        editor: {
                            xtype: 'htmleditor',
                            allowBlank: false
                        }
                    },
                    {
                        xtype: 'actioncolumn',
                        width: 60,
                        header: '操作',
                        align: 'center',
                        items: [
                            {
                                icon: baseUrl('public/img/document_text.png'),  // Use a URL in the icon config
                                tooltip: '编辑',
                                handler: function (grid, rowIndex, colIndex) {
                                    var rec = grid.getStore().getAt(rowIndex);
                                    me.setActiveRecord(rec);
                                }
                            },
                            {
                                icon: baseUrl('public/img/delete.png'),  // Use a URL in the icon config
                                tooltip: '删除',
                                iconCls: 'icon-action-margin',
                                handler: function (grid, rowIndex, colIndex) {
                                    var rec = grid.getStore().getAt(rowIndex);
                                    grid.getStore().remove(rec);
                                }
                            }
                        ]
                    }
                ],
                title: '招聘列表 (* 双击表格即可编辑数据)',
                frame: true,
                plugins: [rowEditing],
                bbar: Ext.create('Ext.PagingToolbar', {
                    store: me.store,
                    displayInfo: true
                })
            });
        },
        onSave: function(){
            var active = this.activeRecord,
                form = this.form.getForm();


            if (form.isValid()) {
                if (active) {
                    form.updateRecord(active)
                } else {
                    this.store.insert(0, form.getValues());
                }
            }
        },
        onReset: function(){
            this.form.getForm().reset();
        },
        setActiveRecord: function(record){
            this.activeRecord = record;
            if (record) {
                this.form.getForm().loadRecord(record);
            }
        }
    });

    Ext.widget('joinview',{
        renderTo: 'join-view'
    });
</script>
<div id="join-view"></div>