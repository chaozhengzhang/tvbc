<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-17
 * Time: 下午9:22
 * To change this template use File | Settings | File Templates.
 */

class Teleplay extends ActiveRecord\Model {

    static $has_many = array(
        array('stills','class_name'=> 'TeleplayStill'),
        array('dramas','class_name'=> 'TeleplayDrama','conditions' => 'type = 1'),
        array('dramays','class_name'=> 'TeleplayDrama','conditions' => 'type = 2'),
        array('parts','class_name'=> 'TeleplayPart'),
    );

    static $before_destroy = array('delete_image');

    public function delete_image()
    {
        $directorys = array(
            './uploads/teleplays/poster/big/',
            './uploads/teleplays/poster/small/',
        );

        foreach ($directorys as $directory) {
            @unlink($directory.$this->poster);
        }

        $stills = $this->stills;
        if($stills) {
            foreach ($stills as $still) {
                @unlink('./uploads/teleplays/stills/'.$still->file_name);
            }
        }
    }

    static function get_by_params($params = array()){
        $columns = self::connection()->columns('teleplays');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        foreach($columns as $columnKey=>$columnValue) {
            if(isset($params[$columnKey])) {
                if($columnValue->raw_type == 'varchar') {
                    $conditions .=  " AND $columnKey LIKE '%".$params[$columnKey]."%'";
                } else {
                    $conditions .=  " AND $columnKey=".$params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions'=>$conditions));
        $data['results'] = array();

        if($data['total'] > 0) {
            $activities = self::all($options);

            if($activities) {
                $data['results'] = array_map(function($row){
                    return $row->to_array();
                },$activities);
            }
        }

        return $data;
    }
}