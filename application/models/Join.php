<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-21
 * Time: 下午7:43
 * To change this template use File | Settings | File Templates.
 */

class Join extends ActiveRecord\Model {

    static $belongs_to = array(
        array('position','order' => "ranking ASC"),
        array('region'),
    );

    static function get_by_params($params = array()){
        $columns = self::connection()->columns('joins');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        foreach($columns as $columnKey=>$columnValue) {
            if(isset($params[$columnKey])) {
                if($columnValue->raw_type == 'varchar') {
                    $conditions .=  " AND $columnKey LIKE '%".$params[$columnKey]."%'";
                } else {
                    $conditions .=  " AND $columnKey=".$params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions'=>$conditions));
        $data['results'] = array();

        if($data['total'] > 0) {
            $activities = self::all($options);

            if($activities) {
                $data['results'] = array_map(function($row){
                    $rs = $row->to_array();
		    if(isset($row->position)){
                    	$rs['position_name'] = $row->position->name;
                    	$rs['region_name'] = $row->region->name;
		    } else {
			$rs['position_name'] = '';
                        $rs['region_name'] = '';
		    }
                    return $rs;
                },$activities);
            }
        }

        return $data;
    }

    static function get_with_region(){
        $regions = Region::all();

        $response = array();

        foreach ($regions as $region) {

            /*$joins = self::all(
                array(
                    'conditions' => "region_id={$region->id}",
                    'order' => "ranking ASC"
                )
            );*/


            $joins = self::find_by_sql("SELECT J.id,J.obligation,J.demand,P.name FROM joins AS J, positions AS P
            WHERE J.region_id={$region->id} AND
                J.position_id=P.id ORDER BY P.ranking ASC
            ");
            $response[$region->id] = $joins;
        }

        return $response;
    }
}
