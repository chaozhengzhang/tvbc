<script type="text/javascript">
    Ext.define('Ext.ux.InfobannerView',{
        extend: 'Ext.grid.Panel',
        alias: 'widget.infobannerview',
        title: '首页新闻设置 (* 双击表格即可编辑)',
        height: 460,
        initComponent: function(config){
            var me = this;

            Ext.apply(me, config || {});

            me.store = Ext.create('Ext.ux.data.NotificationStore', {
                model: 'InfoBanner',
                proxy: {
                    url: baseUrl('data/infobanners')
                }
            });

            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                errorSummary: false,
                listeners: {
                    cancelEdit: function(rowEditing, context) {
                        if (context.record.phantom) {
                            me.store.remove(context.record);
                        }
                    }
                }
            });

            me.plugins = [rowEditing];

            me.columns = [
                {
                    text: '编号',
                    width: 60,
                    sortable: true,
                    dataIndex: 'id'
                },
                {
                    text: '排序',
                    width: 60,
                    sortable: true,
                    dataIndex: 'sequence',
                    field: {
                        xtype: 'numberfield',
                        minValue: 1
                    }
                },
                {
                    text: '图片',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'image',
                    renderer: function(v) {
                        if(v) {
                            return '<img src="uploads/infobanners/' + v+'" width="321" height="151">';
                        } else {
                            return '<img src="public/img/no_picture.png" >';
                        }

                    },
                    editor: {
                        xtype: 'uploadfield',
                        showTexField: true,
                        debug: false,
                        queued: true,
                        setting: {
                            file_types: '*.jpg;*.gif;*.jpeg;*.png',
                            upload_url: '/data/infobanners/upload_image',
                            flash_url: 'public/swfupload/swfupload.swf',
                            flash9_url: 'public/swfupload/swfupload_fp9.swf'
                        },
                        listeners: {
                            uploadStart : function(sup){
                                var selection  = me.getSelectionModel().getSelection();
                                sup.setPostParams({id: selection[0].data.id});
                            },
                            uploadSuccess: function(sup, panel, file, data){
                                var selection  = me.getSelectionModel().getSelection();
                                selection[0].set('image',data.results.file_name);
                            }
                        }
                    }
                },
                {
                    text: '新闻',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'info_id',
                    renderer: function(v,m,r){
                        return v ? r.get('info_name') : "暂未关联站内新闻";
                    },
                    editor: {
                        xtype: 'combo',
                        displayField: 'title',
                        valueField: 'id',
                        store: Ext.create( 'Ext.data.Store', {
                            fields: ['id','title'],
                            proxy : {
                                type : 'rest',
                                startParam: 'offset',
                                url: baseUrl('data/infos'),
                                reader : {
                                    type : 'json',
                                    root : 'results'
                                }
                            },
                            autoLoad : true
                        } ),
                        queryMode: 'local',
                        typeAhead: true
                    }
                },
                {
                    text: '其它链接',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'other_link',
                    field: {
                        xtype: 'textfield',
                        vtype: 'url'
                    }
                }
            ];



            me.bbar = Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true
            });

            me.callParent(arguments);
        }
    });

    Ext.widget('infobannerview',{ renderTo: 'infobanner-view'});
</script>
<div id="infobanner-view"></div>
