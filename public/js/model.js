/**
 * 艺人
 */
Ext.define('Artist', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },{
            name: 'id',
            type: 'display',
            defaultValue: 1
        },
        'name', 'ename', 'initials', 'birthday', 'constellation', 'hobby', 'cover','blog','sex'
    ],
    validations: [{
            type: 'length',
            field: 'name',
            min: 1
        }, {
            type: 'length',
            field: 'ename',
            min: 1
        }
    ]
});

/**
 * 艺人相册
 */
Ext.define('ArtistAlbum', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'artist_id', 'file_name'
    ],
    validations: [{
            type: 'length',
            field: 'file_name',
            min: 1
        }
    ]
});



/**
 * 电视剧
 */
Ext.define('Teleplay', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'display',
        'name',//'电视剧名',
        'producer',//'监制',
        'copy_editor',//'编审',
        'starring',//'主演',
        'plot',//'剧情',
        'poster', //'海报图片'
        {name: 'is_hot', type: 'bool'},
        {name: 'is_await', type: 'bool'}
    ],
    validations: [{
            type: 'length',
            field: 'name',
            min: 1
        }
    ]
});


/**
 * 剧照
 */
Ext.define('TeleplayStill', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'teleplay_id',
        'file_name'
    ],
    validations: [{
            type: 'length',
            field: 'file_name',
            min: 1
        }
    ]
});

/**
 * 剧集
 */
Ext.define('TeleplayDrama', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'url',
        'leave','teleplay_id',{
            name: 'type',
            type: 'int'
        }
    ],
    validations: [{
            type: 'length',
            field: 'url',
            min: 1
        }
    ]
});


/**
 * 剧集
 */
Ext.define('Info', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },{
            name: 'view_count',
            type: 'int',
            defaultValue: 0
        },
        'title',
        'title_color',
        'publish_time',
        'create_time',
        'source','author','note','detail','cover_image',
        { name: 'hot_spot', type: 'bool' },
        { name: 'display', type: 'bool' }
    ],
    validations: [{
            type: 'length',
            field: 'title',
            min: 1
        }
    ]
});


/**
 * 职位
 */
Ext.define('Position', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'name','ranking'
    ],
    validations: [{
            type: 'length',
            field: 'name',
            min: 1
        }
    ]
});


/**
 * 区域
 */
Ext.define('Region', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'name'
    ],
    validations: [{
            type: 'length',
            field: 'name',
            min: 1
        }
    ]
});


/**
 * 招聘
 */
Ext.define('Join', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'position_id','region_id','obligation','position_name','region_name','demand'
    ],
    validations: [{
            type: 'length',
            field: 'obligation',
            min: 1
        }
    ]
});

/**
 * 首页新闻
 */
Ext.define('InfoBanner', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'image','info_id','info_name','other_link','sequence'
    ],
    validations: [{
            type: 'length',
            field: 'image',
            min: 1
        }
    ]
});

/**
 * 首页轮播
 */
Ext.define('Slider', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'image','sequence','title','link'
    ],
    validations: [{
            type: 'length',
            field: 'image',
            min: 1
        }
    ]
});

/**
 * 活动
 */
Ext.define('Campaign', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        { name: 'display', type: 'bool' },
        'name','start_time','end_time','rule','reward','top_prize','create_time'
    ],
    validations: [{
            type: 'length',
            field: 'name',
            min: 1
        },{
            type: 'length',
            field: 'start_time',
            min: 1
        }
    ]
});

/**
 * 用户
 */
Ext.define('User', {
    extend: 'Ext.data.Model',
    fields: [{
            name: 'id',
            type: 'int',
            defaultValue: 0
        },
        'name','phone','email','sex','birthday','artists','pushed','create_time','province','city'
    ],
    validations: [{
            type: 'length',
            field: 'name',
            min: 1
        }
    ]
});

