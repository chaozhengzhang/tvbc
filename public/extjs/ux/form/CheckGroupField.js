/**
 * 多选框组件
 *
 */

Ext.define('Ext.ux.form.CheckGroupField',{
    extend: 'Ext.container.Container',
    alias: 'widget.checkgroupfield',
    allowBlank: true,
    syncStore: null,
    id_prefix: 'checkbox-group-',
    initComponent: function(config){
        var me = this;
        Ext.apply(me, config || {});
        me.callParent(arguments);
    },
    afterRender:function(){
        this.callParent();
        this.initContainer();
    },
    initContainer: function() {
        var me = this;

        me.checkGroup = Ext.widget('checkboxgroup',{
            columns: me.columns || 4
        });

        me.fieldSet = Ext.widget('fieldset',{
            collapsible: me.collapsible || true,
            items: [me.checkGroup]
        });

        me.submitField = Ext.widget('hidden',{
            name: me.name ? me.name : 'checkbox-group-field'
        });

        var form = me.findParentByType('form');
        if(form) {
            form.addListener('actioncomplete',function(fm,action,eOpts){
                this.setValue(this.submitField.getValue());
            },me);
        }

        if(me.syncStore) {
            me.syncStore.addListener('load',function(store,records,successful,eOpts ){
                this.setValue(records[0].get('services'));
            },me);

            me.syncStore.addListener('beforesync',function(options,eOpts ){
                if(options.create) {
                    options.create[0].data.services = this.submitField.getValue();
                }

                if(options.update) {
                    options.update[0].data.services = this.submitField.getValue();
                }
            },me);
        }

        if(me.title) me.fieldSet.setTitle(me.title);

        me.fieldSet.add(me.submitField);
        me.bindStore();
        me.add(me.fieldSet);
    },
    bindStore: function(store){
        var me = this;
        me.store = me.store || store;
        var records = me.store.data.items;
        var length = me.store.getCount();

        if(length > 0) {
            for(var i = 0; i < length;i++){

                me.checkGroup.add({
                    xtype: 'checkbox',
                    submitValue: false,
                    allowBlank: me.allowBlank,
                    boxLabel: records[i].get('name'),
                    name: 'checkbox_group_for_' + (me.name ? me.name : 'field'),
                    id: me.id_prefix + records[i].get('id'),
                    checked: me.setChecked(records[i].get('id')),
                    inputValue: records[i].get('id'),
                    listeners: {
                        'change': function( box ){
                            me.setValues(box.getSubmitValue());
                        }
                    }
                });
            }
        }
    },
    setChecked: function(v){
        var me = this;
        if(me.value) {
            var l = me.value.length;
            for(var i = 0;i < l;i++){
                if(me.value[i] == v) {
                    return true;
                }
            }
        } else {
            return false;
        }
    },
    setValue: function(value){
        if(value) {
            var me = this,value = value || [];
            me.value = value;
            if(!Ext.isArray(value)) {
                var v = value.split(',');
                me.value = v;
            }
            var checkBoxs = me.checkGroup.query('checkbox');

            var l = me.value.length,m = checkBoxs.length;
            if(l > 0) {
                for(var i = 0; i < m; i++) {
                    checkBoxs[i].setValue(false);
                }

                for(var i = 0; i < l; i++){
                    Ext.getCmp(me.id_prefix + me.value[i]).setValue(true);
                }
            }
        }
    },
    setValues: function(){
        var me = this,checkBoxs = me.checkGroup.query('checkbox'),values = [];
        var m = checkBoxs.length;
        for(var i = 0; i < m;i++){
            if(checkBoxs[i].getValue()){
                values.push(checkBoxs[i].getSubmitValue());
            }
        }
        me.submitField.setValue(values.toString());
    },
    getValue: function(){
        return this.submitField.getValue();
    }
});
