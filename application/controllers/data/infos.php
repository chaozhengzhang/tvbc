<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-19
 * Time: 下午9:01
 * To change this template use File | Settings | File Templates.
 */

class Infos extends REST_Controller {

    public function read(){
        $params = $this->get();

        $response = Info::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }


    public function create(){
        $data = $this->input->post();
        unset($data['id']);

        if(isset($data['display'])) {
            $data['publish_time'] = time();
        } else {
            $data['publish_time'] = 0;
        }

        $data['create_time'] = time();
        if(!Info::find_by_title($data['title'])){
            $model = Info::create($data);
            $this->response($model->to_array(),1,'增加成功！');
        } else {
            $this->response(NULL,0,'该新闻已经存在！',FALSE);
        }
    }

    public function update($id = 0){
        $model = Info::find($id);
        $data = $this->put('data');
        if($model) {
            if(isset($data['detail'])) {
                $data['detail'] = str_replace('\n\t','',$data['detail']);
            }
            unset($data['id']);

            if(isset($data['display'])) {
                if($data['display']) {
                    if($model->publish_time == 0) {
                        $data['publish_time'] = time();
                    }
                }
            }

            $model->update_attributes($data);
        }

        $this->response($model->to_array(),1,'编辑成功！');
    }

    public function destroy($id = 0){
        $model = Info::find($id);
        if($model) {
            $model->delete();
        }
        $this->response(NULL,1,'删除成功！');
    }

    public function upload_image()
    {
        $this->load->library('upload');
        $directorys = array(
            './uploads/infos/big/',
            './uploads/infos/small/',
        );

        $sizes_width = array(
            '800',
            '600'
        );

        $sizes_height = array(
            '540',
            '400'
        );
        $image_size = getimagesize($_FILES['userfile']['tmp_name']);
        $image_width = $image_size[0];
        $image_height = $image_size[1];
        $scale = 0;
        if($image_width > 800 || $image_height > 600) {
            $scale = (intval($image_width) / intval($image_height));
        }
        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();

            $this->load->library('image_lib');
            $thumb_config['image_library'] = 'gd2';
            $thumb_config['source_image'] = $directorys[0] . $data['file_name'];
            $thumb_config['create_thumb'] = false;
            $thumb_config['maintain_ratio'] = TRUE;

            if($scale == 0) {
                copy($thumb_config['source_image'],$directorys[1]. $data['file_name']);
            } else {
                if($scale > 1) {
                    $thumb_config['master_dim'] = 'width';
                    foreach ($directorys as $key => $directory) {
                        $thumb_config['width'] = $sizes_width[$key];
                        $thumb_config['new_image'] = $directory . $data['file_name'];
                        $this->image_lib->initialize($thumb_config);
                        $this->image_lib->resize();
                    }
                } else {
                    $thumb_config['master_dim'] = 'height';
                    foreach ($directorys as $key => $directory) {
                        $thumb_config['height'] = $sizes_height[$key];
                        $thumb_config['new_image'] = $directory . $data['file_name'];
                        $this->image_lib->initialize($thumb_config);
                        $this->image_lib->resize();
                    }
                }
            }


            $data['src'] = "/uploads/infos/big/".$data['file_name'];
        }

        echo json_encode(array('error' => 0, 'url' => $data['src']));
    }

    public function file_manager()
    {
        $this->load->helper('directory');

        $path = './uploads/infos/small';

        $current_dir_path = '';
        $moveup_dir_path = '';
        $current_url = '';
        $total_count = 0;
        $file_list = array();

        if ($this->input->get('path')) {
            $current_dir_path = $this->input->get('path');
            $moveup_dir_path = preg_replace('/(.*?)[^\/]+\/$/', '$1', $current_dir_path);
            $path .= '/' . $current_dir_path;
            $current_url = $path;
        }

        $maps = directory_map($path, FALSE, TRUE);
        $i = 0;
        foreach ($maps as $key => $value) {
            if (is_array($value)) {
                $total_count = count($value);
                $file_list[$i]['is_dir'] = true;
                $file_list[$i]['has_file'] = true;
                $file_list[$i]['filesize'] = 0; //文件大小
                $file_list[$i]['is_photo'] = false; //是否图片
                $file_list[$i]['filetype'] = ''; //文件类别，用扩展名判断
                $file_list[$i]['filename'] = $key; //文件类别，用扩展名判断
            } else {
                $total_count = count($maps);
                $ext_arr = array('gif', 'jpg', 'jpeg', 'png', 'bmp');
                $file_list[$i]['is_dir'] = false;
                $file_list[$i]['has_file'] = false;
                $file_list[$i]['filesize'] = filesize($path . $value);
                $file_list[$i]['dir_path'] = $path;
                $file_list[$i]['filename'] = $value;
                $file_ext = explode('.', $value);
                $file_list[$i]['is_photo'] = in_array($file_ext[1], $ext_arr); //是否图片
                $file_list[$i]['datetime'] = date('Y-m-d H:i:s', filemtime($path . $value));
            }

            $i++;
        }


        $result = array();
        //相对于根目录的上一级目录
        $result['moveup_dir_path'] = $moveup_dir_path;
        //相对于根目录的当前目录
        $result['current_dir_path'] = $current_dir_path;
        //当前目录的URL
        $result['current_url'] = base_url('uploads/infos/small'.$current_url);
        //文件数
        $result['total_count'] = $total_count;
        //文件列表数组
        $result['file_list'] = $file_list;

        echo json_encode($result);
    }
}