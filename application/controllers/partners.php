<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-23
 * Time: 下午8:00
 * To change this template use File | Settings | File Templates.
 */

class Partners extends MY_Controller {

    public function index()
    {

        //导航栏高亮
        $this->load->vars('selectNavli_partners','selectNavli');
        $this->render('partners/index');
    }

    public function tudou()
    {

        //导航栏高亮
        $this->load->vars('selectNavli_partners','selectNavli');
        $this->render('partners/tudou');
    }

   public function cases() {
  	
        //导航栏高亮
        $this->load->vars('selectNavli_partners','selectNavli');
	$this->render('partners/case');
   }
}
