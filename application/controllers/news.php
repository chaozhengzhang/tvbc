<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-22
 * Time: 下午11:15
 * To change this template use File | Settings | File Templates.
 */

class News extends MY_Controller{

    public function index(){
        $params = $this->input->get();
        $keyword = '';

        $conditions = 'display>0';

        if(isset($params['keyword'])) {
            $keyword = $params['keyword'];
            $conditions .= " AND title LIKE '%{$keyword}%' OR note LIKE '%{$keyword}%' OR detail LIKE '%{$keyword}%'";
        }

        $infos = Info::all(
            array(
                'select' => 'id,title,title_color,note,publish_time,view_count,cover_image',
                'conditions' => $conditions,
                'order' => 'publish_time DESC'
            )
        );

        //新闻内容列表
        $this->load->vars('infos',$infos);

        //导航栏高亮
        $this->load->vars('selectNavli_news','selectNavli');
        $this->render('news/index');
    }

    public function show($id = 0){
        if(!is_numeric($id) && $id == 0) {
            show_404();
        }

        $info = Info::find(
            array(
                'conditions' => " id={$id} AND display>0"
            )
        );

        if(!$info) {
            show_404();
        }

        $info->view_count  = $info->view_count + 1;
        $info->save();

        //新闻内容
        $this->load->vars('info',$info);

        //导航栏高亮
        $this->load->vars('selectNavli_news','selectNavli');
        $this->render('news/show');
    }
}