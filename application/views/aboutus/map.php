<div class="aboutMapDiv">
    <div class="mapDiv hidenstyle">
        <h2>翡翠东方北京分部</h2>

        <p><span>
              地址：北京市朝阳区朝阳门外大街甲6号万通中心C座1808单元</span><span>邮编：100020</span><br/>
            <span>总机：+86-10-5907 3180</span><span>传真：+86-10-5907 3181</span><!--<span>邮箱：beijing@tvbc.com.cn</span>-->
        </p>

        <div class="area1" onclick="javascript:showMapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
        <div class="area2" onclick="javascript:show2MapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
        <div class="area3" onclick="javascript:show3MapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
    </div>
    <div class="map2Div ">
        <h2>翡翠东方上海总部</h2>

        <p><span>地址：上海市徐汇区长乐路989号世纪商贸广场3201室</span><span>邮编：200031</span><br/>
            <span>总机：+86-21-5403 1111</span><span>传真：+86-21-5403 3999</span><!--<span>邮箱：shanghai@tvbc.com.cn</span>-->
        </p>

        <div class="area1" onclick="javascript:showMapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
        <div class="area2" onclick="javascript:show2MapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
        <div class="area3" onclick="javascript:show3MapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
    </div>
    <div class="map3Div hidenstyle">
        <h2>翡翠东方广州分部</h2>

        <p><span>
              地址：广东省广州市天河区华夏路28号富力盈信大厦3804单元</span><span>邮编：510623</span><br/>
            <span>总机：+86-20-8560 8022</span><span>传真：+86-20-8560 8010</span><!--<span>邮箱：guangzhou@tvbc.com.cn</span>-->
        </p>

        <div class="area1" onclick="javascript:showMapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
        <div class="area2" onclick="javascript:show2MapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
        <div class="area3" onclick="javascript:show3MapDiv();"><img src="<?php echo base_url('public/images/img.png');?>" width="50" height="50"/></div>
    </div>
    <div style="padding-top:26px; text-align:center;">
        <img src="<?php echo base_url('public/images/newbg7.png');?>"/>
    </div>
</div>
<script type="text/javascript">
    function showMapDiv() {
        $(".mapDiv").show();
        $(".map2Div").hide();
        $(".map3Div").hide();
    }
    function show2MapDiv() {
        $(".map2Div").show();
        $(".mapDiv").hide();
        $(".map3Div").hide();
    }
    function show3MapDiv() {
        $(".map3Div").show();
        $(".mapDiv").hide();
        $(".map2Div").hide();
    }
</script>