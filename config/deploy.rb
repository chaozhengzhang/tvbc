set :stages, %w(production testing)
set :default_stage, "testing"
require 'capistrano/ext/multistage'

set :use_sudo, false

set :user, "deploy"
set :group, "deploy"
set :port, 4022
set :keep_releases, 2

set :scm, :git
set :repository, "https://chaozhengzhang@bitbucket.org/chaozhengzhang/tvbc.git"
set :deploy_via, :remote_cache