<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-22
 * Time: 下午9:21
 * To change this template use File | Settings | File Templates.
 */

class Videos extends REST_Controller {

    public function upload(){
        $this->load->library('upload');
        $directorys = array(
            './uploads/videos/'
        );

        $this->create_directorys($directorys);
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = 1;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
        }

        $this->response($data,1,'上传成功！');
    }
}