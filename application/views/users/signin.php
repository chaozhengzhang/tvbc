<link type="text/css" rel="stylesheet" href="<?php echo base_url('public/plugin/validform/css/validform.css'); ?>">
<script type="text/javascript"
        src="<?php echo base_url('public/plugin/validform/js/Validform_v5.3.2_min.js'); ?>"></script>
<div class="regDiv">
    <h2>我的帐户</h2>

    <div class="regMegDiv">
        <div class="regDiv-l">
            <h3>登录 LOGIN（已注册用户由此登录）</h3>

            <p class="regWord">如果您已经拥有一个帐户，请输入您的用户名和密码。</p>

            <form class="login-form" method="post" action="<?php echo base_url('data/users/login'); ?>">
                <p>用户名<br/>
                    <input name="name_email" datatype="*" class="inputxt" type="text" nullmsg="用户名不能为空"/>
                    <span class="Validform_checktip"></span>
                </p>

                <p>密码<br/>
                    <input type="password" name="password" datatype="*" class="inputxt" nullmsg="密码不能为空" />
                    <span class="Validform_checktip"></span>
                </p>
                <p>
                    <input type="submit" class="loginbtn" value="登录"/>
                    <span id="msgdemo" class="Validform_checktip"></span>
                </p>
            </form>
            <p><a href="<?php echo base_url('users/forgot');?>">忘记密码?</a></p>
        </div>
        <div class="regDiv-r">
            <h3>注册 REGISTRATION（没有账号？请在这里注册）</h3>

            <p>创建一个新账号，填写您的正确信息。</p>

            <form class="register-form" method="post" action="<?php echo base_url('data/users/register'); ?>">
                <p><em>*</em>用户名<br/>
                    <input name="name" datatype="*" class="inputxt" type="text" nullmsg="用户名不能为空" />
                    <span class="Validform_checktip"></span>
                </p>

                <p><em>*</em>电子邮件地址<br/>
                    <input name="email" datatype="e" class="inputxt" nullmsg="电子邮箱不能为空"
                           ajaxurl="<?php echo base_url('data/users/valid_email'); ?>" type="text"/>
                    <span class="Validform_checktip"></span>
                </p>

                <p><em>*</em>密码<br/>
                    <input name="password" type="password" datatype="*6-16" class="inputxt" nullmsg="请设置密码！" errormsg="密码范围在6~16位之间！"/>
                    <span class="Validform_checktip"></span>
                </p>

                <p><em>*</em>确认密码<br/>
                    <input name="rep_password" type="password" class="inputxt" datatype="*" recheck="password" nullmsg="请再输入一次密码！" errormsg="您两次输入的账号密码不一致！"/>
                    <span class="Validform_checktip"></span>
                </p>

                <p>手机/座机<br/><input name="phone" type="text"/></p>

                <p><em>*</em>性别<br/>
                    <label>
                        <input type="radio" name="sex" value="1" checked="checked"/>
                    </label>
                    男
                    <label>
                        <input type="radio" name="sex" value="0"/>
                    </label>
                    女
                </p>

                <p><em>*</em>所在地区<br/>
                    <select name="province" id="province" datatype="*" nullmsg="请选择所在省！">
                        <option  selected="selected" value="">请选择省</option>
                        <?php
                            foreach ($provinces as $province) {
                        ?>
                        <option value="<?php echo $province->id;?>"><?php echo $province->name;?></option>
                        <?php
                            }

                        ?>
                    </select>
                    <select name="city" id="city" datatype="*" nullmsg="请选择所在市/县！">
                        <option selected="selected" value="">请选择市/县</option>
                    </select>
                    <span class="Validform_checktip"></span>
                </p>

                <p>生日<br/>
                    <select name="year" id="year">
                        <option  selected="selected" value=" ">请选择年</option>
                  </select>
                    <select name="month" id="month">
                        <option selected="selected" value=" ">请选择月</option>
                    </select>
                    <select name="day" id="day">
                        <option selected="selected" value=" ">请选择日</option>
                    </select>
                </p>
                <p><em>*</em>我最喜爱的TVB艺人（可多选）<br/>
                    <select name="artist[]" id="artist1" class="artist" style="width:120px;">
                   	  <option selected="selected" value="">请选择艺人</option>
                        <?php
                            foreach ($artists as $artist) {
                        ?>
                        <option value="<?php echo $artist->name;?>"><?php echo $artist->name;?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <select name="artist[]" id="artist2" class="artist" style="width:120px;">
                    	<option selected="selected" value="">请选择艺人</option>
                        <?php
                        foreach ($artists as $key => $artist) {
                            ?>
                            <option  value="<?php echo $artist->name;?>"><?php echo $artist->name;?></option>
                        <?php
                        }
                        ?>
                  </select>
                    <select name="artist[]" id="artist3" class="artist" style="width:120px;">
               	    <option selected="selected" value="">请选择艺人</option>
                        <?php
                        foreach ($artists as $key1 => $artist) {
                            ?>
                            <option  value="<?php echo $artist->name;?>"><?php echo $artist->name;?></option>
                        <?php
                        }
                        ?>
                  </select><br />
                    <select name="artist[]" id="artist4" class="artist" style="width:120px;">
                    	<option selected="selected" value="">请选择艺人</option>
                        <?php
                        foreach ($artists as $key1 => $artist) {
                            ?>
                            <option  value="<?php echo $artist->name;?>"><?php echo $artist->name;?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <select name="artist[]" id="artist5" class="artist" style="width:120px;">
                    	<option selected="selected" value="">请选择艺人</option>
                        <?php
                        foreach ($artists as $key1 => $artist) {
                            ?>
                            <option  value="<?php echo $artist->name;?>"><?php echo $artist->name;?></option>
                        <?php
                        }
                        ?>
                    </select>
                   <input type="text" style="width:113px; height:18px; line-height:18px; border:solid 1px #809DB9; font-size:12px; color:#000;"
                          name="other" id="other" placeholder="其他" />
                  <span id="for-artist" class="Validform_checktip"></span>
                </p>

                <p style="line-height:18px;"><label><input name="pushed" type="checkbox" value="1" checked="checked"/></label>
                    我愿意接受翡翠东方TVBC发送给我的最新艺人动态及活动资讯                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </p>

                <p>
                    <input type="submit" class="loginbtn" value="注册"/><span id="regmsg" class="Validform_checktip"></span>
                </p>
            </form>
        </div>
        <div class="clearboth"></div>
        <div class="aboutMem">
            <h3>为什么要注册成为我们的会员?</h3>

            <p>会员享有以下福利：<br/>
                1. 注册会员即可参加品牌线上活动；<br/>
                2. 会员生日神秘礼品；<br/>
                3. 特别邀请参与品牌线下活动，如媒体发布会、新剧记者会等；<br/>
                4. 有机会与自己喜爱的TVB艺人近距离接触。</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        function changeCity(province_id) {

            var html = '<option selected="selected" value="">请选择市/县</option>';

            $('#city').html(html);

            $.ajax({
                url: '/data/cities',
                dataType: 'json',
                data: {
                    province_id: province_id
                },
                success: function(datas) {
                    if(datas.total > 0) {
                        for(var i = 0; i < datas.total;i++) {

                            html += '<option value="' + datas.results[i].id + '">' + datas.results[i].name + '</option>';
                        }
                        $('#city').html(html);
                    }
                }
            })
        }

        $('#province').on('change',function(){
            var province_id = this.value;
            changeCity(province_id);
        });


        $(".login-form").Validform({
            tiptype:function(msg,o,cssctl){
                if(!o.obj.is("form")){
                    var objtip=o.obj.siblings(".Validform_checktip");
                    cssctl(objtip,o.type);
                    objtip.text(msg);
                }else{
                    var objtip=o.obj.find("#msgdemo");
                    cssctl(objtip,o.type);
                    objtip.text(msg);
                }
            },
            showAllError: true,
            ajaxPost:true,
            callback: function (data) {
                if(data.status == 'y') {
                    window.location.href = "/";
                }
            }
        });

        var register_form = $(".register-form").Validform({
            tiptype:function(msg,o,cssctl){
                if(!o.obj.is("form")){
                    var objtip=o.obj.siblings(".Validform_checktip");
                    cssctl(objtip,o.type);
                    objtip.text(msg);
                }else{
                    var objtip=o.obj.find("#regmsg");
                    cssctl(objtip,o.type);
                    objtip.text(msg);
                }
            },
            showAllError: true,
            ajaxPost:true,
            beforeSubmit:function(curform){

                var artists = $('.artist');
                var flag = 0;
                for(var i = 0; i < artists.length;i++){
                    if($(artists[i]).val()) {
                        flag += 1;
                    }
                }

                if($('#other').val()) {
                    flag += 1;
                }

                if(flag == 0) {
                    $('#for-artist').removeClass("Validform_right Validform_loading").addClass("Validform_checktip Validform_wrong");
                    $('#for-artist').html('请选择至少1个明星！');
                    return false;
                } else {
                    $('#for-artist').removeClass("Validform_wrong").addClass("Validform_right Validform_checktip");
                }
            },
            callback: function (data) {
                if(data.status == 'y') {
                    window.location.href = "/";
                }
            }
        });


        var i = 1945;
        var date = new Date();
        year = date.getFullYear();//获取当前年份
        var dropList = '<option value="">请选择</option>';
        for (i; i <= year; i++) {
            dropList = dropList + "<option value='" + i + "'>" + i + "</option>";
        }
        $('select[name=year]').html(dropList);//生成年份下拉菜单
        var monthly = '<option value="">请选择</option>';
        for (month = 1; month < 13; month++) {
            monthly = monthly + "<option value='" + month + "'>" + month + "</option>";
        }
        $('select[name=month]').html(monthly);//生成月份下拉菜单
        var dayly = '<option value="">请选择</option>';
        for (day = 1; day <= 31; day++) {
            dayly = dayly + "<option value='" + day + "'>" + day + "</option>";
        }
        $('select[name=day]').html(dayly);//生成月份下拉菜单

        $('select[name=month]').change(function () {
            var currentDay;
            var Flag = $('select[name=year]').val();
            var currentMonth = $('select[name=month]').val();
            switch (currentMonth) {
                case "1" :
                case "3" :
                case "5" :
                case "7" :
                case "8" :
                case "10" :
                case "12" :
                    total = 31;
                    break;
                case "4" :
                case "6" :
                case "9" :
                case "11" :
                    total = 30;
                    break;
                case "2" :
                    if ((Flag % 4 == 0 && Flag != 0) || Flag == 0) {
                        total = 29;
                    } else {
                        total = 28;
                    }
                default:
                    break;
            }
            for (day = 1; day <= total; day++) {
                currentDay = currentDay + "<option value='" + day + "'>" + day + "</option>";
            }
            $('select[name=day]').html(currentDay);//生成日期下拉菜单
        });
    });

</script>