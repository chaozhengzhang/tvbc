<div class="l-Div newslist">
    <ul>
        <?php
            if(isset($infos)){
                foreach($infos as $info){
        ?>
        <li>
            <h2>
                <a href="<?php echo base_url('news/show/'.$info->id);?>" class="redcor">
                     <font style="color:<?php echo $info->title_color;?>;"><?php echo $info->title;?></font>
                </a>
            </h2>
            <span><?php echo date('Y-m-d H:i:s',$info->publish_time);?>| <?php echo $info->view_count;?>次阅读</span><br/>
            <?php
                if($info->cover_image) {
            ?>
            <img src="<?php echo base_url('uploads/infos/small/'.$info->cover_image);?>" style="max-width:480px;"/>
            <?php } ?>
            <p><?php echo $info->note;?></p>
            <div class="tagDiv">
                <i>Tag：</i><strong class="redcor">翡翠东方</strong>
                <a href="<?php echo base_url('news/show/'.$info->id);?>" class="redcor">阅读全文...</a>
            </div>
            <div class="clearboth"></div>
        </li>
        <?php
                }
            }
        ?>
    </ul>
</div>
<?php $this->load->view('shared/search_tool');?>
