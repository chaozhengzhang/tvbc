<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-19
 * Time: 下午8:37
 * To change this template use File | Settings | File Templates.
 */

class Infos extends CI_Controller {

    public function index(){
        $this->load->view('admins/infos/index');
    }

    public function show_list(){
        $this->load->view('admins/infos/list');
    }

    public function infobanners(){
        $this->load->view('admins/infos/infobanner');
    }
}