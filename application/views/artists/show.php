<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/jquery.fancybox-1.3.4.css');?>"/>
<div class="pepshow">
    <div class="l-pep">
        <img src="<?php echo base_url('uploads/artists/cover/big/'.$artist->cover);?>" width="520" height="680" />
    </div>
    <div class="r-pep <?php echo $artist->sex == 1 ? 'manDiv':'womanDiv';?>">
        <h2><?php echo $artist->name;?><br />
            <?php echo $artist->nick_name;?></h2>
        <div class="<?php echo $artist->sex == 1 ? 'pepMeg' : 'pepMeg womanMeg';?>">
            姓名: <?php echo $artist->name;?><span style="margin-left:5px;">(<?php echo $artist->ename;?>)</span><br />
            生日: <?php echo $artist->birthday;?><br />
            星座: <?php echo $artist->constellation;?><br />
            <?php
                if(!empty($artist->hobby)){
            ?>
            嗜好: <?php echo $artist->hobby;?><br />
            <?php
                }
            $artist_albums = $artist->albums;
            if(isset($artist_albums)) {
            ?>
            <div class="pepMorepic">
                <?php
                foreach ($artist_albums as $artist_album) {
                ?>
                <a  rel="example_group" href="<?php echo base_url('uploads/artists/albums/big/'.$artist_album->file_name);?>" title="" >
                    <img src="<?php echo base_url('uploads/artists/albums/small/'.$artist_album->file_name);?>" />
                </a>
                <?php
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
        <div class="weiboDiv">微博: <a href="<?php echo $artist->blog;?>" target="_blank"><?php echo $artist->blog;?></a></div>
    </div>
    <div class="clearboth"></div>
</div>
<script type="text/javascript" src="<?php echo base_url('public/js/jquery.fancybox-1.3.4.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/jquery.mousewheel-3.0.4.js');?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("a[rel=example_group]").fancybox({
            'transitionIn':'none',
            'transitionOut':'none',
            'titlePosition':'over',
            'titleFormat':function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over"><?php echo $artist->name;?>&nbsp; ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });
    });
</script>