<link href="/public/css/tvbc_newstyle.css" rel="stylesheet" type="text/css"/>
<link href="/public/css/summer_2014.css" rel="stylesheet" type="text/css"/>
<div class="show-min1">
    <table>
        <tr>
            <td>
                <div>
                    <a href="/teleplays/show/83" target="_blank"><img src="/public/images/campaigns/list-zjr.jpg"></a>
                </div>
                <div>
                    <a href="/teleplays/show/82" target="_blank">
                        <img src="/public/images/campaigns/list-hsql.jpg" style="width: 372px;">
                    </a>
                </div>
            </td>
            <td>
                <div>
                    <a href="/teleplays/show/76" target="_blank">
                        <img src="/public/images/campaigns/list-swn.jpg" style="margin-left: -1px;">
                    </a>
                </div>
                <div style="float: left;margin-top: -6px;">
                    <a href="/teleplays/show/82" target="_blank">
                    <img src="/public/images/campaigns/list-nrjlb.jpg">
                    </a>
                    <a href="/teleplays/show/83" target="_blank">
                        <img src="/public/images/campaigns/list-djss.jpg" style="margin-left: -5px;">
                    </a>
                </div>
                <div style="float: left;margin-top: -5px;">
                    <a href="/teleplays/show/88" target="_blank">
                    <img src="/public/images/campaigns/list-sdmb.jpg">
                    </a>
                    <a href="/teleplays/show/90" target="_blank">
                    <img src="/public/images/campaigns/list-snyh.jpg" style="margin-left: -5px;">
                    </a>
                </div>
                <div style="float: right;margin-top: -4px;">
                    <a href="/teleplays/show/93" target="_blank">
                    <img src="/public/images/campaigns/list-xjjj.jpg" style="margin-right: 21px;">
                    </a>
                </div>
            </td>
        </tr>
    </table>
    <!--<div class="kvlist-l">



    </div>
    <div class="kvlist-r">
        <img src="/public/images/campaigns/list-swn.jpg">
    </div>-->
</div>
<div class="clearboth"></div>
<div class="show-min2">
    <div class="showmorevd">
        <ul>
            <?php
            foreach ($campaign_teleplays as $campaign_teleplay) {
            ?>
            <li>
                <a href="<?php echo base_url('teleplays/show/'.$campaign_teleplay->teleplay->id); ?>" target="_blank">
                    <img
                        src="<?php echo base_url('uploads/teleplays/poster/small/'.$campaign_teleplay->teleplay->poster); ?>"/>
                </a>
                <strong>
                    <a href="<?php echo base_url('teleplays/show/'.$campaign_teleplay->teleplay->id); ?>"
                       target="_blank">
                        <?php echo $campaign_teleplay->teleplay->name; ?>
                    </a>
                    <em>
                        <?php if ($campaign_teleplay->teleplay->is_hot) { ?>
                            <img src="/public/images/hot.gif"/>
                        <?php } else if ($campaign_teleplay->teleplay->is_await) { ?>
                            <img src="/public/images/coming.gif"/>
                        <?php } ?>
                    </em>
                </strong>

                <p>
                    <span>监制：<?php echo character_cn_limiter($campaign_teleplay->teleplay->producer, 4); ?></span>
                    <span>编审：<?php echo character_cn_limiter($campaign_teleplay->teleplay->copy_editor, 4); ?></span>
                    <span>演员：<?php echo character_cn_limiter($campaign_teleplay->teleplay->starring, 4); ?></span></p>
            </li>
            <?php
            }
            ?>
            <div class="clearboth"></div>
        </ul>
        <div class="page-nation">
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>