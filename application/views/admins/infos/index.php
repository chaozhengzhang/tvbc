<script type="text/javascript">
Ext.define('Ext.ux.InfoView', {
    extend:'Ext.panel.Panel',
    alias:'widget.infoview',
    autoScroll:true,
    activeRecord:null,
    initComponent:function (config) {
        config = config || {};
        var me = this;
        Ext.apply(me, config);

        me.store = Ext.create('Ext.ux.data.NotificationStore', {
            model:'Info',
            proxy:{
                url:baseUrl('data/infos')
            },
            autoLoad: false
        });

        me.initForm();
        me.items = [
            me.form
        ];

        me.callParent(arguments);
    },
    afterRender:function (cmp, eObj) {
        this.callParent(arguments);

        this.htmlEditor = KindEditor.create('textarea[name="detail_editor"]', {
            uploadJson:'data/infos/upload_image',
            fileManagerJson:'data/infos/file_manager',
            allowFileManager:true,
            height:'300px',
            filePostName:'userfile',
            items:['source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image',/*'insertfile',*/
                'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
                'anchor', 'link', 'unlink']
        });
    },
    initForm:function () {
        var me = this;
        me.form = Ext.widget('form', {
            defaultType:'textfield',
            bodyPadding:5,
            border:0,
            frame:false,
            fieldDefaults:{
                anchor:'100%',
                labelAlign:'right'
            },
            items:[
                {
                    xtype:'hidden',
                    name: 'id'
                },
                {
                    fieldLabel:'标题',
                    name:'title',
                    allowBlank:false,
                    anchor:'60%'
                },
                {
                    fieldLabel:'标题颜色',
                    name:'title_color',
                    anchor:'30%'
                },
                {
                    fieldLabel:'来源',
                    itemId: 'source',
                    name:'source',
                    value: '翡翠影视',
                    anchor:'20%'
                },
                {
                    fieldLabel:'作者',
                    value: '翡翠影视',
                    name:'author',
                    anchor:'20%'
                },
                {
                    xtype: 'hidden',
                    itemId: 'cover_image',
                    name: 'cover_image'
                },
                {
                    xtype:'textarea',
                    fieldLabel:'简介',
                    name:'note',
                    allowBlank:false,
                    anchor:'60%'
                },
                {
                    xtype:'checkbox',
                    fieldLabel:'是否是热点?',
                    name:'hot_spot',
                    inputValue: 1
                },
                {
                    xtype:'checkbox',
                    fieldLabel:'立即发布?',
                    name:'display',
                    inputValue: 1
                },
                {
                    xtype: 'hidden',
                    itemId: 'detail',
                    name: 'detail'

                },
                {
                    xtype:'textarea',
                    fieldLabel:'详情',
                    submitValue: false,
                    name:'detail_editor'
                }
            ],
            buttonAlign:'center',
            buttons:[
                {
                    itemId:'save',
                    text:'保存',
                    handler:function(){
                        var fm = me.form.getForm();
                        if(me.htmlEditor.isEmpty()){
                            tools.showTips('错误','内容不能为空!');
                            return false;
                        }

                        var text = me.htmlEditor.text();
                        var reg = /<img(.+?)src=""*([^\s]+?)""*(\s|>)/ig;
                        var images = text.match(reg);
                        if(images) {
                            var image = images[0].replace(reg,"$2");
                            var t_img = image.split('/');
                            me.down('#cover_image').setValue(t_img[t_img.length - 1]);
                        }

                        me.down('#detail').setValue(me.htmlEditor.html());
                        if(fm.isValid()) {
                            fm.submit({
                                url: baseUrl('data/infos/create'),
                                success: function(form, action) {
                                    tools.showTips('提示',action.result.message);
                                    form.reset();
                                    me.htmlEditor.html('');
                                },
                                failure: function(form, action) {
                                    switch (action.failureType) {
                                        case Ext.form.action.Action.CLIENT_INVALID:
                                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                            break;
                                        case Ext.form.action.Action.CONNECT_FAILURE:
                                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                                            break;
                                        case Ext.form.action.Action.SERVER_INVALID:
                                            Ext.Msg.alert('Failure', action.result.message);
                                    }
                                }
                            });
                        }
                    }
                },
                {
                    text:'重置',
                    scope:me,
                    handler:function(){
                        var fm = me.form.getForm();
                        me.htmlEditor.html('');
                        fm.reset();
                    }
                }
            ]
        });
    }
});

Ext.widget('infoview', {
    renderTo:'infos-div-panel'
});
</script>
<div id="infos-div-panel"></div>
