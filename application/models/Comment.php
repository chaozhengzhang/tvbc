<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-1-16
 * Time: 下午8:01
 */

class Comment extends ActiveRecord\Model {
	static $has_one = array(
		array('profile','class_name'=>'CommentProfile'),

	);

    static $has_many = array(
		array('replies','class_name'=>'Comment'),

	);
	
	static $belongs_to = array(
		array('user','class_name'=>'User'),
	);

	public function detail()
	{
		return $this->profile->detail;
	}

	public function indorsement_count()
	{
		return Indorsement::count(
			array(
				'conditions' => "comment_id=".$this->id
			)
		);
	}
}