<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-24
 * Time: 下午10:25
 * To change this template use File | Settings | File Templates.
 */

class Campaigns extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index()
    {
        $create_time = time();
        $campaign = Campaign::first(array(
            'conditions' => 'display=1 AND end_time>='.$create_time,
            'order' => 'id DESC'
        ));

        if(!$campaign) {
            show_404();
        }
        //导航栏高亮样式
        $this->load->vars('selectNavli_teleplays','selectNavli');
        $this->render('campaigns/index');
    }

    public function show($id = 0,$offset=0){
        if(!is_numeric($id) && $id == 0) {
            show_404();
        }

        $this->load->library('pagination');
        $limit = 10;

        $teleplay_count = CampaignTeleplay::count(array(
            'conditions' => "campaign_id={$id}"
        ));


        $campaign_teleplays = CampaignTeleplay::all(array(
            'conditions' => "campaign_id={$id}",
            'limit' => $limit,
            'offset' => $offset,
        ));

        if(!$teleplay_count) {
            show_404();
        }

        $config['base_url'] = base_url('campaigns/show/'.$id.'/');
        $config['total_rows'] = $teleplay_count;
        $config['per_page'] = $limit;
        $config['first_link'] = '第一页';
        $config['last_link'] = '尾页';

        $this->pagination->initialize($config);

        //剧集
        $this->load->vars('campaign_teleplays',$campaign_teleplays);
        $this->load->vars('teleplay_count',count($campaign_teleplays));

        //导航栏高亮样式
        $this->load->vars('selectNavli_teleplays','selectNavli');

        $this->render('campaigns/show');
    }
}