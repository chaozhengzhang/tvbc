<link rel="stylesheet" type="text/css" href="/public/css/demo1.css"/>
<link rel="stylesheet" type="text/css" href="/public/css/bookblock.css"/>
<link href="/public/css/campaigns.css" rel="stylesheet" type="text/css"/>
<style>
    .campaign_content a { color: #0000ff;}
</style>
<div class="campaign_main">
    <div style="float: left;margin: 37px 0 0 55px;">
        <div>
            <img src="/public/images/campaigns/title.png" alt="TVBC"/>
        </div>
        <div class="campaign_time"></div>
        <div class="campaign_content">
            <table style="border: 0;margin-top: 5px; font-size: 14px; font-weight: 600;">
                <tr>
                    <td style="width: 160px;"><img src="/public/images/campaigns/campaign-time-1.png" alt="TVBC"></td>
                    <td style="width: 160px;"><img src="/public/images/campaigns/campaign-time-2.png" alt="TVBC"></td>
                    <td><img src="/public/images/campaigns/campaign-time-3.png" alt="TVBC"></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">
                        <p>a. TVB剧集文件夹</p>
                        <p>b. TVB精美笔记本</p>
                    </td>
                    <td style="vertical-align: top;">
                        <p>a.TVB明星签名海报</p>
                        <p>b.TVB明星签名剧照</p>
                    </td>
                    <td style="vertical-align: top;">
                        <p>a. 2014北京电视节入场名额</p>
                        <p>b. “TVB年度盛典之旅”名额，将免费</p>
                        <p>获得一张往返香港机票，一探神秘的</p>
                        <p>将军澳电视城，参加 2014TVB 年度</p>
                        <p>盛典。</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <img src="/public/images/campaigns/campaign-time-step.png" alt="TVBC">
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p>1. 扫描下方二维码，<a href="http://tvbc.com.cn/downloads" target="_blank">下载iTVB官方移动客户端</a>;</p>
                        <p>2. 关注“翡翠东方TVBC”官方微信账号，并注册成为微信会员;</p>
                        <p>3. <a href="http://www.tvbc.com.cn/users/signin" target="_blank">注册</a>成为翡翠东方TVBC官网粉丝俱乐部会员;</p>
                        <p>4. 在<a href="http://tvbc.com.cn" target="_blank">TVBC官网</a>、<a href="http://tv.tudou.com/TVB/index.html" target="_blank">土豆网</a>、<a href="http://tv.youku.com/hk/indextvb" target="_blank">优酷网</a>任何一个平台点击收看任何一部TVB暑期剧场大片;</p>
                        <p>5. 参与暑期剧场线上话题讨论、网上投票、转帖、或参与<a href="http://weibo.com/tvbc2012" target="_blank">微博</a>互动活动。</p>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 30px;">
            <img src="/public/images/campaigns/campaign-code.png" alt="TVBC">
        </div>
    </div>
    <div style="float: left;margin-left: 30px;margin-top: 250px;">
        <div style="background-color: #0d66a4;text-align: center; height: 48px;width: 248px; color: #FFFFFF;line-height: 48px;font-size: 24px;">暑期佳片推荐</div>
        <div style="border: 1px solid #262d80; border-top: 0px;">
            <table style="border: 0px;" class="campaign_teleplay">
                <tr>
                    <td>
                        <a href="/teleplays/show/83" target="_blank">
                            <img src="/public/images/campaigns/zjr.jpg" style="height: 140px;margin-top: 5px;">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="/teleplays/show/81" target="_blank">
                            <img src="/public/images/campaigns/dj.jpg" style="height: 150px;">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="/teleplays/show/76" target="_blank">
                            <img src="/public/images/campaigns/swl.jpg">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="/teleplays/show/82" target="_blank">
                        <img src="/public/images/campaigns/hsql.jpg">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="/teleplays/show/95" target="_blank">
                        <img src="/public/images/campaigns/xqj.jpg" style="height: 145px;">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="/teleplays/show/80" target="_blank">
                        <img src="/public/images/campaigns/nrjlb.jpg">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="text-align: right;margin-right: 5px;">
                            <a href="/campaigns/show/<?php echo $campaign->id;?>" style="color: #0c65a4;">more更多</a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
