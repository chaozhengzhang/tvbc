<div class="magicCon">
    <ul>
        <?php
            if(isset($teleplays)) {
                foreach ($teleplays as $teleplay) {
        ?>
        <li>
            <a href="<?php echo base_url('teleplays/show/'.$teleplay->id);?>" target="_blank">
                <img src="<?php echo base_url('uploads/teleplays/poster/small/'.$teleplay->poster);?>"/>
            </a>
            <strong>
                <a href="<?php echo base_url('teleplays/show/'.$teleplay->id);?>" target="_blank">
                    <?php echo $teleplay->name;?>
                </a>
                <em>
                    <?php if($teleplay->is_hot) {?>
                        <img src="http://test.tvbc.com.cn/public/images/hot.gif" />
                    <?php } else if($teleplay->is_await) {?>
                        <img src="http://test.tvbc.com.cn/public/images/coming.gif" />
                    <?php }?>
                </em>
            </strong>
            <p>
                <span>监制：<?php echo character_cn_limiter($teleplay->producer,4);?></span>
                <span>编审：<?php echo character_cn_limiter($teleplay->copy_editor,4);?></span>
                <span>演员：<?php echo character_cn_limiter($teleplay->starring,4);?></span></p>
        </li>
        <?php
                }
            }
        ?>
    </ul>
</div>