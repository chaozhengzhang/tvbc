<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-22
 * Time: 下午9:46
 * To change this template use File | Settings | File Templates.
 */

class MY_Controller extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->user = $this->session->userdata('user');
        $this->load->vars('user',$this->user);
    }

    public function render($view,$action=''){
        $create_time = time();
        $campaign = Campaign::first(array(
            'conditions' => 'display=1 AND end_time>='.$create_time,
            'order' => 'id DESC'
        ));

        if($campaign) {
            $this->load->vars('campaign',$campaign);
        }

	$show_logo = '';
	if($action != ''){
	   $show_logo = 1;
	}
        $index_bg = System::find_by_name('index_bg_default');
        $this->load->vars('index_bg',$index_bg->values);
        $this->load->vars('show_logo',$show_logo);
        $this->load->view('shared/header');
        $this->load->view($view);
        $this->load->view('shared/footer');
    }
}
