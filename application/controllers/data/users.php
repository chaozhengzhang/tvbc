<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-26
 * Time: 下午9:04
 * To change this template use File | Settings | File Templates.
 */

include_once APPPATH."third_party/PHPExcel.php";
class Users extends REST_Controller {


    public function read()
    {
        $params = $this->get();

        $response = User::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function login(){
        $params = $this->post();
        $user = User::find_by_name($params['name_email']);

        if($user) {
            if($user->password == sha1($params['password'].$user->nonce)) {
                $data = $user->to_array();
                unset($data['password']);
                unset($data['nonce']);
                $this->session->set_userdata('user',$data);
                echo '{"info":"","status":"y"}';
            } else {
                echo '{"info":"用户名或密码错误！","status":"n"}';
            }
        } else {
            echo '{"info":"用户名或密码错误！","status":"n"}';
        }

    }

    public function valid_email(){
        $email = $this->post('param');
        $user = User::find_by_email($email);
        if(!$user) {
            echo '{"info":"","status":"y"}';
        } else {
            echo '{"info":"该邮箱已经存在！","status":"n"}';
        }
    }

    public function register()
    {
        $data = $this->post();
        $nonce = rand(10001,99999);
        $data['nonce'] = $nonce;
        $data['create_time'] = time();
        $data['password'] = sha1($data['password'].$nonce);
        $data['birthday'] = strtotime($data['year'].'-'.$data['month'].'-'.$data['day']);
        $artists = implode(',',array_unique($data['artist']));

        $data['artists'] = $artists.','.$data['other'];

        $province = Province::find($data['province']);
        $data['province'] = $province->name;

        $city = City::find($data['city']);
        $data['city'] = $city->name;

        if(!empty($data['area'])) {
            $area = Area::find($data['area']);
            $data['area'] = $area->name;
        }
        unset($data['rep_password']);
        unset($data['year']);
        unset($data['month']);
        unset($data['day']);
        unset($data['artist']);
        unset($data['other']);
        if(!User::find_by_email($data['email'])) {
            $user = User::create($data);
            if($user) {
                $this->session->set_userdata('user',$data);
                echo '{"info":"","status":"y"}';
            } else {
                echo '{"info":"用户邮箱已经被使用！","status":"n"}';
            }
        } else {
            echo '{"info":"用户邮箱已经被使用！","status":"n"}';
        }
    }

    public function export(){
        $response = User::get_by_params();
        $file_name = time().'.xls';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('微软雅黑')->setSize(9);

        $results = $response['results'];

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '姓名')
            ->setCellValue('B1', '电话')
            ->setCellValue('C1', '邮箱')
            ->setCellValue('D1', '性别')
            ->setCellValue('E1', '生日')
            ->setCellValue('F1', '喜欢的明星')
            ->setCellValue('G1', '接受发送信息')
            ->setCellValue('H1', '注册时间')
            ->setCellValue('I1', '所在地区')->getStyle("A1:H1")->getFont()->setBold(true);

        $i = 2;
        foreach($results as $data) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $data['name'])
                ->setCellValue('B'.$i, $data['phone'])
                ->setCellValue('C'.$i, $data['email'])
                ->setCellValue('D'.$i, $data['sex'] == 1 ? "男":"女")
                ->setCellValue('E'.$i, $data['birthday'])
                ->setCellValue('F'.$i, $data['artists'])
                ->setCellValue('G'.$i, $data['pushed'] ? '是' :'否')
                ->setCellValue('H'.$i, $data['create_time'])
                ->setCellValue('I'.$i, $data['province'].'-'.$data['city']);
            $i++;
        }

        // 设置工资表名称
        $objPHPExcel->getActiveSheet()->setTitle('用户信息');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'"');
        //header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: '.gmdate('D, d M Y H:i:s').' GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function forgot()
    {
        $data = $this->post();
        $user = User::find_by_name($data['username']);
        if($user && $user->email == $data['email']) {
            $name = $user->name;
            $password = random_string('alnum',6);
            $nonce = rand(10001,99999);
            $now_time = date('Y-m-d');
            $message = <<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <table border="0" width="100%" height="100">
        <tr>
            <td>
                亲爱的用户:{$name}，您好！
            </td>
        </tr>
        <tr>
            <td>
                感谢您注册翡翠东方TVBC官网。
            </td>
        </tr>
        <tr>
            <td>
                您已经进行了密码找回操作，您的新密码为： {$password}
            </td>
        </tr>
        <tr>
            <td>
                翡翠东方TVBC祝您生活愉快，万事如意。
            </td>
        </tr>
        <tr>
            <td>
                <img src="http://www.tvbc.com.cn/images/logo.png">
            </td>
        </tr>
        <tr>
            <td>
                上海翡翠东方传播有限公司（TVBC）
            </td>
        </tr>
        <tr>
            <td>
                {$now_time}
            </td>
        </tr>
    </table>
</body>
</html>
HTML;
            $to  = $user->email; // note the comma
            $subject = '翡翠东方TVBC,用户密码找回';
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

            $headers .= 'To: '.$user->email . "\r\n";
            $headers .= 'From: norepeat@tvbc.com.cn>' . "\r\n";
            $is_send = mail($to, $subject, $message, $headers);

            if($is_send) {
                $user->nonce = $nonce;
                $user->password = sha1($password.$nonce);
                $user->save();
                $this->session->set_userdata('email',$user->email);
                echo '{"info":"","status":"y"}';
            }
        } else {
            echo '{"info":"用户或注册邮箱不存在！","status":"n"}';
        }
    }
}