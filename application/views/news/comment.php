<script type="text/javascript" src="<?php echo base_url('public/js/limitTextarea.js'); ?>"></script>
<link type="text/css" rel="stylesheet"
      href="<?php echo base_url('public/plugin/validform/css/validform.css'); ?>">
<link type="text/css" rel="stylesheet"
      href="<?php echo base_url('public/plugin/fancyBox/css/jquery.fancybox.css'); ?>">
<script type="text/javascript"
        src="<?php echo base_url('public/plugin/validform/js/Validform_v5.3.2_min.js'); ?>"></script>
<script type="text/javascript"
        src="<?php echo base_url('public/plugin/fancyBox/jquery.mousewheel-3.0.6.pack.js'); ?>"></script>
<script type="text/javascript"
        src="<?php echo base_url('public/plugin/fancyBox/jquery.fancybox.pack.js'); ?>"></script>
<script type="text/javascript">

    $(function () {
        $(".comment-form").Validform({
            tiptype: function (msg, o, cssctl) {
                if (!o.obj.is("form")) {
                } else {
                    var objtip = o.obj.find("#msgdemo");
                    cssctl(objtip, o.type);
                    objtip.text(msg);
                }
            },
            showAllError: true,
            ajaxPost: true,
            callback: function (data) {
                if (data.status == 'y') {
                    window.location.href = "/news/comment/<?php echo $info->id;?>";
                } else {
                    alert(data.info)
                }
            }
        });

        $('.fancybox').fancybox();
        $(".login-form").Validform({
            tiptype: function (msg, o, cssctl) {
                if (!o.obj.is("form")) {
                    var objtip = o.obj.siblings(".Validform_checktip");
                    cssctl(objtip, o.type);
                    objtip.text(msg);
                } else {
                    var objtip = o.obj.find("#msgdemo");
                    cssctl(objtip, o.type);
                    objtip.text(msg);
                }
            },
            showAllError: true,
            ajaxPost: true,
            callback: function (data) {
                if (data.status == 'y') {
                    window.location.reload();
                }
            }
        });
    });
</script>
<div class="l-Div newslist comment-list">
    <div>
        <h2>
            <a href="<?php echo base_url('news/show/' . $info->id); ?>" class="redcor">
                <font style="color:<?php echo $info->title_color; ?>;"><?php echo $info->title; ?></font>
            </a>
        </h2>
    </div>
    <ul>
        <li>
            <?php $this->load->view('shared/comment'); ?>
        </li>
        <?php
        if (isset($comments)) {
            foreach ($comments as $key=>$comment) {
                ?>
                <li>
                    <span><b><?php echo $comment->user->name; ?></b><label
                            style="margin-left: 20px;"><?php echo date('Y-m-d H:i:s', $comment->create_time); ?>
                        </label>
                    </span>

                    <div class="x-detail">
                        <?php echo $comment->detail(); ?>
                    </div>
                    <div class="comment-tool">
                        <span class="x-tool-button x-indorsement-button" id="comment-id-<?php echo $comment->id; ?>">支持
                            <em class="x-indorsement">(<?php echo $comment->indorsement_count(); ?>)</em>
                        </span>
						<?php if(!isset($user)) {?>
							<span id="signin-tip">要发布评论,请先<a class="fancybox" href="#usre-signin-view" id="sign-in-button">登录</a></span>
						<?php } else {?>
							<span class="x-tool-button x-reply-button">回复</span>
						<?php } ?>
                        <span class="x-tool-button">分享</span>
                    </div>
                    <div class="comment-reply">
                        <form class="comment-form" method="post" action="<?php echo base_url('data/comments'); ?>">
                            <div class="comment-detail">
                                <span class="detail-info" id="detail-info-<?php echo $key + 1;?>">
                                    <span class="limit-tip" id="limit-tip-<?php echo $key + 1;?>">还可以输入</span>
                                    <b class="info-limit" id="info-limit-<?php echo $key + 1;?>">150</b>字
                                </span>
                                <input type="hidden" name="comment_id" value="<?php echo $comment->id; ?>">
                                <input type="hidden" name="info_id" value="<?php echo $info->id; ?>">
                            </div>
                            <div class="comment-detail-area">
                                <textarea class="comment-detail-text" id="detail-<?php echo $key + 1;?>" name="detail" rows="6" datatype="*1-200"
                                          class="inputxt" nullmsg="回复内容不能为空"></textarea>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#detail-<?php echo $key + 1;?>' ).limitTextarea({
                                            maxNumber: 200,     //最大字数
                                            info_limit: '#info-limit-<?php echo $key + 1;?>',
                                            onOk: function () {
                                                $('#detail-<?php echo $key + 1;?>').css('border-color', '#CCC');
                                                $('#info-limit-<?php echo $key + 1;?>').css('color', '#000');
                                            },
                                            onOver: function () {
                                                $('#detail-<?php echo $key + 1;?>').css('border-color', '#F00');
                                                $('#info-limit-<?php echo $key + 1;?>').css('color', '#F00');
                                                $('#limit-tip-<?php echo $key + 1;?>').html('已超出');
                                            }
                                        });
                                    });
                                </script>
								<?php if(!isset($user)) {?>
									<span id="signin-tip">要回复评论,请先<a class="fancybox" href="#usre-signin-view" id="sign-in-button">登录</a></span>
								<?php } else {?>
									<button id="publish-button" class="comment-button">回复</button>
								<?php } ?>
                            </div>
                        </form>
                    </div>
                    <?php
                    if($comment->replies) {
                    ?>
                    <div class="x-reply">
                        <dl>
                            <?php
                                foreach($comment->replies as $key=>$replie) {
                            ?>
                                <dt style="margin: 5px 0;"><span class="x-user-name"><?php echo $replie->user->name; ?></span>
                                    <span>
                                        <?php echo date('Y-m-d H:i:s', $replie->create_time); ?></span>
                                    <span style="float: right"><?php echo $key + 1;?></span>
                                </dt>
                                <dd>
                                    <div class="x-detail">
                                        <?php echo $replie->detail(); ?>
                                    </div>
                                    <div style="text-align: right;height: 25px;margin: 10px 0 0;">
                                        <span class="x-tool-button">分享</span>
                                    </div>
                                </dd>
                            <?php
                            }
                            ?>
                        </dl>
                    </div>
                    <?php
                    }
                    ?>
                </li>
            <?php
            }
        }
        ?>
    </ul>
    <div class="page-nation" style="margin: 5px;clear: both;">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <script type="text/javascript">
        $(function () {

            $('.x-indorsement-button').click(function (e) {
                var me = this, id = this.id.split('-');
                $.post('<?php echo base_url('data/indorsements')?>',
                    {comment_id: id[id.length - 1],info_id: <?php echo $info->id;?>},
                    function (data) {
                        $(me).html('<em class="x-indorsement-gay">已支持(' + data.info + ')</em>');
                        $(me).unbind('click');
                    }, 'json'
                );
            });

            $('.x-reply-button').click(function(){
                var me = this;
                $(this).parent().next().slideToggle(500);
            });
        });
    </script>
</div>
<?php $this->load->view('shared/search_tool'); ?>
<?php $this->load->view('shared/signin'); ?>