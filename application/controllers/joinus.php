<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-23
 * Time: 下午8:11
 * To change this template use File | Settings | File Templates.
 */

class Joinus extends MY_Controller {

    public function index(){

        //地区
        $regions = Region::all();

        $this->load->vars('regions',$regions);

        //招聘
        $joins = Join::get_with_region();


        $this->load->vars('joins',$joins);

        //导航栏高亮
        $this->load->vars('selectNavli_joinus','selectNavli');
        $this->render('joinus/index');
    }
}