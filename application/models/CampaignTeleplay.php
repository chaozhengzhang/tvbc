<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-24
 * Time: 下午11:27
 * To change this template use File | Settings | File Templates.
 */

class CampaignTeleplay extends ActiveRecord\Model {
    static $belongs_to = array(
        array('campaign','class_name' => 'Campaign'),
        array('teleplay','class_name' => 'Teleplay')
    );


    static function get_by_params($params = array())
    {
        $columns = self::connection()->columns('campaign_teleplays');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        foreach ($columns as $columnKey => $columnValue) {
            if (isset($params[$columnKey])) {
                if ($columnValue == 'varchar') {
                    $conditions .= " AND $columnKey LIKE '%" . $params[$columnKey] . "%'";
                } else {
                    $conditions .= " AND $columnKey=" . $params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions' => $conditions));
        $data['results'] = array();

        if ($data['total'] > 0) {
            $activities = self::all($options);

            if ($activities) {
                $data['results'] = array_map(function ($row) {
                    $_data = $row->to_array();
                    //$_data['campaign_name'] = $row->campaign ? $row->campaign->name : '';
                    $_data['teleplay_name'] = $row->teleplay ? $row->teleplay->name : '';
                    return $_data;
                }, $activities);
            }
        }

        return $data;
    }
}