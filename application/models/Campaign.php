<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-24
 * Time: 下午10:23
 * To change this template use File | Settings | File Templates.
 */

class Campaign extends ActiveRecord\Model {

    static function get_by_params($params = array()){
        $columns = self::connection()->columns('campaigns');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        $conditions = ' 1=1 ';

        foreach($columns as $columnKey=>$columnValue) {
            if(isset($params[$columnKey])) {
                if($columnValue->raw_type == 'varchar') {
                    $conditions .=  " AND $columnKey LIKE '%".$params[$columnKey]."%'";
                } else {
                    $conditions .=  " AND $columnKey=".$params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions'=>$conditions));
        $data['results'] = array();

        if($data['total'] > 0) {
            $activities = self::all($options);

            if($activities) {
                $data['results'] = array_map(function($row){
                    $_data = $row->to_array();
                    $_data['start_time'] = date('Y-m-d',$row->start_time);
                    $_data['end_time'] = date('Y-m-d',$row->end_time);
                    $_data['create_time'] = date('Y-m-d H:i:s',$row->create_time);
                    return $_data;
                },$activities);
            }
        }

        return $data;
    }

}