<?php

class Systems extends REST_Controller {

    public function set_index_bg()
    {
        $this->load->library('upload');
        $directorys = array(
            './uploads/systems/'
        );
        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
        }
        //index_bg_default.png
        $bg_index = System::find_by_name('index_bg_default');
        $bg_index->values = $data['file_name'];
        $bg_index->save();
        $this->response($data, 1, '上传成功！');
    }

    public function set_index_bg_defualt()
    {
        $bg_index = System::find_by_name('index_bg_default');
        $bg_index->values = 'index_bg_default.png';
        $bg_index->save();
        $this->response($bg_index->to_array(), 1, '设置成功！');
    }
} 