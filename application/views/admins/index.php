<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>TVBC-数据管理平台v1.0</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/extjs/resources/css/ext-all-debug.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/admin_main.css'); ?>"/>
<script type="text/javascript">
    function baseUrl(url) {
        return '<?php echo base_url();?>' + url;
    }
</script>
<script src="<?php echo base_url('public/extjs/ext-all-debug.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/kindeditor/kindeditor-all.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/kindeditor/lang/zh_CN.js');?>"></script>
<script src="<?php echo base_url('public/extjs/locale/ext-lang-zh_CN.js'); ?>"></script>
<script>

Ext.Loader.setConfig({
    enabled: true,
    paths: {
        'Ext.ux': baseUrl('public/extjs/ux')
    }
});

Ext.require([
    'Ext.ux.form.CheckGroupField', 'Ext.ux.window.Notification',
    'Ext.ux.data.NotificationStore', 'Ext.ux.DataView.DragSelector',
    'Ext.ux.DataView.LabelEditor','Ext.ux.RowExpander',
    'Ext.ux.ProgressColumn', 'Ext.ux.CheckColumn',
    'Ext.ux.statusbar.StatusBar','Ext.ux.SwfUploadPanel',
    'Ext.ux.form.field.UploadField'
]);

Ext.define('Ext.ux.RestPassWordWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.restpasswordwindow',
    title: '密码修改',
    width: 400,
    height: 180,
    modal: true,
    y: 150,
    frame: false,
    plain: true,
    autoShow: true,
    layout: 'fit',
    url: '',
    initComponent: function (config) {
        var me = this;
        Ext.apply(me, config);
        this.items = [
            {
                xtype: 'form',
                frame: false,
                resizable: false,
                border: false,
                defaultType: 'textfield',
                bodyPadding: 10,
                defaults: {
                    anchor: '95%',
                    allowBlank: false,
                    selectOnFocus: true,
                    msgTarget: 'side',
                    labelAlign: 'right'
                },
                items: [
                    {
                        anchor: '80%',
                        inputType: 'password',
                        fieldLabel: '密码',
                        blankText: '密码不能为空',
                        emptyText: '请输入密码',
                        name: 'password',
                        minLength: 6,
                        id: 'new_password'
                    },
                    {
                        anchor: '80%',
                        inputType: 'password',
                        vtype: 'password',
                        initialPassField: 'new_password',
                        name: 'pass_cfrm',
                        minLength: 6,
                        fieldLabel: '重复密码',
                        blankText: '重复密码不能为空',
                        emptyText: '请输入重复密码'
                    }
                ],
                buttonAlign: 'center',
                buttons: [
                    {
                        text: '保存',
                        handler: function () {
                            var form = this.up('form').getForm();
                            if (form.isValid()) {
                                form.submit({
                                    url: me.url,
                                    success: function (form, action) {
                                        Ext.Msg.alert('提示', action.result.message);
                                        form.reset();
                                        me.close();
                                    },
                                    failure: function (form, action) {
                                        Ext.Msg.alert('错误', action.result.message);
                                        form.reset();
                                    }
                                });

                            }
                        }
                    },
                    {
                        text: '重置',
                        handler: function () {
                            this.up('form').getForm().reset();
                        }
                    }
                ]
            }
        ];
        me.callParent(arguments);
    },
    afterRender: function (cmp, options) {
        this.callParent();

    }
});

Ext.define('Ext.ux.DefualtView', {
    extend: 'Ext.Viewport',
    alias: 'widget.defualtview',
    layout: {
        type: 'border',
        padding: 5
    },
    initComponent: function (config) {
        config = config || {};
        var me = this;

        var treePanelStore = Ext.create('Ext.data.TreeStore', {
            root: {
                expanded: true
            },
            proxy: {
                type: 'ajax',
                url: baseUrl('data/admins/get_navigation')
            },
            fields: ['id', 'text', 'leaf', 'controller']
        });

        me.treePanel = Ext.create('Ext.tree.Panel', {
            store: treePanelStore,
            border: 0,
            rootVisible: false
        });

        var northView = Ext.widget('container', {
            region: 'north',
            autoScroll: true,
            border: 0,
            items: [
                {
                    xtype: 'toolbar',
                    border: 0,
                    frame: true,
                    style: {
                        backgroundColor: '#3892D3'
                    },
                    items: ['<div><b style="font-size: 16px;">TVBC-数据管理平台v1.0</b></div>', '->', {
                        text: 'Administrator',
                        xtype: 'splitbutton',
                        menu: {
                            xtype: 'menu',
                            items: [
                                {
                                    text: '密码设置',
                                    handler: function () {
                                        Ext.widget('restpasswordwindow', {
                                            url: baseUrl('data/admins/update')
                                        });
                                    }
                                },
                                {
                                    text: '注销',
                                    handler: function () {
                                        window.location.href = baseUrl('data/admins/logout');
                                    }
                                }
                            ]
                        }
                    }, '']
                }
            ]
        });

        me.containerView = Ext.widget('panel', {
            border: false,
            region: 'center',
            id: 'center-content-view',
            autoScroll: true,
            loader: {}
        });

        me.containerView.getLoader().addListener('load', function (loader, response, options, eOpts) {
            var store = this.treePanel.getStore(), childNode = store.getNodeById(options.nodeId);
            this.treePanel.getSelectionModel().select(childNode);
        }, me);

        var westView = Ext.widget('panel', {
            region: 'west',
            width: '17%',
            title: '',
            split: true,
            collapsible: true,
            autoScroll: true,
            items: [
                me.treePanel
            ]
        });


        me.items = [northView, westView, me.containerView];
        Ext.apply(me, config);
        me.callParent(arguments);
    },
    onBoxReady: function (view, width, height, eOpts) {
        var me = this;
        me.callParent(arguments);
        me.treePanel.getSelectionModel().on('select', me.onLeafSelection, me);
    },
    onLeafSelection: function (selModel, record) {
        var me = this, loaderView = me.containerView.getLoader();

        if (record.get('leaf')) {
            loaderView.load({
                autoLoad: true,
                loadMask: {
                    msg: '页面加载中......'
                },
                removeAll: true,
                scope: this,
                scripts: true,
                url: record.get('controller')
            });
        }
    }
});


Ext.onReady(function () {
    Ext.QuickTips.init();

    Ext.widget('defualtview');
});
</script>
<script src="<?php echo base_url('public/js/vtypes.js'); ?>"></script>
<script src="<?php echo base_url('public/js/model.js'); ?>"></script>
<script src="<?php echo base_url('public/js/data.js'); ?>"></script>
<script src="<?php echo base_url('public/js/store.js'); ?>"></script>
<script src="<?php echo base_url('public/js/tools.js'); ?>"></script>
<script src="<?php echo base_url('public/swfupload/swfupload.js');?>"></script>
</head>
<body>
</body>
</html>
