
/**
 * 商圈
 */
var businessCircleStore = Ext.create( 'Ext.data.Store', {
    model : 'BusinessCircle',
    proxy : {
        type : 'rest',
        startParam: 'offset',
        url: baseUrl('data/businesscircles'),
        reader : {
            type : 'json',
            root : 'results'
        }
    },
    autoLoad : false
} );


/**
 * 餐厅
 */
var restaurantStore = Ext.create( 'Ext.ux.data.NotificationStore', {
    model : 'Restaurant',
    autoLoad : false,
    autoSync : true,
    proxy : {
        url : baseUrl('data/restaurants')
    }
});

