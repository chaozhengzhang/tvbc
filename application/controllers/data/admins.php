<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zhang.chaozheng
 * Date: 13-5-27
 * Time: 下午4:57
 * To change this template use File | Settings | File Templates.
 */

class Admins extends REST_Controller {

    /**
     * 用户登录
     */
    public function login(){
        $data = $this->post();
        $admin = Admin::find_by_name($data['name']);
        if($admin && $admin->password = sha1($data['password'].$admin->nonce)) {
            $this->session->set_userdata('admin',$admin->to_array());
            $this->response(NULL,1,'登陆成功！');
        } else {
            $this->response(NULL,1,'用户名或密码错误！',FALSE);
        }
    }

    /**
     * 用户退出登录
     */
    public function logout(){
        $this->session->unset_userdata('admin');
        $this->session->sess_destroy();
        $_SESSION['admin']=null;
        session_destroy();

        redirect(base_url('admin/signin'));
    }

    /**
     *
     */
    public function get_navigation()
    {
        echo <<<JSON
{
    children: [{
            text: "艺人管理",
            expanded: true,
            children: [{
                    text: "新增/编辑",
                    controller: "admin/artists",
                    leaf: true
                }, {
                    text: "相册",
                    controller: "admin/artists/albums",
                    leaf: true
                }
            ]
        }, {
            text: "影视剧管理",
            expanded: true,
            children: [{
                    text: "新增/编辑",
                    controller: "admin/teleplays",
                    leaf: true
                }, {
                    text: "剧照",
                    controller: "admin/teleplays/stills",
                    leaf: true
                }, {
                    text: "剧集",
                    controller: "admin/teleplays/dramas",
                    leaf: true
                }
            ]
        }, {
            text: "新闻管理",
            expanded: true,
            children: [{
                    text: "新增/编辑",
                    controller: "admin/infos",
                    leaf: true
                },{
                    text: "列表",
                    controller: "admin/infos/show_list",
                    leaf: true
                },{
                    text: "首页新闻设置",
                    controller: "admin/infos/infobanners",
                    leaf: true
                }
            ]
        }, {
            text: "招聘管理",
            expanded: true,
            children: [{
                    text: "职位编辑",
                    controller: "admin/positions",
                    leaf: true
                }, {
                    text: "信息编辑",
                    controller: "admin/joins",
                    leaf: true
                }
            ]
        }, {
            text: "其它管理",
            expanded: true,
            children: [{
                    text: "首页轮播",
                    controller: "admin/systems/sliders",
                    leaf: true
                }, {
                    text: "公司介绍视频",
                    controller: "admin/systems/video",
                    leaf: true
                }
            ]
        }, {
            text: "活动管理",
            expanded: true,
            children: [{
                    text: "编辑",
                    controller: "admin/campaigns",
                    leaf: true
                }
            ]
        }, {
            text: "用户管理",
            expanded: true,
            children: [{
                    text: "查看",
                    controller: "admin/users",
                    leaf: true
                }
            ]
        }, {
            text: "系统设置",
            expanded: true,
            children: [{
                    text: "首页背景管理",
                    controller: "admin/systems/setting",
                    leaf: true
                }
            ]
        }
    ]
}
JSON;
    }
}


