<script type="text/javascript">
Ext.define('Ext.ux.UserView', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.userview',
    title: '用户列表(* 双击表格中的数据即可进入编辑状态)',
    minHeight: 320,
    maxHeight: 520,
    initComponent: function (config) {
        var me = this;
        Ext.apply(me, config || {});

        me.store = Ext.create('Ext.ux.data.NotificationStore', {
            model: 'User',
            proxy: {
                url: baseUrl('data/users')
            }
        });

        /*var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
         errorSummary: false,
         listeners: {
         cancelEdit: function (rowEditing, context) {
         if (context.record.phantom) {
         me.store.remove(context.record);
         }
         }
         }
         });

         me.plugins = [rowEditing];*/

        me.columns = [
            {
                text: '编号',
                width: 60,
                sortable: true,
                dataIndex: 'id'
            },
            {
                text: '姓名',
                flex: 1,
                sortable: true,
                dataIndex: 'name'
            },
            {
                text: '电话',
                flex: 1,
                sortable: true,
                dataIndex: 'phone'
            },
            {
                text: '邮箱',
                flex: 1,
                sortable: true,
                dataIndex: 'email'
            },
            {
                text: '性别',
                flex: 1,
                sortable: true,
                dataIndex: 'sex',
                renderer: function (v) {
                    return v == 1 ? '男' : '女';
                }
            },
            {
                text: '生日',
                flex: 1,
                sortable: true,
                dataIndex: 'birthday'
            },
            {
                text: '所在地',
                flex: 1,
                sortable: true,
                dataIndex: 'province',
                renderer: function (v,metaDate,record) {
                    return v + '-' + record.get('city');
                }
            },
            {
                text: '喜欢的明星',
                flex: 1,
                sortable: true,
                dataIndex: 'artists'
            },
            {
                text: '接受发送信息',
                flex: 1,
                sortable: true,
                dataIndex: 'pushed',
                renderer: function (v) {
                    return v == 1 ? '是' : '否';
                }
            },
            {
                text: '注册日期',
                flex: 1,
                sortable: true,
                dataIndex: 'create_time'
            }
        ];

        me.tbar = [
            {
                xtype: 'form',
                border: 0,
                defaultType: 'textfield',
                fieldDefaults: {
                    labelAlign: 'right',
                    labelWidth: 60
                },
                items: [
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaults: {
                            flex: 1
                        },
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: '用户名',
                                name: 'name'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '邮箱',
                                name: 'email'
                            },
                            {
                                xtype: 'combo',
                                width: 160,
                                fieldLabel: '性别',
                                name: 'sex',
                                displayField: 'name',
                                valueField: 'id',
                                store: Ext.create('Ext.data.Store', {
                                    autoDestroy: true,
                                    fields: ['id', 'name'],
                                    data: [
                                        {id: 1, name: '男'},
                                        {id: 2, name: '女'}
                                    ]
                                }),
                                queryMode: 'local',
                                typeAhead: true
                            }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        defaults: {
                            flex: 1
                        },
                        items: [
                            {
                                xtype: 'datefield',
                                fieldLabel: '注册日期',
                                format: 'Y-m-d',
                                name: 'start_time',
                                itemId: 'startdt',
                                vtype: 'daterange',
                                endDateField: 'enddt'
                            },
                            {
                                xtype: 'displayfield',
                                value: '-',
                                margin: '0 5'
                            },
                            {
                                xtype: 'datefield',
                                format: 'Y-m-d',
                                hideLabel: true,
                                name: 'end_time',
                                itemId: 'enddt',
                                vtype: 'daterange',
                                startDateField: 'startdt'
                            },
                            {
                                xtype: 'combo',
                                labelWidth: 100,
                                width: 180,
                                fieldLabel: '接受发送信息？',
                                name: 'pushed',
                                displayField: 'name',
                                valueField: 'id',
                                store: Ext.create('Ext.data.Store', {
                                    autoDestroy: true,
                                    fields: ['id', 'name'],
                                    data: [
                                        {id: 1, name: '是'},
                                        {id: 2, name: '否'}
                                    ]
                                }),
                                queryMode: 'local',
                                typeAhead: true
                            }
                        ]
                    }
                ],
                buttonAlign: 'center',
                buttons: [
                    {
                        text: '查询',
                        handler: function(){
                            var form = this.up('form').getForm();
                            me.store.proxy.extraParams = form.getValues();
                            me.store.load();
                        }
                    },
                    {
                        text: '重置',
                        handler: function(){
                            var form = this.up('form').getForm();
                            form.reset();
                        }
                    },
                    {
                        text: '导出',
                        handler: function(){
                            var form = this.up('form').getForm();
                            window.open(baseUrl('data/users/export',Ext.Object.toQueryString(form.getValues())));
                        }
                    }
                ]
            }
        ];

        me.bbar = Ext.create('Ext.PagingToolbar', {
            store: me.store,
            displayInfo: true
        });

        me.callParent(arguments);
    }
});

Ext.widget('userview', {
    renderTo: 'user-view'
});
</script>
<div id="user-view"></div>