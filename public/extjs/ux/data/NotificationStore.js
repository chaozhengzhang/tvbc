Ext.define('Ext.ux.data.NotificationStore', {
    extend: 'Ext.data.Store',
    alias: 'widget.NotificationStore',
    autoLoad: true,
    autoSync: true,
    pageSize: 30,
    constructor: function(config) {
        config = config || {};
        var me = this;

        var writeDefined = (typeof config.listeners != 'undefined' && typeof config.listeners.write) == 'function';
        Ext.apply(me, config);
        Ext.apply(me.proxy, {
            type: 'rest',
            startParam: 'offset',
            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'results',
                messageProperty: 'message'
            },
            writer: {
                type: 'json',
                writeAllFields: false,
                root: 'data'
            },
            listeners: {
                scope: me,
                exception: me.exceptionAction
            }
        });

        me.addEvents('afterSave');
        me.callParent(arguments);
        if ( ! writeDefined) me.addListener('write', me.writeAction, me);
    },
    exceptionAction: function(proxy, response, operation) {
        var error = operation.getError();
        this.initNotification(error.status, error.statusText);
    },
    writeAction: function(store, operation, eOpts) {
        if (this.hasListener('afterSave')) {
            this.fireEvent('afterSave', store, operation, eOpts);
        } else {
            this.initNotification(operation.action, operation.resultSet.message);
        }
    },
    initNotification: function(action, message) {
        Ext.widget('uxNotification', {
            position: 't',
            cls: 'ux-notification-light',
            iconCls: 'ux-notification-icon-information',
            closable: true,
            title: action,
            manager: 'fullscreen',
            slideInDuration: 500,
            autoCloseDelay: 4000,
            html: message
        }).show();
    }

});