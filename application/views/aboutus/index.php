<div class="l-Div aboutCon">
    <div class="videoDiv" style="position:relative; z-index:1">
        <div style="width:650px; height:400px; margin:0px auto;">
            <div id="flashcontent"></div>
            <div id="profile"></div>
        </div>
        <script type="text/javascript" src="<?php echo base_url('ckplayer/ckplayer.js');?>" charset="utf-8"></script>
        <script type="text/javascript" src="<?php echo base_url('public/js/offlights.js');?>" charset="utf-8"></script>
        <script type="text/javascript">
            var box = new LightBox('flashcontent');
            function closelights() {//关灯
                box.Show();
                $('.left_table_content').css({
                    'border': "1px solid #DBDBDB"
                });
            }
            function openlights() {//开灯
                box.Close();
                $('.left_table_content').css({
                    'border-left': "medium none"
                });
            }
            function initPlayer(url) {

                var flashvars={
                    f:url,//视频地址
                    s:'0',
                    c:'0',
                    x:'',
                    i:'',
                    u:'',
                    t:'10|10',
                    y:'',
                    e:'1',
                    v:'80',
                    p:'1',
                    h:'0',
                    q:'',
                    m:'0',
                    o:'',
                    w:'',
                    g:'',
                    j:'',
                    my_url:encodeURIComponent(window.location.href)
                };
                var params={bgcolor:'#000000',allowFullScreen:true,allowScriptAccess:'always',wmode: 'opaque'};
                var attributes={id: 'tvbc_av_video', name: 'tvbc_av_video',menu:'false'};
                swfobject.embedSWF('ckplayer/ckplayer.swf', 'profile', '600', '400', '10.0.0','ckplayer/expressInstall.swf', flashvars, params, attributes);

            }
            $(function () {
                initPlayer('<?php echo base_url('uploads/videos/1.flv')?>');
            });
        </script>
    </div>
    <div class="aboutMeg">
        <h2 class="bluecor">翡翠简介</h2>

        <p><strong style="font-size:14px; font-weight:bold">上海翡翠东方传播有限公司(TVBC)</strong>是由香港电视广播有限公司(TVB)、华人文化产业投资基金(CMC)，上海东方传媒集团有限公司(SMG)共同成立的合资公司。
        </p>

        <p>
            2012年8月8日上海翡翠东方传播有限公司正式成立，公司将致力于多元化发展，协助TVB更好地在内地开展包括广告、影视制作、版权发行、艺人合作、公关活动等全部业务。翡翠东方将根植中国文化土壤，继承TVB优良血统，以“传承娱乐精髓、弘融华人生活”为核心理念。独家运作并展开TVB在中国内地的各项业务，助力TVB实现从以香港业务为发展核心到以大中国业务为发展前沿的战略转移，缔造对全中国乃至全球华人最具影响力的华语媒体帝国。</p>

        <h3><strong>业务板块及业务内容介绍:</strong></h3>
        <h4><strong>影视剧投资</strong></h4>

        <p>沿袭TVB电视剧制作优势，不断推陈出新；同时立足国内市场，投资研发新影视作品，与国内电视台合作开创独播剧等。</P>
        <h4><strong>版权发行</strong></h4>

        <p>致力于运作TVB影视剧在内地电视界、新媒体网络平台、移动平台上的发行，以及代理发行业务。</P>
        <h4><strong>艺人经纪</strong></h4>

        <p>
            在中国内地市场包装、宣传TVB旗下艺人，协助运作影视剧拍摄、广告代言、商业演出及其他各类宣传推广活动，引入TVB成熟的艺人培训机制，面向内地市场，全面开发、包装成熟艺人及培训、发掘新艺人，寻求多元化发展，提高艺人国内知名度及海外合作机会。</P>
        <h4><strong>广告</strong></h4>

        <p>负责开展TVB平台上的常规广告投放、剧集及栏目冠名、剧集内产品植入等；承包经营土豆网TVB剧专区的新媒体广告业务。</P>
        <h4><strong>公关活动</strong></h4>

        <P>整合TVB平台影响力及丰富的艺人资源，为客户策划执行线下公关活动，以及一站式多媒体宣传与推广。</P>

        <h4><strong>新媒体项目开发</strong></h4>
        <p>依托TVB丰富资源， 拓展包括互联网视频、移动视频在内的各类新媒体项目合作，与中国移动i视界合作开发iTVB项目，在各类手机、pad等移动终端上向用户提供TVB海量影视剧、明星互动平台、港澳生活娱乐资讯等内容。</p>

        <h4><strong>文化演艺项目投资</strong></h4>
        <p>与国内知名演出制作公司合作投资并运作音乐剧等各类文化演艺项目。</p>

        <div><img src="<?php echo base_url('public/images/newbg5.png');?>"/></div>
    </div>
</div>
<?php $this->load->view('shared/search_tool');?>