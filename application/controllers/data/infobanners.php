<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-22
 * Time: 下午12:55
 * To change this template use File | Settings | File Templates.
 */

class Infobanners extends REST_Controller {

    public function read()
    {
        $params = $this->get();
        
        $response = InfoBanner::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }

    public function create(){
        $data = $this->post('data');
        unset($data['info_name']);
        if(InfoBanner::find_by_info_id($data['info_id'])){
            $this->response(NULL,0,'该新闻已经存在！',FALSE);
        } else {
            $response = InfoBanner::create($data);
            $this->response($response->to_array(),1,'增加成功！');
        }
    }

    public function update($id = 0)
    {
        $model = InfoBanner::find($id);
        $params = $this->put('data');
        unset($params['info_name']);

        if(isset($params['other_link'])) {
            $params['info_id'] = 0;
        }

        if(isset($params['info_id']) && $params['info_id'] > 0) {
            $params['other_link'] = '';
        }

        $model->update_attributes($params);
        $this->response($model->to_array(),1,'编辑成功！');
    }

    public function destroy($id){
        $model = InfoBanner::find($id);
        $model->delete();
        $this->response(NULL,1,'删除成功！');
    }

    public function upload_image(){
        $this->load->library('upload');
        $directorys = array(
            './uploads/infobanners/'
        );
        $id = $this->post('id');
        $info_banner = InfoBanner::find($id);
        $image = $info_banner->image;

        $this->create_directorys($directorys);
        $file_name = time();
        $config['upload_path'] = $directorys['0'];
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload()) {
            $data = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
            @unlink($directorys[0].$image);
        }

        $this->response($data,1,'上传成功！');
    }
}
