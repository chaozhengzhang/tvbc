<div class="personlist">
    <ul>
        <?php
            if(isset($artists)) {
                foreach ($artists as $key => $artist) {
        ?>
        <li class="pepMeb"><?php echo $key; ?></li>
                <?php
                    foreach ($artist as $data) {
                ?>
        <li>
            <a href="<?php echo base_url('artists/show/'.$data['id']);?>" target="_blank">
                <img src="<?php echo base_url('uploads/artists/cover/small/'.$data['cover']);?>" width="101" height="153"/>
            </a>
            <div class="personName">
                <?php echo $data['name']; ?>
            </div>
        </li>
                <?php
                    }
                }
            }
        ?>
    </ul>
    <div class="clearboth"></div>
</div>
