<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-24
 * Time: 下午11:51
 * To change this template use File | Settings | File Templates.
 */

class Campaign_teleplays extends REST_Controller {

    public function read(){
        $params = $this->get();

        $response = CampaignTeleplay::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }


    public function create(){
        $data = $this->post('data');

        if($data['teleplay_id'] == 0 && empty($data['link'])) {
            $this->response(NULL,0,'剧集或者其它链接地址不能为空！',FALSE);
        }

        if(isset($data['teleplay_id'])) {
            $teleplay = CampaignTeleplay::first(array(
                'conditions' => "teleplay_id=".$data['teleplay_id']
            ));
            if($teleplay) {
                $this->response(NULL,0,'该剧集已经存在！',FALSE);
            }
        }

        unset($data['teleplay_name']);
        unset($data['id']);
        $response = CampaignTeleplay::create($data);
        $rs = $response->to_array();
        $rs['teleplay_name'] = $response->teleplay ? $response->teleplay->name : '';
        $this->response($rs,1,'增加成功！');
    }

    public function update($id = 0)
    {
        $model = CampaignTeleplay::find($id);
        $params = $this->put('data');

        $model->update_attributes($params);
        $this->response($model->to_array(),1,'编辑成功！');
    }

    public function destroy($id){
        $model = CampaignTeleplay::find($id);
        $model->delete();
        $this->response(NULL,1,'删除成功！');
    }
}