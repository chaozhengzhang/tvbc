<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-22
 * Time: 下午12:56
 * To change this template use File | Settings | File Templates.
 */

class InfoBanner extends ActiveRecord\Model {

    static $belongs_to = array(
        array('info','class_name' => 'Info')
    );

    static function get_by_params($params = array()){
        $columns = self::connection()->columns('info_banners');
        $options = array();
        if (isset($params['limit'])) {
            $options['limit'] = $params['limit'];
            $options['offset'] = $params['offset'];
            unset($params['limit']);
            unset($params['start']);
        }

        if(isset($params['order'])) {
            $options['order'] = $params['order']. " ASC";
        }

        $conditions = ' 1=1 ';

        foreach($columns as $columnKey=>$columnValue) {
            if(isset($params[$columnKey])) {
                if($columnValue->raw_type == 'varchar') {
                    $conditions .=  " AND $columnKey LIKE '%".$params[$columnKey]."%'";
                } else {
                    $conditions .=  " AND $columnKey=".$params[$columnKey];
                }
                unset($params[$columnKey]);
            }
        }

        $options['conditions'] = $conditions;

        $data['total'] = self::count(array('conditions'=>$conditions));
        $data['results'] = array();

        if($data['total'] > 0) {
            $activities = self::all($options);

            if($activities) {
                $data['results'] = array_map(function($row){
                    $_data = $row->to_array();
                    $_data['info_name'] = '';
                    if($row->info_id) {
                        $_data['info_name'] = $row->info->title;
                    }

                    return $_data;
                },$activities);
            }
        }

        return $data;
    }
}