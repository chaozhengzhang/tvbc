<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-17
 * Time: 下午9:23
 * To change this template use File | Settings | File Templates.
 */

class Teleplays extends CI_Controller {

    public function index()
    {
        $this->load->view('admins/teleplays/index');
    }

    public function stills()
    {
        $this->load->view('admins/teleplays/stills');
    }

    public function dramas()
    {
        $this->load->view('admins/teleplays/dramas');
    }
}