<script type="text/javascript">
Ext.define('Ext.ux.ArtistView', {
    extend: 'Ext.container.Container',
    alias: 'widget.artistview',
    autoScroll: true,
    activeRecord: null,
    initComponent: function (config) {
        var me = this;
        Ext.apply(me, config || {});
        me.store = Ext.create('Ext.ux.data.NotificationStore', {
            model: 'Artist',
            autoLoad: true,
            autoSync: true,
            proxy: {
                url: baseUrl('data/artists')
            },
            listeners: {
                afterSave: function(){
                    me.onReset();
                }
            }
        });
        me.initArtisForm();
        me.initGrid();
        me.items = [me.form, me.grid];
        me.callParent(arguments);
    },
    initArtisForm: function () {
        var me = this;

        me.form = Ext.widget('form', {
            defaultType: 'textfield',
            bodyPadding: 10,
            fieldDefaults: {
                anchor: '40%',
                labelAlign: 'right'
            },
            items: [
                {
                    xtype: 'hidden',
                    name: 'id'
                },
                {
                    fieldLabel: '中文名',
                    name: 'name',
                    allowBlank: false
                },
                {
                    fieldLabel: '性别',
                    name: 'sex',
                    allowBlank: false,
                    xtype: 'combo',
                    displayField: 'name',
                    valueField: 'id',
                    queryMode: 'local',
                    anchor: '20%',
                    store: Ext.create('Ext.data.Store', {
                        fields: ['name'],
                        data: [{
                            id: 1,
                            name: '男'
                        },{
                            id: 2,
                            name: '女'
                        }]
                    })
                },
                {
                    fieldLabel: '英文名',
                    name: 'ename',
                    allowBlank: false
                },
                {
                    xtype: 'combo',
                    fieldLabel: '姓名首字母',
                    name: 'initials',
                    displayField: 'name',
                    valueField: 'name',
                    queryMode: 'local',
                    anchor: '20%',
                    allowBlank: false,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['name'],
                        data: letter
                    })
                },
                {
                    fieldLabel: '生日',
                    anchor: '20%',
                    name: 'birthday',
                    allowBlank: false
                },
                {
                    xtype: 'combo',
                    fieldLabel: '星座',
                    name: 'constellation',
                    displayField: 'name',
                    valueField: 'name',
                    queryMode: 'local',
                    anchor: '20%',
                    allowBlank: false,
                    store: Ext.create('Ext.data.Store', {
                        fields: ['name'],
                        data: constellation
                    })
                },
                {
                    fieldLabel: '嗜好',
                    anchor: '70%',
                    name: 'hobby'
                },
                {
                    fieldLabel: '微博地址',
                    anchor: '70%',
                    name: 'blog',
                    vtype: 'url'
                },
                {
                    xtype: 'uploadfield',
                    itemId: 'cover_file',
                    fieldLabel: '图片',
                    fileType: 'image',
                    labelWidth: 60,
                    name: 'cover',
                    isSubmitValue: true,
                    margin: '0 0 0 40',
                    width: 520,
                    buttonText: '选择...',
                    debug: false,
                    queued: true,
                    showImage: true,
                    setting: {
                        file_types: '*.jpg;*.gif;*.jpeg;*.png',
                        upload_url: '/data/artists/upload_cover',
                        flash_url: 'public/swfupload/swfupload.swf',
                        flash9_url: 'public/swfupload/swfupload_fp9.swf'
                    }
                }
            ],
            buttonAlign: 'center',
            buttons: [
                {
                    text: '保存',
                    handler: function (btn, e) {
                        me.onSave();
                    }
                },
                {
                    text: '重置',
                    handler: function (btn, e) {
                        me.onReset();
                    }
                }
            ]
        });
    },

    initGrid: function () {
        var me = this;

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            errorSummary: false,
            autoCancel: false
        });

        me.grid = Ext.widget('grid', {
            store: me.store,
            columnLines: true,
            padding: 10,
            columns: [
                {
                    header: '编号',
                    width: 50,
                    dataIndex: 'id'
                },
                {
                    header: '中文名',
                    dataIndex: 'name',
                    flex: 1,
                    editor: {
                        allowBlank: false
                    }
                },
                {
                    header: '性别',
                    dataIndex: 'sex',
                    width: 60,
                    renderer: function(v){
                        return v == 1 ? '男':'女';
                    },
                    editor: {
                        allowBlank: false,
                        xtype: 'combo',
                        displayField: 'name',
                        valueField: 'id',
                        queryMode: 'local',
                        anchor: '20%',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['name'],
                            data: [{
                                id: 1,
                                name: '男'
                            },{
                                id: 2,
                                name: '女'
                            }]
                        })
                    }
                },
                {
                    header: '英文名',
                    flex: 1,
                    dataIndex: 'ename',
                    editor: {
                        allowBlank: false
                    }
                },
                {
                    header: '姓名首字母',
                    flex: 1,
                    dataIndex: 'initials',
                    editor: {
                        xtype: 'combo',
                        displayField: 'name',
                        valueField: 'name',
                        queryMode: 'local',
                        allowBlank: false,
                        store: Ext.create('Ext.data.Store', {
                            fields: ['name'],
                            data: letter
                        })
                    }
                },
                {
                    header: '星座',
                    flex: 1,
                    dataIndex: 'constellation',
                    editor: {
                        xtype: 'combo',
                        displayField: 'name',
                        valueField: 'name',
                        queryMode: 'local',
                        allowBlank: false,
                        store: Ext.create('Ext.data.Store', {
                            fields: ['name'],
                            data: constellation
                        })
                    }
                },
                {
                    header: '嗜好',
                    flex: 1,
                    dataIndex: 'hobby',
                    editor:{
                        xtype: 'textfield'
                    }
                },
                {
                    header: '微博地址',
                    flex: 1,
                    dataIndex: 'blog',
                    editor: {
                        allowBlank: false,
                        vtype: 'url'
                    }
                },
                {
                    xtype: 'actioncolumn',
                    width: 60,
                    header: '操作',
                    align: 'center',
                    items: [
                        {
                            icon: baseUrl('public/img/document_text.png'),  // Use a URL in the icon config
                            tooltip: '编辑',
                            handler: function (grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                me.setActiveRecord(rec);
                            }
                        },
                        {
                            icon: baseUrl('public/img/delete.png'),  // Use a URL in the icon config
                            tooltip: '删除',
                            iconCls: 'icon-action-margin',
                            handler: function (grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                grid.getStore().remove(rec);
                            }
                        }
                    ]
                }
            ],
            minHeight: 300,
            maxHeight: 480,
            title: '艺人列表',
            frame: true,
            plugins: [rowEditing],
            bbar: Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true
            })
        });
    },
    onSave: function () {
        var active = this.activeRecord,
            form = this.form.getForm();


        if (form.isValid()) {
            if (active) {
                form.updateRecord(active)
            } else {
                this.store.insert(0, form.getValues());
            }
        }
    },
    onReset: function () {
        this.down('#cover_file').setIamgeSrc({
            src: baseUrl('public/img/no_picture.gif')
        });
        this.form.getForm().reset();
    },
    setActiveRecord: function(record){
        this.activeRecord = record;
        if (record) {
            var data = {
                src : baseUrl('uploads/artists/cover/small/') + record.get('cover') + "?" + Math.random()
            };
            this.down('#cover_file').setIamgeSrc(data);
            this.form.getForm().loadRecord(record);
        }
    }
});

Ext.widget('artistview', {
    renderTo: 'artists-view'
})
</script>
<div id="artists-view"></div>