<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-24
 * Time: 下午10:22
 * To change this template use File | Settings | File Templates.
 */

class Campaigns extends REST_Controller {

    public function read(){
        $params = $this->get();

        $response = Campaign::get_by_params($params);

        $this->response($response['results'],$response['total']);
    }


    public function create(){
        $data = $this->post('data');
        if(Campaign::find_by_name($data['name'])){
            $this->response(NULL,0,'该活动已经存在！',FALSE);
        } else {
            $data['create_time'] = time();
            $data['start_time'] = strtotime($data['start_time']);
            $data['end_time'] = strtotime($data['end_time']);
            $response = Campaign::create($data);

            $rs = $response->to_array();
            $rs['start_time'] = date('Y-m-d',$response->start_time);
            $rs['end_time'] = date('Y-m-d',$response->end_time);
            $rs['create_time'] = date('Y-m-d H:i:s',$response->create_time);
            $this->response($rs,1,'增加成功！');
        }
    }

    public function update($id = 0)
    {
        $model = Campaign::find($id);
        $params = $this->put('data');

        if(isset($params['display'])) {
            if($params['display']) {
                Campaign::update_all(array(
                    'set' => ' display=0'
                ));
            }
        }

        if(isset($params['start_time'])) $params['start_time'] = strtotime($params['start_time']);
        if(isset($params['end_time'])) $params['end_time'] = strtotime($params['end_time']);

        $model->update_attributes($params);
        $rs = $model->to_array();
        $rs['start_time'] = date('Y-m-d',$model->start_time);
        $rs['end_time'] = date('Y-m-d',$model->end_time);
        $rs['create_time'] = date('Y-m-d H:i:s',$model->create_time);
        $this->response($rs,1,'编辑成功！');
    }

    public function destroy($id){
        $model = Campaign::find($id);
        $model->delete();
        $this->response(NULL,1,'删除成功！');
    }

}