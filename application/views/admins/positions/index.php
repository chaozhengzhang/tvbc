<script type="text/javascript">
    Ext.define('Ext.ux.PositionView',{
        extend: 'Ext.grid.Panel',
        alias: 'widget.positionview',
        frame: true,
        title: '职位管理 (*双击表格即可编辑数据)',
        minHeight:320,
        initComponent: function(config){
            var me = this;

            Ext.apply(me,  config||{});

            me.store = Ext.create('Ext.ux.data.NotificationStore', {
                model: 'Position',
                proxy: {
                    url: baseUrl('data/positions')
                }
            });

            me.columns = [
                {
                    text: '编号',
                    width: 40,
                    sortable: true,
                    dataIndex: 'ranking'
                }, {
                    text: '名称',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'name',
                    field: {
                        xtype: 'textfield',
                        allowBlank: false
                    }
                },
                {
                    xtype:'actioncolumn',
                    width:120,
                    text: '排序',
                    align: 'center',
                    items: [{
                        icon: 'public/img/up.png',
                        tooltip: '向上',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var ranking = rec.get('ranking');
                            if(ranking == 1) {
                                tools.showTips('提示','已经拍到第一位了！~');
                            } else {
                                rec.set('ranking',ranking - 1);
                                me.store.load();
                            }
                        }
                    },{
                        icon: 'public/img/down.png',
                        tooltip: '向下',
                        iconCls: 'icon-action-margin',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var ranking = rec.get('ranking');
                            var maxRanking = grid.getStore().getCount();
                            if(ranking == maxRanking) {
                                tools.showTips('提示','已经到最后一位了！~');
                            } else {
                                rec.set('ranking',ranking + 1);
                                me.store.load();
                            }
                        }
                    }]
                },
                {
                    xtype:'actioncolumn',
                    width:80,
                    text: '删除',
                    align: 'center',
                    items: [{
                        icon: 'public/img/delete.png',
                        tooltip: '删除职位',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            Ext.Msg.show({
                                title:'警告?',
                                msg: '数据一旦删除将无法恢复,确认删除？',
                                buttons: Ext.Msg.YESNO,
                                fn: function(btn, text){
                                    if(btn == 'yes') {
                                        me.store.remove(rec);
                                    }
                                },
                                icon: Ext.Msg.WARNING
                            });
                        }
                    }]
                }
            ];

            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                errorSummary: false,
                listeners: {
                    cancelEdit: function(rowEditing, context) {
                        if (context.record.phantom) {
                            me.store.remove(context.record);
                        }
                    }
                }
            });

            me.plugins = [rowEditing];

            me.tbar = [
                '-',
                {
                    text: '新增',
                    iconCls: 'icon-add',
                    handler: function(){
                        me.store.insert(0, new Position());
                        rowEditing.startEdit(0, 0);
                    }
                },
                '-'
            ];

            me.bbar = Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true
            });

            me.callParent(arguments);
        }
    });

    Ext.widget('positionview',{
        renderTo: 'position-view'
    });
</script>
<div id="position-view"></div>