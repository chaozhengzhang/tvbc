set :application, "tvbc"
server '101.226.179.55', :app, :web, :db, :primary => true

set :deploy_to, "/var/www/html/#{application}/testing"
set :branch, :develop
namespace :app do
  task :destroy do
    run "rm -rf #{release_path}/config #{release_path}/.git #{release_path}/.gitignore #{release_path}/Capfile"
    run "rm -f  #{release_path}/.gitignore"
    run "ln -s  #{shared_path}/public/config/database.php  #{release_path}/application/config/database.php"
    run "ln -s  /var/www/html/tvbc/staging/shared/public/uploads #{release_path}/uploads"
  end
end

after("deploy:update_code","app:destroy","deploy:cleanup")