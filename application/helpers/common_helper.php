<?php

if ( ! function_exists('get_ip_address')) {
	/**
	 * 获取用户真实IP
	 */
	function get_ip_address(){
		if (isset($_SERVER)){
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
				return $_SERVER["HTTP_X_FORWARDED_FOR"];
			} else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
				return $_SERVER["HTTP_CLIENT_IP"];
			} else {
				return $_SERVER["REMOTE_ADDR"];
			}
		} else {
			if (getenv("HTTP_X_FORWARDED_FOR")){
				return getenv("HTTP_X_FORWARDED_FOR");
			} else if (getenv("HTTP_CLIENT_IP")) {
				return getenv("HTTP_CLIENT_IP");
			} else {
				return getenv("REMOTE_ADDR");
			}
		}
	}
}

if( ! function_exists('get_real_address')) {

	/**
	 * 获取用户真实ip所在地
	 * @return array
	 */
	function get_real_address() {
		$server_url = "http://api.map.baidu.com/location/ip?ak=OPqTxbVaIuckWfDQFppp2ouM&ip=&coor=bd09ll";
		$infos = file_get_contents($server_url);
		$address_info = json_decode($infos);
		if(! $address_info->status) {
			return array(
				'address' => $address_info->content->address,
				'longitude' => $address_info->content->point->x,
				'latitude' => $address_info->content->point->y,
			);
		} else {
			return array(
				'address' => '',
				'longitude' => 0,
				'latitude' => 0,
			);
		}
	}
}


if( ! function_exists('check_keyword')) {
	/**
	 * 遍历一次待检测的字符串，逐个字符判断在$words是否存在以此字符为索引的值
	 *
	 */
	function check_keyword($text, $words)
	{
		foreach ($words as $word) {
            if(preg_match("/".$word."/i", $text)){
                return true;
                exit;
            }
        }
		return null;
	}
}