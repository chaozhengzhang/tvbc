<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-6-23
 * Time: 下午5:42
 * To change this template use File | Settings | File Templates.
 */

class Artists extends MY_Controller {

    public function index()
    {

        $artists = Artist::get_with_initial();

        //明星列表
        $this->load->vars('artists',$artists);
        //导航栏高亮样式
        $this->load->vars('selectNavli_artists','selectNavli');

        $this->render('artists/index');
    }

    public function show($id = 0){

        if(!is_numeric($id) && $id == 0) {
            show_404();
        }

        $artist = Artist::find($id);

        if(!$artist) {
            show_404();
        }

        //明星详情
        $this->load->vars('artist',$artist);

        //导航栏高亮样式
        $this->load->vars('selectNavli_artists','selectNavli');

        $this->render('artists/show');
    }
}